"""Build a validation set to test event detection algorithms

The validation set is saved as a CSV table with columns:
    - series: A concatenation of the session ID and the ROI ID to uniquely
    identify the corresponding baseline-corrected calcium fluorescence trace
    (dF/F)
    - timestamp: The timestamp (in ISO8601 format) at which each value in the
    dF/F traces was recorded
    - value: The value of the dF/F trace at the corresponding timestamp
    - label: The label of the corresponding value in the dF/F trace. Initially
    filled with zeros (0s), this column will be filled with the external
    annotation tool TRAINSET (https://trainset.geocene.com/). After annotation,
    this column should contain ones (1s) at the timestamps where a human
    evaluator judged that a calcium event (spike) occurred.

The validation set consists of dF/F traces from ROIs sampled from the whole
dataset. The ROIs are sampled in a stratified manner according to group (rested
vs. sleep deprived flies), i.e., so that the proportion of ROIs from each group
is the same as in the whole dataset.
"""

import logging
import os
import traceback
from datetime import datetime, timedelta

import click
import numpy as np
import pandas as pd
import pytz

from sleepyflybrains import (
    dataset,
)

# Set up logging level to INFO when debugging
logging.basicConfig(level=logging.INFO)

# Random seed to use for reproducibility
SEED = 2024


def get_session_from_id(list_of_sessions, session_id):
    selected_session = None
    for session in list_of_sessions:
        if session.session_id == session_id:
            selected_session = session
            break
    return selected_session


def _make_roi_dict(traces_dict, list_of_sessions):
    roi_dict = {
        "session_id": [],
        "roi_id": [],
        "roi_trace": [],
        "timestamp": [],
    }
    for session_id, trace_array in traces_dict.items():
        # Get session date YYYY-MM-DD HH:MM:SS:MILLISECONDS
        date = get_session_from_id(list_of_sessions, session_id).date

        # Get time vector. We purposely do not normalize by the sample rate
        # to artificially consider that we have 1 sample per minute. This is
        # purely to adapt to the interface of the external annotation tool
        # TRAINSET (https://trainset.geocene.com/), where the time resolution
        # is 1 second, but the default zoom level has ticks every 3 hours.
        _, n_timesteps = trace_array.shape
        times = np.arange(n_timesteps).astype(float)  # in "minutes"

        # Get corresponding timestamps (in ISO8601 format) using the datetime
        # package
        timestamps = []
        for time in times:
            # Turn date into a datetime object
            dt = datetime.fromisoformat(date)
            # Add the time (in seconds) to the datetime object
            dt += timedelta(minutes=time)
            # Include time zone information
            dt = pytz.timezone("Europe/Zurich").localize(dt)
            timestamps.append(dt.replace(microsecond=0).isoformat())

        # Record the session ID, ROI ID, ROI trace, and timestamps
        for roi_id, roi_trace in enumerate(trace_array):
            roi_dict["session_id"].append(session_id)
            roi_dict["roi_id"].append(roi_id)
            roi_dict["roi_trace"].append(roi_trace)
            roi_dict["timestamp"].append(timestamps)

    return roi_dict


def _sample_val_set(roi_dict, val_size, seed, sr_dict):
    n_rois = len(roi_dict["roi_trace"])
    if val_size < 1:
        n_samples = int(val_size * n_rois)
        rng = np.random.default_rng(seed=seed)
        sampled_indices = rng.choice(
            n_rois,
            size=n_samples,
            replace=False,
        )
    else:
        # If val_size is 1 (or larger), then just sample all ROIs
        n_samples = n_rois
        sampled_indices = np.arange(n_samples)
    logging.info("\tTotal number of ROIs: {}".format(n_rois))
    logging.info("\tSampling {} ROIs...".format(n_samples))
    val_dict = {
        "session_id": [],
        "roi_id": [],
        "roi_trace": [],
        "timestamp": [],
    }
    for key in roi_dict.keys():
        for i in sampled_indices:
            val_dict[key].append(roi_dict[key][i])
            if key == "roi_trace":
                logging.info(
                    "\t\tSampled trace range: "
                    + "min = {:.3f} max = {:.3f}".format(
                        np.min(roi_dict[key][i]),
                        np.max(roi_dict[key][i]),
                    )
                )
            if key == "session_id":
                logging.info(
                    "\t\tSampled trace sample-rate: {}".format(
                        sr_dict[roi_dict[key][i]]
                    )
                )
    return val_dict


def _make_df_from_sampled_data(val_dict):
    df = {
        "series": [],
        "timestamp": [],
        "value": [],
        "label": [],
    }
    for i, roi_trace in enumerate(val_dict["roi_trace"]):
        n_timesteps = len(roi_trace)
        df["series"] += [
            "{}@{}".format(
                val_dict["session_id"][i],
                val_dict["roi_id"][i],
            )
        ] * n_timesteps
        df["timestamp"] += val_dict["timestamp"][i]
        df["value"] += roi_trace.tolist()
        df["label"] += [0] * n_timesteps

    return pd.DataFrame(df)


def sample_rois(list_of_sessions, val_size, seed=2023):
    # Get traces from all sessions
    traces_dict, sr_dict = dataset.get_traces(
        list_of_sessions,
        trace_type="dFF",
    )
    # Make lists with the same sizes, one with session IDs, another with ROI
    # numbers, another with the individual ROI traces, and another with the
    # timestamps of each trace
    roi_dict = _make_roi_dict(traces_dict, list_of_sessions)
    # Sample val_size fraction from these lists
    val_dict = _sample_val_set(roi_dict, val_size, seed, sr_dict)
    # Return a dataframe with the sampled data
    return _make_df_from_sampled_data(val_dict)


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the dataset directory",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the directory where to save the output",
)
@click.option(
    "--subsets",
    required=False,
    default=["84C10_23E10_GCaMP7f"],
    multiple=True,
    help="List of sub-datasets to load and sample from",
)
@click.option(
    "--val_size",
    required=False,
    default=0.1,
    type=click.FloatRange(min=0, max=1),
    help="Fraction of ROIs to sample into the validation set",
)
def main(data_dir, save_dir, subsets, val_size):
    # Load dataset
    session_dataset = dataset.SessionDataset(data_dir, subsets=subsets)

    # Get rested and sleep-deprived sessions
    rested = session_dataset.get_rested()
    sleep_deprived = session_dataset.get_sleep_deprived()

    # Sample from the rested group
    print("Sampling from the rested group...")
    df_rested = sample_rois(rested, val_size, seed=SEED)

    # Sample from the sleep-deprived group
    print("Sampling from the sleep-deprived group...")
    df_sleep_deprived = sample_rois(sleep_deprived, val_size, seed=SEED)

    # Concatenate the two dataframes
    print("Building the validation set table...")
    df = pd.concat([df_rested, df_sleep_deprived])

    # Save the dataframe as a CSV file
    path = os.path.join(save_dir, "validation_traces.csv")
    df.to_csv(path, index=False)

    print("Done. Output was saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
