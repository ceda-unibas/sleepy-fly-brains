import json
import os
import traceback

import click
import numpy as np
import pandas as pd
from tqdm import tqdm

from sleepyflybrains import dataset, detectors, utils


def _load_human_annotations(annotations_table):
    print("Loading validation data...")
    validation_peaks_df = pd.read_csv(annotations_table)

    # Turn NaNs into zeros in the `'label'` column
    validation_peaks_df["label"] = validation_peaks_df["label"].fillna(0)
    return validation_peaks_df


def _get_sample_rates(session_dataset):
    sample_rates = {}
    for session in session_dataset.session_list:
        sample_rates[session.session_id] = session.sample_rate
    return sample_rates


def _make_spike_detector(detector, model_dir):
    print("Loading spike detection model...")
    if detector == "peak":
        peak_detector = detectors.PeakDetector(model_dir)
    elif detector == "cascade":
        peak_detector = detectors.CascadeDetector(model_dir)
    elif detector == "oasis":
        peak_detector = detectors.OASISDetector(model_dir)
    else:
        raise ValueError("Invalid detector type: {}".format(detector))
    return peak_detector


def _aggregate_matches_and_mismatches(
    validation_peaks_df, sample_rates, peak_detector, time_tolerance
):
    # Create a copy of the validation peaks dataframe onto which we will
    # append the detected peaks
    validation_and_detected_peaks_df = validation_peaks_df.copy(deep=True)

    # Add a new column to the dataframe to store the predicted peaks
    validation_and_detected_peaks_df["predicted_label"] = np.nan

    TP = 0
    FP = 0
    FN = 0
    roi_grouping = validation_peaks_df.groupby("series")
    for name, roi_df in tqdm(roi_grouping, desc="ROIs"):
        # Get session ID
        session_id, _ = name.split("@")

        # Get dFF trace of ROI
        roi_trace = roi_df["value"].values

        # The trace is supposed to be a 2D array
        # with dimensions #ROIs x #Time-steps. Since only a 1D array is
        # being provided here, we need to reshape it accordingly
        roi_trace = roi_trace.reshape((1, -1))

        # Get sample rate of session
        sample_rate = sample_rates[session_id]

        # Predict peaks
        if isinstance(peak_detector, detectors.CascadeDetector) or isinstance(
            peak_detector, detectors.PeakDetector
        ):
            peaks = peak_detector.predict_discrete(
                roi_trace, sample_rate=sample_rate
            ).flatten()
        elif isinstance(peak_detector, detectors.OasisDetector):
            peaks = peak_detector.predict_discrete(roi_trace).flatten()

        # Append predicted peaks to copied dataframe.
        validation_and_detected_peaks_df.loc[
            validation_and_detected_peaks_df["series"] == name, "predicted_label"
        ] = peaks

        # Get human-annotated peaks
        gt_peaks = roi_df["label"].values

        # Compute TP, FP, and FN
        increment_TP, increment_FP, increment_FN = (
            utils.count_correct_and_incorrect_peaks(
                gt_peaks=gt_peaks,
                predicted_peaks=peaks,
                tolerance_in_samples=int(time_tolerance * sample_rate / 1000),
            )
        )
        TP += increment_TP
        FP += increment_FP
        FN += increment_FN
    return TP, FP, FN, validation_and_detected_peaks_df


def _compute_metrics(TP, FP, FN):
    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    f1 = 2 * (precision * recall) / (precision + recall)
    results = dict(TP=TP, FP=FP, FN=FN, precision=precision, recall=recall, f1=f1)
    return results


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the dataset directory",
)
@click.option(
    "--model_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the spike detection model directory",
)
@click.option(
    "--annotations_table",
    required=True,
    type=click.Path(file_okay=True, exists=True),
    help="Path to the CSV table with human-annotated dFF peaks",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True),
    help="Path to the directory where to save the output",
)
@click.option(
    "--detector",
    required=True,
    type=click.Choice(["cascade", "peak", "oasis"]),
    help="Spike detector to use",
)
@click.option(
    "--time_tolerance",
    required=False,
    type=int,
    default=250,
    help="Delay or advance (in milliseconds) tolerated when "
    + "matching detected peaks with validation peaks",
)
@click.option(
    "--subsets",
    required=False,
    default=["84C10_23E10_GCaMP7f"],
    multiple=True,
    help="List of sub-datasets to use",
)
@click.option(
    "--save_detections",
    required=False,
    default=False,
    is_flag=True,
    help="Save detected peaks as CSV file",
)
def main(
    data_dir,
    model_dir,
    annotations_table,
    save_dir,
    detector,
    time_tolerance,
    subsets,
    save_detections,
):
    # Load CSV table with hand-annotated dFF peaks for validation
    validation_peaks_df = _load_human_annotations(annotations_table)

    # Create dataset object
    session_dataset = dataset.SessionDataset(data_dir, subsets=subsets)

    # Get sample rate of each session in the dataset
    sample_rates = _get_sample_rates(session_dataset)

    # Create spike detector object
    peak_detector = _make_spike_detector(detector, model_dir)

    # Run evaluation
    print("Running evaluation...")

    # Loop over annotated ROIs, aggregating number of true positives (TP),
    # false positives (FP), and false negatives (FN)
    TP, FP, FN, validation_and_detected_peaks_df = _aggregate_matches_and_mismatches(
        validation_peaks_df, sample_rates, peak_detector, time_tolerance
    )

    # Compute precision, recall, and F1 score
    results = _compute_metrics(TP, FP, FN)

    # Save results
    print("Saving results...")
    os.makedirs(save_dir, exist_ok=True)

    # Save evaluation results as JSON file
    file_name = "eval_metrics_{}detector_{}ms_tolerance".format(
        detector, time_tolerance
    )
    file_path = os.path.join(save_dir, file_name + ".json")
    with open(file_path, "w") as f:
        json.dump(results, f, indent=4)

    if save_detections:
        # Save dataframe with detected peaks as CSV file
        file_name = "validation_and_detected_peaks_{}detector".format(
            detector,
        )
        file_path = os.path.join(save_dir, file_name + ".csv")
        validation_and_detected_peaks_df.to_csv(file_path, index=False)

    print("Done. The results were saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
