import json
import logging
import os
import traceback

import click
import numpy as np
import ray
import ruptures as rpt
from tqdm import tqdm

from sleepyflybrains import dataset, detectors

# Set up logging level to INFO when debugging
# logging.basicConfig(level=logging.INFO)


# Define the breakpoint finding routine as a Ray remote to parallelize the
# operations
@ray.remote
def find_breakpoints(signal, cost, jump, pen):
    """Find breakpoints in a signal using a change point detection algorithm

    Parameters
    ----------
    signal : array-like
        1D array of shape (1, n_samples) containing the trace of an ROIs
    cost : ruptures.costs.Cost
        Cost function to use in change point detection
    jump : int
        Subsampling parameter for the change point detection algorithm
    pen : float
        Penalty parameter for the change point detection algorithm

    Returns
    -------
    list
        List of breakpoints found in the signal
    """
    algo = rpt.Binseg(custom_cost=cost, jump=jump).fit(signal)
    return algo.predict(pen=pen)


def change_point_detection(traces_dict, sample_rates, config):
    """Find change points in a set of traces

    Parameters
    ----------
    traces_dict : dict
        Dictionary of traces, where keys are session IDs and values are
        2D arrays of shape (n_rois, n_samples)
    sample_rates : dict
        Dictionary of sample rates, where keys are session IDs and values are
        the respective session's sample rate in Hz
    config : dict
        Configuration dictionary containing the keys:
            window_size : float
                Window size (in seconds) for the auto-regressive cost in change point
                detection
            jump : float
                Subsampling parameter (in seconds) for the change point detection
                algorithm
            sigma : float
                Scale parameter in change point detection that controls how many
                change points are detected (higher values -> fewer change points)

    Returns
    -------
    dict
        Dictionary of breakpoints, where keys are session IDs and values are
        lists of breakpoints for each ROI
    """
    # Get configuration parameters
    window_size = config["window_size"]
    jump = config["jump"]
    sigma = config["sigma"]

    bkps_dict = {}
    for session_id, array in tqdm(traces_dict.items()):
        print("\tComputing change points for session {}...".format(session_id))
        try:
            array = array.toarray()
        except AttributeError:
            pass

        # Get number of ROIs and time samples
        n_rois, n_samples = array.shape
        logging.info(
            "Session {} has {} ROIs and {} time samples".format(
                session_id, n_rois, n_samples
            )
        )

        # Get session's sample rate
        sample_rate = sample_rates[session_id]
        logging.info(
            "Sample rate for session {} is {} Hz".format(session_id, sample_rate)
        )

        # Define cost function
        window_size_in_samples = int(window_size * sample_rate)  # Change to samples
        logging.info(
            "Window size for session {} is {} samples".format(
                session_id, window_size_in_samples
            )
        )
        cost = rpt.costs.CostAR(order=window_size_in_samples)

        # Convert jump to samples
        jump_in_samples = int(jump * sample_rate)

        # Find breakpoints for each ROI in parallel
        # Restrict signal to one of the ROIs
        session_bkps = ray.get(
            [
                find_breakpoints.remote(
                    array[i, :], cost, jump_in_samples, np.log(n_samples) * 1 * sigma**2
                )
                for i in range(n_rois)
            ]
        )

        bkps_dict[session_id] = session_bkps
    return bkps_dict


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the dataset directory",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the directory where to save the output",
)
@click.option(
    "--spike_detector",
    required=True,
    type=click.Choice(["lookup", "peak"]),
    help="Spike detector to use",
)
@click.option(
    "--models_dir",
    required=False,
    type=click.Path(dir_okay=True, exists=True),
    default=None,
    help="Path to the spike detection models",
)
@click.option(
    "--window_size",
    required=False,
    type=float,
    default=10,
    help="Window size (in seconds) for the auto-regressive cost"
    + " in change point detection",
)
@click.option(
    "--jump",
    required=False,
    type=float,
    default=1,
    help="Subsampling parameter (in seconds) for the change"
    + " point detection algorithm",
)
@click.option(
    "--sigma",
    required=False,
    type=float,
    default=0.35,
    help="Scale parameter in change point detection that controls"
    + " how many change points are detected (higher values -> fewer"
    + " change points))",
)
@click.option(
    "--subsets",
    required=False,
    default=["84C10_23E10_GCaMP7f"],
    multiple=True,
    help="List of sub-datasets to use",
)
def main(
    data_dir, save_dir, spike_detector, models_dir, window_size, jump, sigma, subsets
):
    # Load dataset
    session_dataset = dataset.SessionDataset(data_dir, subsets=subsets)

    # Get rested and sleep-deprived sessions
    rested = session_dataset.get_rested()
    sleep_deprived = session_dataset.get_sleep_deprived()

    # Create spike detector object
    if spike_detector == "peak":
        sd = detectors.PeakDetector(model_dir=os.path.join(models_dir, "peakdetector"))
    elif spike_detector == "lookup":
        sd = detectors.LookupDetector(model_dir=os.path.join(models_dir, "lookup"))
    print("Using {} spike detector".format(spike_detector))

    # Get discrete spike traces for all sessions
    rested_traces, rested_sr = dataset.get_traces(
        rested, trace_type="predict_discrete", sd=sd
    )
    sleep_deprived_traces, sleep_deprived_sr = dataset.get_traces(
        sleep_deprived, trace_type="predict_discrete", sd=sd
    )

    # Define cost function and parameters for change point detection
    print("Change point detection for sessions in the rested group...")
    rested_bkps_dict = change_point_detection(
        rested_traces, rested_sr, dict(window_size=window_size, jump=jump, sigma=sigma)
    )

    print("Change point detection for sessions in the sleep-deprived group...")
    sleep_deprived_bkps_dict = change_point_detection(
        sleep_deprived_traces,
        sleep_deprived_sr,
        dict(window_size=window_size, jump=jump, sigma=sigma),
    )

    # Save results as json
    print("Saving results...")
    os.makedirs(save_dir, exist_ok=True)
    arguments = {
        "spike_detector": spike_detector,
        "window_size": window_size,
        "jump": jump,
        "sigma": sigma,
    }
    results = {
        "arguments": arguments,
        "rested": rested_bkps_dict,
        "sleep_deprived": sleep_deprived_bkps_dict,
    }
    file_name = "results_change_point_detection_from_" + "{}_spikes.json".format(
        spike_detector
    )
    with open(os.path.join(save_dir, file_name), "w") as f:
        json.dump(results, f, indent=4)

    print("Done. Dictionary of results were saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
