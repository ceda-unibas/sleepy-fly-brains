"""Build a look up table for LookupDetector from a CSV file with annotated
events.
"""

import logging
import os
import traceback

import click
import numpy as np
import pandas as pd

# Set up logging level to INFO when debugging
logging.basicConfig(level=logging.INFO)


@click.command()
@click.option(
    "--val_traces_path",
    required=True,
    type=click.Path(dir_okay=False, exists=True),
    help="Path to the CSV file containing annotated traces",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the directory where to save the output",
)
@click.option(
    "--subsets",
    required=False,
    default=["84C10_23E10_GCaMP7f"],
    multiple=True,
    help="List of sub-datasets to build a look up table for",
)
def main(val_traces_path, save_dir, subsets):
    print("Building the look up table...")

    # Read the validation traces table
    lookup_table = pd.read_csv(val_traces_path)

    # This dataframe should have columns 'series', 'timestamp', 'value',
    # and 'label'. The values in 'timestamp' are useless for us. So let us
    # rename this column 'time_step' and replace these values with equivalent
    # sequences of increasing integers
    lookup_table.rename(columns={"timestamp": "time_step"}, inplace=True)
    series_grouping = lookup_table.groupby("series")
    for _, series_df in series_grouping:
        lookup_table.loc[series_df.index, "time_step"] = np.arange(len(series_df))

    # The column 'series' contains the session ID and the ROI ID separated
    # by an '@' symbol. We can split this column into two columns, one
    # containing the session ID and the other containing the ROI ID
    lookup_table[["series", "roi"]] = lookup_table["series"].str.split("@", expand=True)

    # Rename the column 'series' to 'session_id'
    lookup_table.rename(columns={"series": "session_id"}, inplace=True)

    # Rename the column 'value' to 'dFF'
    lookup_table.rename(columns={"value": "dFF"}, inplace=True)

    # Drop the rows where label is different from 1 to reduce the footprint of
    # the table we'll save
    lookup_table = lookup_table[lookup_table["label"] == 1]

    # Now that we know where the events are, we can drop the 'label' column
    lookup_table.drop(columns="label", inplace=True)

    # Extract parts of the table that correspond to the selected subsets
    for subset in subsets:
        # Remove the "_GCaMP7f" suffix from the subset name if it is there
        prefix = subset.replace("_GCaMP7f", "")
        subset_table = lookup_table[lookup_table["session_id"].str.contains(prefix)]
        if subset_table.empty:
            logging.warning("No traces found for subset {}. Skipping.".format(subset))
            continue
        # Save the subset table to a CSV file
        subset_save_path = os.path.join(save_dir, subset + ".csv")
        subset_table.to_csv(subset_save_path, index=False)

    print("Done. Output was saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
