"""Hyperparameter search for PeakDetector"""

import json
import os
import traceback

import click
import pandas as pd
from hyperopt import fmin, hp, space_eval, tpe
from ot import wasserstein_1d
from scipy.ndimage import gaussian_filter1d
from tqdm import tqdm

from sleepyflybrains import dataset, detectors


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the dataset directory",
)
@click.option(
    "--annotations_table",
    required=True,
    type=click.Path(file_okay=True, exists=True),
    help="Path to the CSV table with human-annotated dFF peaks",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True),
    help="Path to the directory where to save the output",
)
@click.option(
    "--subsets",
    required=False,
    default=["84C10_23E10_GCaMP7f"],
    multiple=True,
    help="List of sub-datasets to use",
)
def main(data_dir, annotations_table, save_dir, subsets):
    # Load CSV table with hand-annotated dFF peaks for validation
    validation_peaks_df = pd.read_csv(
        annotations_table,
    )

    # Turn NaNs into zeros in the `'label'` column
    validation_peaks_df["label"] = validation_peaks_df["label"].fillna(0)

    # Create dataset object
    session_dataset = dataset.SessionDataset(data_dir, subsets=subsets)

    # Get sample rate of each session in the dataset
    sample_rates = {}
    for session in session_dataset.session_list:
        sample_rates[session.session_id] = session.sample_rate

    # Create spike detector object
    peak_detector = detectors.PeakDetector()

    # Define hyperparameter search space
    space = {
        "distance": hp.uniform("distance", 0.2, 2),  # Minimum distance between
        # peaks [seconds]
        "prominence": hp.uniform("prominence", 0.01, 0.99),  # Minimum
        # prominence of
        # peaks
        "width": hp.uniform("width", 0.1, 1),  # Width of the peak [seconds]
        "rel_height": hp.uniform("rel_height", 0, 1),  # Minimum relative
        # height of peaks
        "filter_sigma": hp.uniform("filter_sigma", 0.1, 1),  # Standard
        # deviation of
        # the Gaussian
        # denoising filter
        # [seconds]
        "filter_radius": hp.uniform("filter_radius", 0.1, 1),  # Radius of the
        # Gaussian
        # denoising
        # filter
        # [seconds]
    }

    # Define objective function
    def objective(params):
        # Set denoising filter
        def denoising_filter(trace, sample_rate):
            return gaussian_filter1d(
                trace,
                sigma=params["filter_sigma"] * sample_rate,
                mode="nearest",
                radius=int(params["filter_radius"] * sample_rate),
                axis=-1,
            )

        # Loop over annotated ROIs, aggregating prediction losses
        loss = 0
        roi_grouping = validation_peaks_df.groupby("series")
        for name, roi_df in tqdm(roi_grouping, desc="ROIs"):
            # Get session ID
            session_id, _ = name.split("@")

            # Get dFF trace of ROI
            roi_trace = roi_df["value"].values

            # The trace is supposed to be a 2D array
            # with dimensions #ROIs x #Time-steps. Since only a 1D array is
            # being provided here, we need to reshape it accordingly
            roi_trace = roi_trace.reshape((1, -1))

            # Get sample rate of session
            sample_rate = sample_rates[session_id]

            # Predict peaks
            peaks = peak_detector.predict_discrete(
                roi_trace,
                sample_rate=sample_rate,
                distance=params["distance"] * sample_rate,
                prominence=params["prominence"],
                width=params["width"] * sample_rate,
                rel_height=params["rel_height"],
                denoising_filter=lambda x: denoising_filter(x, sample_rate),
            )

            # Get human-annotated peaks and reshape them to match the shape of
            # the predicted peaks
            gt_peaks = roi_df["label"].values
            gt_peaks = gt_peaks.reshape((1, -1))

            # Compute Wasserstein distance between detected peaks and
            # validation peaks
            loss += wasserstein_1d(peaks.T, gt_peaks.T)[0]

        return loss

    # Run hyperparameter search
    print("Running hyperparameter search...")
    best = fmin(
        fn=objective,
        space=space,
        algo=tpe.suggest,
        max_evals=10000,
    )

    # Print best hyperparameters
    print("Best hyperparameters:")
    hyperparams = space_eval(space, best)
    print(hyperparams)

    # Save best hyperparameters to json file
    print("Saving results...")
    os.makedirs(save_dir, exist_ok=True)
    with open(os.path.join(save_dir, "best_params.json"), "w") as f:
        json.dump(hyperparams, f)

    print("Done. The results were saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
