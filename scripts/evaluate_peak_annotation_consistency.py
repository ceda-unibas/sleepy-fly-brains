"""Evaluate consistency in past and current peak annotations made by a human"""

import json
import os
import traceback

import click
import pandas as pd

from sleepyflybrains import (
    dataset,
    utils,
)


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the dataset directory",
)
@click.option(
    "--past_annotations",
    required=True,
    type=click.Path(file_okay=True, exists=True),
    help="Path to the CSV file with past annotations",
)
@click.option(
    "--current_annotations",
    required=True,
    type=click.Path(file_okay=True, exists=True),
    help="Path to the CSV file with current annotations",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the directory where to save the output",
)
@click.option(
    "--time_tolerance",
    required=False,
    type=int,
    default=250,
    help="Delay or advance (in milliseconds) tolerated when "
    + "matching detected peaks with validation peaks",
)
@click.option(
    "--subsets",
    required=False,
    default=["84C10_23E10_GCaMP7f"],
    multiple=True,
    help="List of sub-datasets to use",
)
@click.option(
    "--save_merged_annotations",
    required=False,
    default=False,
    is_flag=True,
    help="Whether to save the merged annotations as a CSV file",
)
def main(
    data_dir,
    past_annotations,
    current_annotations,
    save_dir,
    time_tolerance,
    subsets,
    save_merged_annotations,
):
    # Load CSV tables with hand-annotated dFF peaks
    print("Loading annotation data...")
    past_annotations_df = pd.read_csv(
        past_annotations,
    )
    current_annotations_df = pd.read_csv(
        current_annotations,
    )

    # Turn NaNs into zeros in the `'label'` column
    past_annotations_df["label"] = past_annotations_df["label"].fillna(0)
    current_annotations_df["label"] = current_annotations_df["label"].fillna(0)

    # Figure out which series belong to both past and current annotations
    series_intersection = set(past_annotations_df["series"]).intersection(
        set(current_annotations_df["series"])
    )

    # Filter out series that are not in the intersection
    past_annotations_df = past_annotations_df[
        past_annotations_df["series"].isin(series_intersection)
    ]
    past_annotations_df.reset_index(drop=True, inplace=True)

    current_annotations_df = current_annotations_df[
        current_annotations_df["series"].isin(series_intersection)
    ]
    current_annotations_df.reset_index(drop=True, inplace=True)

    # Merge past and current annotations into a single dataframe
    validation_peaks_df = pd.merge(
        past_annotations_df,
        current_annotations_df,
        on=["series", "timestamp", "value"],
        suffixes=("_past", "_current"),
    )

    # Get sample rate of each session in the dataset
    session_dataset = dataset.SessionDataset(data_dir, subsets=subsets)
    sample_rates = {}
    for session in session_dataset.session_list:
        sample_rates[session.session_id] = session.sample_rate

    # Run evaluation
    print("Running evaluation...")

    # Compute number of true positives (TP),
    # false positives (FP), and false negatives (FN), taking the current
    # annotations as ground truth
    TP = 0
    FP = 0
    FN = 0
    roi_grouping = validation_peaks_df.groupby("series")
    for name, roi_df in roi_grouping:
        # Get session ID
        session_id, _ = name.split("@")

        # Get sample rate of session
        sample_rate = sample_rates[session_id]

        # Interpret current annotations as ground truth and past annotations as
        # predicted peaks
        gt_peaks = roi_df["label_current"].values
        predicted_peaks = roi_df["label_past"].values

        # Compute TP, FP, and FN
        increment_TP, increment_FP, increment_FN = (
            utils.count_correct_and_incorrect_peaks(
                gt_peaks=gt_peaks,
                predicted_peaks=predicted_peaks,
                tolerance_in_samples=int(time_tolerance * sample_rate / 1000),
            )
        )
        TP += increment_TP
        FP += increment_FP
        FN += increment_FN

    # Compute precision, recall, and F1 score
    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    f1 = 2 * (precision * recall) / (precision + recall)

    # Build dictionary with results
    results = {
        "TP": TP,
        "FP": FP,
        "FN": FN,
        "precision": precision,
        "recall": recall,
        "f1": f1,
    }

    # Save results
    print("Saving results...")
    os.makedirs(save_dir, exist_ok=True)

    # Save evaluation results as JSON file
    file_name = "eval_metrics_peak_annotation_consistency_{}ms_tolerance".format(
        time_tolerance
    )
    file_path = os.path.join(save_dir, file_name + ".json")
    with open(file_path, "w") as f:
        json.dump(results, f, indent=4)

    # Save merged annotations as CSV file
    if save_merged_annotations:
        file_name = "past_current_annotations_merged"
        file_path = os.path.join(save_dir, file_name + ".csv")
        validation_peaks_df.to_csv(file_path, index=False)

    print("Done. The results were saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
