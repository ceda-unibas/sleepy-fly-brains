"""Hyperparameter search for PeakDetector"""

import json
import os
import traceback

import click
import numpy as np
import pandas as pd
from hyperopt import fmin, hp, space_eval, tpe
from ot import wasserstein_1d
from tqdm import tqdm

from sleepyflybrains import dataset, detectors


class NpEncoder(json.JSONEncoder):
    """JSON encoder for numpy types"""

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the dataset directory",
)
@click.option(
    "--annotations_table",
    required=True,
    type=click.Path(file_okay=True, exists=True),
    help="Path to the CSV table with human-annotated dFF peaks",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True),
    help="Path to the directory where to save the output",
)
@click.option(
    "--subsets",
    required=False,
    default=["84C10_23E10_GCaMP7f"],
    multiple=True,
    help="List of sub-datasets to use",
)
def main(data_dir, annotations_table, save_dir, subsets):
    # Load CSV table with hand-annotated dFF peaks for validation
    validation_peaks_df = pd.read_csv(
        annotations_table,
    )

    # Turn NaNs into zeros in the `'label'` column
    validation_peaks_df["label"] = validation_peaks_df["label"].fillna(0)

    # Create dataset object
    session_dataset = dataset.SessionDataset(data_dir, subsets=subsets)

    # Get sample rate of each session in the dataset
    sample_rates = {}
    for session in session_dataset.session_list:
        sample_rates[session.session_id] = session.sample_rate

    # Create spike detector object
    oasis_detector = detectors.OASISDetector()

    # Define hyperparameter search space
    space = {
        "optimize_g": hp.choice("optimize_g", [1, 2, 4, 8]),  # Number of
        # large isolated
        # calcium events
        # to use for
        # figuring out
        # the decay time
        # constant
        "penalty": hp.choice("penalty", [0, 1]),  # Use l0 or l1 penalty in the
        # deconvolution objective
        "threshold": hp.uniform("threshold", 0.0, 0.5),  # Threshold for
        # binarizing the spike
        # trains returned by
        # the OASIS
        # deconvolution
        # function
        "max_iter": hp.choice("max_iter", [5, 10, 15, 20]),  # Maximum number
        # of iterations
        # for the
        # deconvolution
        # algorithm
        "sn_factor": hp.uniform("sn_factor", 0.7, 1.0),  # Factor by which to
        # exponentiate the
        # noise level
        # estimation to better
        # distinguish low- and
        # high-SNR traces
    }

    # Define objective function
    def objective(params):
        # Loop over annotated ROIs, aggregating prediction losses
        loss = 0
        roi_grouping = validation_peaks_df.groupby("series")
        for _, roi_df in tqdm(roi_grouping, desc="ROIs"):
            # Get dFF trace of ROI
            roi_trace = roi_df["value"].values

            # The trace is supposed to be a 2D array
            # with dimensions #ROIs x #Time-steps. Since only a 1D array is
            # being provided here, we need to reshape it accordingly
            roi_trace = roi_trace.reshape((1, -1))

            # Predict peaks
            peaks = oasis_detector.predict_discrete(
                roi_trace,
                threshold=params["threshold"],
                optimize_g=params["optimize_g"],
                penalty=params["penalty"],
                max_iter=params["max_iter"],
                sn_factor=params["sn_factor"],
            )

            # Get human-annotated peaks and reshape them to match the shape of
            # the predicted peaks
            gt_peaks = roi_df["label"].values
            gt_peaks = gt_peaks.reshape((1, -1))

            # Compute Wasserstein distance between detected peaks and
            # validation peaks
            loss += wasserstein_1d(peaks.T, gt_peaks.T)[0]

        return loss

    # Run hyperparameter search
    print("Running hyperparameter search...")
    best = fmin(
        fn=objective,
        space=space,
        algo=tpe.suggest,
        max_evals=10000,
    )

    # Print best hyperparameters
    print("Best hyperparameters:")
    hyperparams = space_eval(space, best)
    print(hyperparams)

    # Save best hyperparameters to json file
    print("Saving results...")
    os.makedirs(save_dir, exist_ok=True)
    with open(os.path.join(save_dir, "best_params.json"), "w") as f:
        json.dump(hyperparams, f, cls=NpEncoder)

    print("Done. The results were saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
