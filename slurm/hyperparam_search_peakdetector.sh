#!/bin/bash

#Name of the job
#SBATCH --job-name=hyperparam_search_peakdetector

#Number of CPU cores reserved
#SBATCH --cpus-per-task=4

#Memory reserved per core.
#SBATCH --mem-per-cpu=32G

#Time during which the task will run
#SBATCH --time=06:00:00
#SBATCH --qos=6hours

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/hyperparam_search_peakdetector.o
#SBATCH --error=logs/hyperparam_search_peakdetector.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
past_annotations=$SFB_DATA/analysis_files/validation_traces-labeled_OLD.csv
current_annotations=$SFB_DATA/analysis_files/validation_traces-labeled.csv

#Run the desired commands
#########################
# Fit a model with current annotations
python scripts/hyperparam_search_peakdetector.py --data_dir $SFB_DATA --annotations_table $current_annotations --save_dir $SFB_DATA/models/peakdetector

# Fit a model with past annotations
python scripts/hyperparam_search_peakdetector.py --data_dir $SFB_DATA --annotations_table $past_annotations --save_dir $SFB_DATA/models/peakdetector/fit_with_past_annotations
