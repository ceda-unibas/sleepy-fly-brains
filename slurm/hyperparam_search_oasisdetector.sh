#!/bin/bash

#Name of the job
#SBATCH --job-name=hyperparam_search_oasisdetector

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved per core.
#SBATCH --mem-per-cpu=2G

#Time during which the task will run
#SBATCH --time=24:00:00
#SBATCH --qos=1day

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/hyperparam_search_oasisdetector.o
#SBATCH --error=logs/hyperparam_search_oasisdetector.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
annotations=$SFB_DATA/analysis_files/validation_traces-labeled_OLD.csv

#Run the desired commands
#########################
# Fit a model with current annotations
python scripts/hyperparam_search_oasisdetector.py --data_dir $SFB_DATA --annotations_table $annotations --save_dir $SFB_DATA/models/oasis
