#!/bin/bash

#Name of the job
#SBATCH --job-name=evaluate_peakdetector

#Number of CPU cores reserved
#SBATCH --cpus-per-task=4

#Memory reserved.
#SBATCH --mem-per-cpu=32G

#Create array job to execute the same script with different parameters
#SBATCH --array=1-31

#Time during which the task will run
#SBATCH --time=00:30:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/evaluate_peakdetector_%a.o
#SBATCH --error=logs/evaluate_peakdetector_%a.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
array_job_cmds=slurm/evaluate_detector_cmds.sh
model_dir=$SFB_DATA/models/peakdetector
detector="peak"
past_annotations=$SFB_DATA/analysis_files/validation_traces-labeled_OLD.csv
current_annotations=$SFB_DATA/analysis_files/validation_traces-labeled.csv

#Run the desired commands
#########################
eval $(sed -n ${SLURM_ARRAY_TASK_ID}p $array_job_cmds)
