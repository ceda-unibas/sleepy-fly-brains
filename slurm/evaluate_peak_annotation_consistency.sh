#!/bin/bash

#Name of the job
#SBATCH --job-name=evaluate_peak_annotation_consistency

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved
#SBATCH --mem=32G

#Time during which the task will run
#SBATCH --time=00:30:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/evaluate_peak_annotation_consistency.o
#SBATCH --error=logs/evaluate_peak_annotation_consistency.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
past_annotations=$SFB_DATA/analysis_files/validation_traces-labeled_OLD.csv
current_annotations=$SFB_DATA/analysis_files/validation_traces-labeled.csv
save_dir=$SFB_DATA/analysis_files

#Run the desired commands
#########################

# Tolerance 50 ms
python scripts/evaluate_peak_annotation_consistency.py --data_dir $SFB_DATA --save_dir $save_dir --past_annotations $past_annotations --current_annotations $current_annotations --time_tolerance 50 --save_merged_annotations

# Tolerance 100 ms
python scripts/evaluate_peak_annotation_consistency.py --data_dir $SFB_DATA --save_dir $save_dir --past_annotations $past_annotations --current_annotations $current_annotations --time_tolerance 100

# Tolerance 150 ms
python scripts/evaluate_peak_annotation_consistency.py --data_dir $SFB_DATA --save_dir $save_dir --past_annotations $past_annotations --current_annotations $current_annotations --time_tolerance 150

# Tolerance 200 ms
python scripts/evaluate_peak_annotation_consistency.py --data_dir $SFB_DATA --save_dir $save_dir --past_annotations $past_annotations --current_annotations $current_annotations --time_tolerance 200

# Tolerance 250 ms
python scripts/evaluate_peak_annotation_consistency.py --data_dir $SFB_DATA --save_dir $save_dir --past_annotations $past_annotations --current_annotations $current_annotations --time_tolerance 250

# Tolerance 300 ms
python scripts/evaluate_peak_annotation_consistency.py --data_dir $SFB_DATA --save_dir $save_dir --past_annotations $past_annotations --current_annotations $current_annotations --time_tolerance 300
