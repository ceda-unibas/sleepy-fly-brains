# Fit on current; eval on current
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_current --detector $detector --time_tolerance 50 --save_detections --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_current  --detector $detector --time_tolerance 100 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_current --detector $detector --time_tolerance 150 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_current --detector $detector --time_tolerance 200 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_current --detector $detector --time_tolerance 250 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_current --detector $detector --time_tolerance 300 --annotations_table $current_annotations

# Fit on current; eval on past
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_past --detector $detector --time_tolerance 50 --save_detections --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_past  --detector $detector --time_tolerance 100 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_past --detector $detector --time_tolerance 150 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_past --detector $detector --time_tolerance 200 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_past --detector $detector --time_tolerance 250 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir/eval_on_past --detector $detector --time_tolerance 300 --annotations_table $past_annotations

# Fit on past; eval on current
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_current --detector $detector --time_tolerance 50 --save_detections --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_current  --detector $detector --time_tolerance 100 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_current --detector $detector --time_tolerance 150 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_current --detector $detector --time_tolerance 200 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_current --detector $detector --time_tolerance 250 --annotations_table $current_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_current --detector $detector --time_tolerance 300 --annotations_table $current_annotations

# Fit on past; eval on past
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_past --detector $detector --time_tolerance 50 --save_detections --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_past  --detector $detector --time_tolerance 100 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_past --detector $detector --time_tolerance 150 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_past --detector $detector --time_tolerance 200 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_past --detector $detector --time_tolerance 250 --annotations_table $past_annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir/fit_with_past_annotations --save_dir $model_dir/fit_with_past_annotations/eval_on_past --detector $detector --time_tolerance 300 --annotations_table $past_annotations