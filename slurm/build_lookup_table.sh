#!/bin/bash

#Name of the job
#SBATCH --job-name=build_lookup_table

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved per core.
#SBATCH --mem-per-cpu=16G

#Time during which the task will run
#SBATCH --time=0:05:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/build_lookup_table.o
#SBATCH --error=logs/build_lookup_table.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
ANALYSIS_DIR=$SFB_DATA/analysis_files
val_traces_path=$ANALYSIS_DIR/validation_traces-labeled.csv
save_dir=$SFB_DATA/models/lookup

#Run the desired commands
#########################
python scripts/build_lookup_table.py --val_traces_path $val_traces_path --save_dir $save_dir
