#!/bin/bash

#Name of the job
#SBATCH --job-name=evaluate_oasisdetector

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved.
#SBATCH --mem-per-cpu=2G

#Time during which the task will run
#SBATCH --time=00:30:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/evaluate_oasisdetector.o
#SBATCH --error=logs/evaluate_oasisdetector.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
model_dir=$SFB_DATA/models/oasis
detector="oasis"
annotations=$SFB_DATA/analysis_files/validation_traces-labeled_OLD.csv

#Run the desired commands
#########################
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir --detector $detector --time_tolerance 50 --save_detections --annotations_table $annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir --detector $detector --time_tolerance 100 --annotations_table $annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir --detector $detector --time_tolerance 150 --annotations_table $annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir --detector $detector --time_tolerance 200 --annotations_table $annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir --detector $detector --time_tolerance 250 --annotations_table $annotations
python scripts/evaluate_spike_detections.py --data_dir $SFB_DATA --model_dir $model_dir --save_dir $model_dir --detector $detector --time_tolerance 300 --annotations_table $annotations
