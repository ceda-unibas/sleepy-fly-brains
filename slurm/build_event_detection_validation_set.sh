#!/bin/bash

#Name of the job
#SBATCH --job-name=build_event_detection_validation_set

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved per core.
#SBATCH --mem-per-cpu=16G

#Time during which the task will run
#SBATCH --time=0:05:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/build_event_detection_validation_set.o
#SBATCH --error=logs/build_event_detection_validation_set.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
# val_size=0.1  # Use 10% of the data
val_size=1 # Use 100% of the data

#Run the desired commands
#########################
python scripts/build_event_detection_validation_set.py --data_dir $SFB_DATA --save_dir $SFB_DATA/analysis_files --val_size $val_size
