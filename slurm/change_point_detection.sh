#!/bin/bash

#Name of the job
#SBATCH --job-name=change_point_detection

#Number of CPU cores reserved
#SBATCH --cpus-per-task=4

#Memory reserved
#SBATCH --mem=32G

#Create array job
#SBATCH --array=1-2

#Time during which the task will run
#SBATCH --time=24:00:00
#SBATCH --qos=1day

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/change_point_detection_%a.o
#SBATCH --error=logs/change_point_detection_%a.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sfb

#Export the required environment variables
##########################################
array_job_cmds=slurm/change_point_detection_cmds.sh
window_size=10 # Window size (in seconds) for the auto-regressive cost
jump=1         # Subsampling parameter (in seconds) for the CPD algorithm
sigma=0.35     # Scale parameter controling how many change points are detected
# (higher values -> fewer change points))

#Run the desired commands
#########################
eval $(sed -n ${SLURM_ARRAY_TASK_ID}p $array_job_cmds)
