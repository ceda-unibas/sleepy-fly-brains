# sleepy-fly-brains

How does neural activity change when comparing sleep-deprived versus well-rested flies?

[![Project Badge](https://img.shields.io/badge/Project%20status-Ongoing-green)][project-page]

## Installation

Create a new conda environment for this project by running the following on your Terminal:

```sh
conda env create -f environment.yml
```

Activate the new environment with `conda activate sfb` (or `source activate sfb`, depending on your system) and install the necessary dependencies with

```sh
pip install .
```

If you are working in another environment, you can install the package from its GitLab archive with

```sh
pip install git+https://gitlab.com/ceda-unibas/sleepy-fly-brains/-/archive/main/sleepy-fly-brains-main.tar.gz
```

### Additional steps

There may be additional dependencies to run the notebooks and scripts in this repository. Make sure to refer to the respective README files in the [`notebooks`][notebooks] and [`scripts`][scripts] folders for further instructions.

## Usage

The notebooks in [`notebooks`][notebooks] are meant to highlight package functionality while answering specific questions within the context of the project. You can see these notebooks as an examples gallery, usually going from more basic to more advanced topics, hence the numeration. Thus, examples of how to load and display data are usually found in the first notebooks, while more advanced scientific questions are addressed in the later ones.

## Documentation

The documentation for this project is available at [ceda-unibas.gitlab.io/sleepy-fly-brains/](https://ceda-unibas.gitlab.io/sleepy-fly-brains/).

## Contributing

Contributions are welcome, see [CONTRIBUTING][contributing].

## Authors and acknowledgment

This project is a result of a collaboration between [CeDA][ceda] and [Anissa Kempf's research group][kempf] at the University of Basel.

[Cascade software][cascade] is the work of [Rupprecht et al. 2021][cascade-paper]:

> Rupprecht P, Carta S, Hoffmann A, Echizen M, Blot A, Kwan AC, Dan Y, Hofer SB, Kitamura K, Helmchen F, Friedrich RW, _A database and deep learning toolbox for noise-optimized, generalized spike inference from calcium imaging_, Nature Neuroscience (2021).

The OASIS algorithm is the work of [Friedrich et al. 2017][oasis-paper]:

> Friedrich, J., Zhou, P., & Paninski, L. (2017). Fast online deconvolution of calcium imaging data. PLoS Computational Biology, 13(3), e1005423. [doi.org/10.1371/journal.pcbi.1005423](https://doi.org/10.1371/journal.pcbi.1005423)

## License

This project is released under the [BSD 3-Clause License][license].

[cascade]: https://github.com/HelmchenLabSoftware/Cascade
[cascade-paper]: https://www.nature.com/articles/s41593-021-00895-5
[ceda]: https://ceda.unibas.ch/
[docs]: https://ceda-unibas.gitlab.io/sleepy-fly-brains/
[contributing]: CONTRIBUTING.md
[kempf]: https://www.biozentrum.unibas.ch/research/research-groups/research-groups-a-z/overview/unit/research-group-anissa-kempf
[license]: LICENSE
[oasis-paper]: https://pmc.ncbi.nlm.nih.gov/articles/PMC5370160/
[notebooks]: https://gitlab.com/ceda-unibas/sleepy-fly-brains/-/tree/main/notebooks
[scripts]: https://gitlab.com/ceda-unibas/sleepy-fly-brains/-/tree/main/scripts
[project-page]: https://ceda.unibas.ch/project_sleepy_fly_brains.html
