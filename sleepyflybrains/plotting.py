"""Plotting module"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from oasis.plotting import simpleaxis


def format_x_axis(fig, x_vals, n_ticks=10):
    """Format the x axis of a plotly figure tot get evenly spaced ticks

    Parameters
    ----------
    fig : plotly Figure
        The figure to format
    x_vals : array-like
        The values of the x axis
    n_ticks : int, optional
        Number of ticks to show on the x axis, by default 10

    Returns
    -------
    plotly Figure
        The figure with the x axis formatted
    """
    # Set tickvals to n_ticks evenly sampled points from `x_vals`
    tmp = np.unique(x_vals)
    tickvals = tmp[0 :: int(len(tmp) / n_ticks)]

    # If the elements in `x_vals` are float, format them to 2 decimal places.
    # Otherwise, let ricktext be the same as tickvals
    ticktext = [f"{t:.2f}" if isinstance(t, float) else t for t in tickvals]

    # Update the x axis
    fig.update_xaxes(
        tickangle=-45,
        tickmode="array",
        tickvals=tickvals,
        ticktext=ticktext,
    )
    return fig


def _ts_in_parallel(df, times_column_name, series_label, scatter):
    new_df = df.set_index(times_column_name)
    new_df.index.name = times_column_name
    new_df.columns.name = series_label
    if scatter:
        # If `scatter` is True, plot the time-series as scatter plots
        fig = px.scatter(
            new_df,
            error_y=[0] * len(new_df),
            error_y_minus=[0] * len(new_df),
            facet_row=series_label,
            facet_row_spacing=0.01,
            height=300,
            width=600,
        )
    else:
        # Otherwise, plot them as lines
        fig = px.line(
            new_df,
            facet_row=series_label,
            facet_row_spacing=0.01,
            height=300,
            width=600,
        )

    # hide and lock down axes
    fig.update_yaxes(visible=False, fixedrange=True)

    # Remove facet/subplot labels
    fig.update_layout(annotations=[], overwrite=True)

    # Strip down the rest of the plot
    fig.update_layout(
        showlegend=False, plot_bgcolor="white", margin=dict(t=40, l=10, b=10, r=10)
    )
    return fig


def _ts_on_top_of_each_other(df, times_column_name, scatter):
    # Get the column names of the data frame
    column_names = df.columns
    column_names = [name for name in column_names if name not in [times_column_name]]
    # Otherwise, overlay the time-series onto single plot
    if scatter:
        # If `scatter` is True, plot the time-series as scatter plots
        fig = go.Figure()
        for column in column_names:
            fig.add_trace(
                go.Scatter(
                    name=column,
                    x=df[times_column_name],
                    y=df[column],
                    error_y=dict(
                        type="percent",
                        symmetric=False,
                        value=0,
                        valueminus=100,
                        visible=True,
                    ),
                )
            )
    else:
        # Otherwise, plot them as lines
        fig = px.line(df, x=times_column_name, y=column_names)
    return fig


def plot_time_series(
    df,
    times_column_name="Times",
    series_label="Series",
    parallel=False,
    format_times=True,
    scatter=False,
    **kwargs,
):
    """Plot time series with plotly

    Parameters
    ----------
    df : pandas DataFrame
        The data to plot. Should have a column with the times and one or more
        columns with the values
    times_column_name : string, optional
        Name of the column with the times, by default "Times"
    series_label : string, optional
        Name of the column with the series labels, by default 'Series'
    parallel : bool, optional
        Make a parallel plot of the series, by default False.
    format_times : bool, optional
        Format the x axis, by default True
    scatter : bool, optional
        Plot the time series as scatter plots, by default False
    kwargs : dict, optional
        Extra keyword arguments to pass to `plotly.Figure.update_layout`

    Returns
    -------
    plotly Figure
        The plot object
    """
    if parallel:
        # If `parallel` is True, plot the time-series as parallel plots
        fig = _ts_in_parallel(df, times_column_name, series_label, scatter)
    else:
        fig = _ts_on_top_of_each_other(df, times_column_name, scatter)
    if format_times:
        # Format the x axis
        fig = format_x_axis(fig, df[times_column_name])
    fig.update_layout(**kwargs)
    return fig


def _seqs_to_df(seqs, index, seq_names):
    try:
        seqs = seqs.toarray()
    except AttributeError:
        pass
    # If `seqs` is one dimensional, make it two dimensional by adding a dummy
    # dimension
    if len(seqs.shape) == 1:
        seqs = seqs[np.newaxis, :]
    return pd.DataFrame(seqs, index=seq_names).transpose()


def _parse_index_and_label(index_label, index, df):
    if index_label is None:
        index_label = "Index"
    if index is not None:
        df[index_label] = index
    else:
        df[index_label] = np.arange(len(df))
    return index_label


def plot_sequences(
    seqs,
    seq_names=None,
    index=None,
    index_label=None,
    seqlabel=None,
    parallel=True,
    format_horizontal_axis=False,
    scatter=False,
    **kwargs,
):
    """Plot sequences of numbers with plotly

    Parameters
    ----------
    seqs : array-like
        Array of number sequences. It is interpreted to have one sequence per
        row
    seq_names : array-like, optional
        Names of the individual sequences, by default None
    index : list
        List of elements by which to index the sequences. Should be the same
        length as the rows in `seqs`. Defaults to
        `[0, 1, 2, ..., len(seqs[0, :])]`
    index_label : string, optional
        Label of the horizontal axis, by default 'Index'
    seqlabel : string, optional
        Label for what a sequence represents, by default 'Sequence'
    parallel : bool, optional
        Make a parallel plot of sequences, by default True. If False, the
        sequences will be overlaid to one another.
    scatter : bool, optional
        Plot the time series as scatter plots, by default False. If False, the
        sequences will be plotted as plotly lines.
    kwargs : dict, optional
        Extra keyword arguments to pass to `plotly.Figure.update_layout`

    Returns
    -------
    plotly Figure
        The plot object

    Notes
    -----
    If `seqs` is a sparse matrix, it will be converted to a dense matrix
    before plotting. The sequences will then also be plotted as scatter plots,
    instead of lines.
    """
    # Turn array to dataframe for plotting
    df = _seqs_to_df(seqs, index, seq_names)
    # If `index` is not provided, make one
    index_label = _parse_index_and_label(index_label, index, df)
    return plot_time_series(
        df,
        times_column_name=index_label,
        series_label="Sequence" if seqlabel is None else seqlabel,
        parallel=parallel,
        format_times=format_horizontal_axis,
        scatter=scatter,
        **kwargs,
    )


def plot_detections(
    signal,
    detection_time_steps,
    sample_rate=None,
    signal_label="Signal",
    detection_label="Detection",
):
    """Plot a signal with detections overlaid

    Parameters
    ----------
    signal : array-like
        The signal to plot
    detection_time_steps : array-like
        The time-steps of the detections. A red cross will be plotted at these
        times, at the corresponding signal value.
    sample_rate : int, optional
        The sample rate of the signal, by default None. If provided, the x
        axis will be labeled in seconds, instead of samples.
    signal_label : string, optional
        Label of the signal, by default 'Signal'
    detection_label : string, optional
        Label of the detections, by default 'Detection'

    Returns
    -------
    plotly Figure
        The plot object
    """
    fig = go.Figure()

    x = np.arange(len(signal))
    detection_times = detection_time_steps
    xlabel = "Samples"
    if sample_rate is not None:
        x = x / sample_rate
        detection_times = detection_time_steps / sample_rate
        xlabel = "Time [s]"

    fig.add_trace(
        go.Scatter(
            y=signal,
            x=x,
            mode="lines+markers",
            name=signal_label,
        )
    )

    fig.add_trace(
        go.Scatter(
            x=detection_times,
            y=[signal[j] for j in detection_time_steps],
            mode="markers",
            marker=dict(size=8, color="red", symbol="cross"),
            name=detection_label,
        )
    )

    fig.update_layout(
        xaxis_title=xlabel,
    )

    return fig


def plot_spike_deconvolution(
    trace,
    denoised_trace,
    estimated_spike_train,
    true_denoised_trace=None,
    true_spikes=None,
):
    """Plot the results of a spike deconvolution

    Parameters
    ----------
    trace : array-like
        The measured trace
    denoised_trace : array-like
        The denoised trace
    estimated_spike_train : array-like
        The estimated spike train
    true_denoised_trace : array-like, optional
        The true denoised trace, by default None
    true_spikes : array-like, optional
        The true spike train, by default None

    Returns
    -------
    Figure, Axes
        `matplotlib` plot objects

    Notes
    -----
    This function is a slight modification of `plot_trace` defined at
    https://github.com/j-friedrich/OASIS/blob/master/examples/Demo.ipynb
    """
    # Create figure object
    fig, ax = plt.subplots(2, 1, figsize=(20, 4))

    # Plot the denoised trace
    ax[0].plot(denoised_trace, lw=2, label="denoised")

    # Plot the true denoised trace, if provided
    if true_denoised_trace is not None:
        ax[0].plot(true_denoised_trace, c="r", label="truth", zorder=-11)

    # Plot the measured (noisy) trace
    ax[0].plot(trace, label="data", zorder=-12, c="y")

    # Cosmetics
    ax[0].legend(ncol=3, frameon=False, loc=(0.02, 0.85))
    simpleaxis(ax[0])

    # Plot the estimated spike train
    ax[1].plot(estimated_spike_train, lw=2, label="deconvolved", c="g")

    if true_spikes is not None:
        for k in np.where(true_spikes)[0]:
            plt.plot([k, k], [-0.1, 1], c="r", zorder=-11, clip_on=False)

    # Cosmetics
    ax[1].set_ylim([0, 1.3])
    ax[1].legend(ncol=3, frameon=False, loc=(0.02, 0.85))
    simpleaxis(plt.gca())

    return fig, ax
