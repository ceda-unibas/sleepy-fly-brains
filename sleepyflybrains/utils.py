"""Utilities module"""

import numpy as np


def count_correct_and_incorrect_peaks(gt_peaks, predicted_peaks, tolerance_in_samples):
    """Count true positives, false positives, false negatives, and true
    negatives

    Parameters
    ----------
    gt_peaks : array-like
        Binary array of ground-truth peaks, with length #Time-steps. Should
        contain only zeros and ones. The ones indicate the time-steps where
        peaks were detected
    predicted_peaks : array-like
        Binary array of predicted peaks, with length #Time-steps. Should
        contain only zeros and ones. The ones indicate the time-steps where
        peaks were detected
    tolerance_in_samples : int
        Delay or advance (in number of samples) tolerated when matching
        predicted peaks with ground-truth peaks

    Returns
    -------
    TP : int
        Number of true positives
    FP : int
        Number of false positives
    FN : int
        Number of false negatives

    Notes
    -----
    The true positive matches are found within a tolerance window, just as in
    [Liu et al. 2018]
    """
    # Get true and predicted peak locations from the binary arrays
    true_locations = np.argwhere(gt_peaks == 1).flatten()
    predicted_locations = np.argwhere(predicted_peaks == 1).flatten()

    # Create iterators for the true and predicted peak locations
    true_locations_iter = iter(true_locations)
    predicted_locations_iter = iter(predicted_locations)

    # Go over the true and predicted peak locations, counting matches within a
    # tolerance window
    TP = 0
    try:
        t1 = next(true_locations_iter)
        t2 = next(predicted_locations_iter)
        while True:
            if abs(t1 - t2) <= tolerance_in_samples:
                # Match found, so increment TP
                TP += 1

                # Move to next location in both iterators
                t1 = next(true_locations_iter)
                t2 = next(predicted_locations_iter)
            elif t1 < t2:
                # Move to next true location to see if it matches the current
                # predicted location
                t1 = next(true_locations_iter)
            else:
                # Move to next predicted location to see if it matches the
                # current true location
                t2 = next(predicted_locations_iter)
    except StopIteration:
        # One of the iterators has reached the end, so we can stop
        pass

    # False negatives are the number of true peaks that were not detected
    FN = len(true_locations) - TP

    # False positives are the number of predicted peaks that were not real
    FP = len(predicted_locations) - TP

    return TP, FP, FN


def set_param_from_dict(obj, param_dict, param_name):
    """Set parameter value from a dictionary if it exists

    Parameters
    ----------
    param_dict : dict
        Dictionary containing the parameter value
    param_name : str
        Name of the parameter

    Notes
    -----
    If the parameter does not exist in the dictionary, the attribute is set
    to None.
    """
    try:
        setattr(obj, param_name, param_dict[param_name])
    except KeyError:
        setattr(obj, param_name, None)
