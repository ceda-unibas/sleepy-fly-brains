"""Time-series processing module"""

import numpy as np
import pandas as pd
from ot import wasserstein_1d
from pybaselines import Baseline
from scipy.signal import welch


def compute_baseline(fluorescence_traces, method, **kwargs):
    """Compute baseline of a set of fluorescence traces

    Parameters
    ----------
    fluorescence_traces : array-like
        Array of fluorescence traces. The implied dimensions are #ROIs x
        #Time-steps
    method : str
        Method to use for computing the baseline. Should be an attribute of
        `pybaselines.Baseline`. See the `pybaselines` documentation for options
    kwargs : dict, optional
        Additional keyword arguments to pass to the baseline fitter.
        See the specific algorithm's entry in the `pybaselines`
        documentation for options

    Returns
    -------
    array-like
        Array of baselines. It has the same dimensions as `fluorescence_traces`
    """
    n_rois, n_times = fluorescence_traces.shape
    baseline_fitter = Baseline(np.arange(n_times), check_finite=False)
    baseline_algo = getattr(baseline_fitter, method)
    baselines = []
    for i in range(n_rois):
        baseline, _ = baseline_algo(fluorescence_traces[i, :], **kwargs)
        baselines.append(baseline)
    baselines = np.vstack(baselines)
    return baselines


def correct_baseline(fluorescence_traces, baselines):
    """Correct baseline of a set of fluorescence traces

    Parameters
    ----------
    fluorescence_traces : array-like
        Array of fluorescence traces. The implied dimensions are #ROIs x
        #Time-steps
    baselines : array-like
        Array of baselines. It has the same dimensions as
        `fluorescence_traces`. See `compute_baseline`

    Returns
    -------
    array-like
        Array of baseline-corrected fluorescence traces. It has the same
        dimensions as `fluorescence_traces`

    Notes
    -----
    This function computes the relative fluorescence change (dF/F) by subtracting
    then dividing the estimated baselines
    """
    correct_traces = fluorescence_traces - baselines  # dF
    correct_traces /= baselines  # dF/F
    return correct_traces


def remove_measurement_artifacts(functional_traces, nonfunctional_traces):
    """Remove nonfunctional signal from functional signal

    Parameters
    ----------
    functional_traces : array-like
        Array of functional traces. The implied dimensions are #ROIs x #Time-steps
    nonfunctional_traces : array-like
        Array of nonfunctional traces. The implied dimensions are #ROIs x #Time-steps

    Returns
    -------
    array-like
        Array of cleaned functional traces. It has the same dimensions as
        `functional_traces`

    Notes
    -----
    This function removes nonfunctional signal from functional signal by
    computing the best scaling coefficient to remove nonfunctional signal from
    functional signal. It then subtracts the nonfunctional signal from the
    functional signal
    """
    # Make the minimum value per ROI zero
    min_functional_val = np.min(functional_traces, axis=1)[None].T
    functional_traces -= min_functional_val
    min_nonfunctional_val = np.min(nonfunctional_traces, axis=1)[None].T
    nonfunctional_traces -= min_nonfunctional_val

    # Compute best scaling coefficient to remove nonfunctional signal from functional
    # signal
    n_rois, _ = functional_traces.shape
    alpha = np.zeros((n_rois,))
    for i in range(n_rois):
        # This alpha minimizes the L2 distances
        # ||F_functional[i, :] - alpha * F_nonfunctional[i, :]||_2
        alpha[i] = np.inner(
            nonfunctional_traces[i, :], functional_traces[i, :]
        ) / np.inner(nonfunctional_traces[i, :], nonfunctional_traces[i, :])

    # Remove nonfunctional signal from functional signal
    cleaned_traces = functional_traces - np.diag(alpha) @ nonfunctional_traces

    # Add back the minimum value per ROI
    cleaned_traces += min_functional_val

    return cleaned_traces


def intermittency_measure(window, intermitency_fun=None):
    """Measure of intermitency between the left and right halves of a window of
    a stack of time series

    Parameters
    ----------
    window : array-like of shape (N, T)
        A T-wide window of a stack of N time series
    intermitency_fun : function, optional
        Function defining the intermitency between left and right half windows.
        Must be called as `intermitency_fun(left_half, right_half)`. If None,
        the 1-Wasserstein distance is used, by default None

    Returns
    -------
    float
        Measure of intermitency between the left and right halves of the window
    """
    # Split array into left and right halves
    left_half = window[:, : window.shape[1] // 2]
    right_half = window[:, window.shape[1] // 2 :]

    # Parse intermitency function
    if intermitency_fun is None:
        return wasserstein_1d(left_half.T, right_half.T)
    else:
        return intermitency_fun(left_half, right_half)


def rolling_mean(session_traces, window_size=1, min_num_obs=None, std=None):
    """Compute rolling mean of a set of traces

    Parameters
    ----------
    session_traces : array-like
        Array of traces. The implied dimensions are #ROIs x #Time-steps
    window_size : int, optional
        Size of the rolling window in time steps, by default 1
    min_num_obs : int, optional
        Minimum number of observations in window required to have a value, by
        default None. If None, it will be set to `window_size / 2`
    std : float, optional
        Standard deviation of the Gaussian window used to weight the window,
        by default None. If None, it will be set to `window_size / 5.`

    Returns
    -------
    array-like
        Array of rolling means. It has the same dimensions as `session_traces`.
        The leading `min_num_obs` columns will be comprised of NaN values.

    See also
    --------
    pandas.DataFrame.rolling
    """

    try:
        session_traces = session_traces.toarray()
    except AttributeError:
        pass

    # Minimum number of observations in window required to have a value
    min_num_obs = int(window_size / 2) if min_num_obs is None else min_num_obs

    # A Gaussian window with std=window_size/5. seems to work well for
    # smoothing out binary traces
    std = window_size / 5.0 if std is None else std

    # Create a dataframe with the individual ROI traces as columns
    df = pd.DataFrame(
        session_traces.T,
    )

    # Compute the rolling mean with a Gaussian window
    rolling_mean = df.rolling(
        window=window_size,
        win_type="gaussian",
        min_periods=min_num_obs,
    ).mean(std=std)

    return rolling_mean.to_numpy().T


def compute_psds(traces, sample_rates, window_size_in_seconds):
    """Compute power spectral densities (PSDs) of traces

    Parameters
    ----------
    traces : dict
        Time-series data. Keys are session IDs and values are the corresponding
        time-series data. See output of `sleepyflybrains.dataset.get_traces`
    sample_rates : dict
        Sample rates (Hz) of the time-series data. Keys are session IDs and
        the values are the corresponding sample rates
    window_size_in_seconds : float
        Size of the window (in seconds) to use for computing the PSD

    Returns
    -------
    tuple
        Tuple of two elements:
            - `freqs`: dictionary of frequencies. Keys are session IDs and values are
                the corresponding frequencies
            - `psds`: dictionary of PSDs. Keys are session IDs and values are the
                corresponding PSDs

    Notes
    -----
    The PSDs are computed using the Welch method implemented in `scipy.signal`
    """
    # Loop over sessions, computing PSDs
    psds = {}
    freqs = {}
    for session_id, array in traces.items():
        try:
            # Array in traces could be a sparse matrix, so we need to convert
            # it to a dense array
            array = array.toarray()
        except AttributeError:
            pass

        # Get window size in number of samples
        window_size = int(window_size_in_seconds * sample_rates[session_id])

        # Compute PSD
        session_freqs, Pxx_den = welch(
            array,
            fs=sample_rates[session_id],
            nperseg=window_size,
            detrend="constant",
        )

        psds[session_id] = Pxx_den
        freqs[session_id] = session_freqs

    return freqs, psds
