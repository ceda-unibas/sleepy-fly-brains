import json
import os

import numpy as np
import oasis
import pandas as pd
import ruamel.yaml as yaml
from scipy.ndimage import gaussian_filter1d
from scipy.signal import find_peaks

from sleepyflybrains import utils
from sleepyflybrains.third_party.cascade2p import cascade
from sleepyflybrains.third_party.cascade2p.utils_discrete_spikes import (
    infer_discrete_spikes,
)


class SpikeDetector:
    """Base class for spike detectors from calcium fluorescence traces

    Notes
    -----
    This class is not meant to be used directly, but rather to be inherited by
    child classes implementing specific spike detection algorithms.
    """

    def __init__(self) -> None:
        pass

    def predict(self, dFF, **kwargs) -> np.ndarray:
        # Should be implemented by the child class
        raise NotImplementedError

    def predict_discrete(self, dFF, *args, **kwargs) -> np.ndarray:
        """Predict binary spikes from an array of baseline-corrected
        calcium fluorescence traces

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are #ROIs x #Time-steps
        args : list, optional
            Positional arguments to be passed to the `find_spikes` method
        kwargs : dict, optional
            Keyword arguments to be passed to the `find_spikes` method

        Returns
        -------
        array-like
            Array of binary spikes. It has the same dimensions as `dFF`, with
            1s indicating the presence of a spike and 0s indicating the
            absence of a spike. The presence of a spike is determined by the
            `find_spikes` method.
        """
        # Find spikes
        spikes = self.find_spikes(dFF, *args, **kwargs)

        # Make sure elements in spikes are lists of integers
        spikes = [list(map(int, row)) for row in spikes]

        # Generate matrix with discrete spikes
        discrete_spikes = np.zeros_like(dFF)

        # Make sure traces have dimensions #ROIs x #Time-steps, even if the
        # user inputs the flattened trace of a single ROI
        if len(discrete_spikes.shape) == 1:
            discrete_spikes = discrete_spikes.reshape(1, -1)

        for i, row in enumerate(spikes):
            discrete_spikes[i, row] = 1.0

        return discrete_spikes

    def find_spikes(self, dFF, **kwargs) -> list:
        # Should be implemented by the child class
        raise NotImplementedError


class PeakDetector(SpikeDetector):
    """Spike detector based on `scipy.signal.find_peaks`

    This class implements a spike detector that uses the `find_peaks` function
    from the `scipy.signal` module to detect spikes in baseline-corrected
    calcium fluorescence traces.

    Attributes
    ----------
    prominence : float or None
        Prominence of peaks used for spike detection
    distance : float or None
        Minimum distance between peaks used for spike detection
    width : float or None
        Width of peaks used for spike detection
    rel_height : float or None
        Relative height of peaks used for spike detection
    filter_sigma : float or None
        Sigma parameter for the Gaussian denoising filter
    filter_radius : float or None
        Radius parameter for the Gaussian denoising filter
    """

    def __init__(self, model_dir=None) -> None:
        super().__init__()
        # Load peak-finding parameters from a model directory and set them as
        # attributes
        self._load_params(model_dir)

    def _load_params(self, model_dir):
        """Load peak-finding parameters from a model directory

        Parameters
        ----------
        model_dir : str
            Path to the model directory

        Returns
        -------
        dict
            Dictionary of peak-finding parameters
        """
        if model_dir is not None:
            # Load `best_params.json` file from model_dir
            with open(os.path.join(model_dir, "best_params.json")) as file:
                best_params = json.load(file)
        else:
            best_params = {}

        # Set peak-finding parameters from loaded dictionary
        utils.set_param_from_dict(self, best_params, "prominence")
        utils.set_param_from_dict(self, best_params, "distance")
        utils.set_param_from_dict(self, best_params, "width")
        utils.set_param_from_dict(self, best_params, "rel_height")
        utils.set_param_from_dict(self, best_params, "filter_sigma")
        utils.set_param_from_dict(self, best_params, "filter_radius")

    def _default_denoising_filter(self, sample_rate):
        """Default denoising filter

        The default denoising filter is a Gaussian filter with sigma equal to
        `self.filter_sigma` times the sample rate, and a radius equal to
        `self.filter_radius` times the sample rate.

        Returns
        -------
        function
            Default denoising filter

        Notes
        -----
        The values of `self.filter_sigma` and `self.filter_radius` are read
        from the `best_params.json` file in the model directory provided to the
        class constructor. If no model directory is provided, the default
        values are `None`, which means that no denoising filter will be
        applied (i.e. the identity function will be returned)
        """
        # Parse filter parameters
        if self.filter_sigma is None:
            # Return identity function because Gaussian filter is undefined
            return lambda x: x
        if self.filter_radius is None:
            radius = None
        else:
            radius = int(self.filter_radius * sample_rate)

        # Return Gaussian denoising filter
        return lambda x: gaussian_filter1d(
            x,
            sigma=self.filter_sigma * sample_rate,
            mode="nearest",
            radius=radius,
            axis=-1,
        )

    def predict(self, dFF, **kwargs):
        """Dummy method to comply with the parent class

        This function does the same as `predict_discrete`

        See Also
        --------
        predict_discrete
        """
        # Dummy method to comply with the parent class
        return self.predict_discrete(dFF, **kwargs)

    def _set_params_from_sample_rate(self, sample_rate, **kwargs):
        prominence = kwargs.get("prominence", None)
        distance = kwargs.get("distance", None)
        width = kwargs.get("width", None)
        rel_height = kwargs.get("rel_height", None)
        denoising_filter = kwargs.get("denoising_filter", None)

        if sample_rate is not None:
            prominence = self.prominence if prominence is None else prominence
            distance = self.distance if distance is None else distance
            distance += sample_rate
            width = self.width if width is None else width
            width *= sample_rate
            rel_height = self.rel_height if rel_height is None else rel_height
            denoising_filter = (
                self._default_denoising_filter(sample_rate)
                if denoising_filter is None
                else denoising_filter
            )
        return prominence, distance, width, rel_height, denoising_filter

    def find_spikes(
        self,
        dFF,
        sample_rate=None,
        **kwargs,
    ):
        """Find spike times from an array of baseline-corrected calcium
        fluorescence traces

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are #ROIs x #Time-steps
        sample_rate : float, optional
            Sampling rate (Hz) of the time-series, by default None. If
            provided, the peak-finding parameters from the model loaded in the
            class constructor will be used. Parameters such as `distance` and
            `width`, which are recorded in seconds in the model, will be
            converted to the corresponding number of samples according to the
            provided sample rate. If None, the peak-finding parameters will be
            used as they are provided to the method
        kwargs : dict, optional
            Here you can optionally bypass the peak-finding parameters from the
            model by providing them as keyword arguments. The available
            parameters are `prominence`, `distance`, `width`, `rel_height`, and
            `denoising_filter`. If `sample_rate` is provided, distance and width should be supplied in the units of seconds. Otherwise, they should be supplied in units of samples. (If `denoising_filter` is provided, it should be
            a function that takes an array of traces and returns the denoised
            traces

        Returns
        -------
        list
            List of spike times for each ROI

        Notes
        -----
        For more information on the parameters, see the documentation of
        `scipy.signal.find_peaks`

        See Also
        --------
        scipy.signal.find_peaks
        scipy.ndimage.gaussian_filter1d
        """
        # Set peak-finding parameters according to the sample rate
        prominence, distance, width, rel_height, denoising_filter = (
            self._set_params_from_sample_rate(
                sample_rate,
                **kwargs,
            )
        )

        # Make sure traces have dimensions #ROIs x #Time-steps, even if the
        # user inputs the flattened trace of a single ROI
        if len(dFF.shape) == 1:
            dFF = dFF.reshape(1, -1)

        # Denoise traces
        if denoising_filter is not None:
            dFF = denoising_filter(dFF)

        spikes = []
        # Loop over ROIs and find peaks
        for _, row in enumerate(dFF):
            peaks, _ = find_peaks(
                row,
                prominence=prominence,
                distance=distance,
                width=width,
                rel_height=rel_height,
            )
            spikes.append(peaks)
        return spikes


class CascadeDetector(SpikeDetector):
    """Cascade spike detector class

    Based on Cascade models, this class implements methods to detect spikes
    (binary or probabilistic) from baseline corrected calcium fluorescence
    traces

    Parameters
    ----------
    models_dir : str
        Path to the directory where to load/save the predictive models and
        ancillary files. If the directory provided does not exist, the
        class constructor will create it.

    Attributes
    ----------
    models_dir : str
        Path to the directory where to load/save the predictive models and
        ancillary files
    model_name : str
        Name of the Cascade predictive model to use
    model_sample_rate : float
        Sample rate (Hz) of the Cascade predictive model that was chosen

    Notes
    -----
    The original Cascade software can be found on
    https://github.com/HelmchenLabSoftware/Cascade and is the work of
    :cite:`rupprecht2021a`
    """

    def __init__(self, models_dir) -> None:
        self.models_dir = models_dir
        # Create new directory if the provided path does not contain one
        # already
        os.makedirs(self.models_dir, exist_ok=True)

        # Set default model
        self.set_model()

    def download(self, name="update_models", verbose=0):
        """Download Cascade model or list of available models

        Parameters
        ----------
        name : str, optional
            Name of the model to download, by default `'update_models'`. The
            default option downloads a list of available models to the path in
            `self.models.dir`.
        verbose : int, optional
            Level of verbosity, by default 0. The higher the value, the more
            verbose are the output messages
        """
        cascade.download_model(name, model_folder=self.models_dir, verbose=verbose)

    def get_available_models_list(self, force_update=False, **kwargs):
        """Get list of Cascade available models

        Parameters
        ----------
        force_update : bool, optional
            Force update of the available models file by re-downloading
            `available_models.yaml`, by default False

        Returns
        -------
        list
            List of Cascade available models
        """
        if force_update:
            # Force download `available_models.yaml` to the models directory
            self.download(**kwargs)
        try:
            yaml_obj = yaml.YAML(typ="rt")
            with open(os.path.join(self.models_dir, "available_models.yaml")) as file:
                available_models = list(yaml_obj.load(file))
        except FileNotFoundError:
            available_models = self.get_available_models_list(
                force_update=True, **kwargs
            )
        return available_models

    def set_model(
        self,
        model_name="Global_EXC_30Hz_smoothing50ms_causalkernel",
        force_download=False,
        verbosity=0,
    ):
        """Set Cascade model to use

        Parameters
        ----------
        model_name : str, optional
            Name of the Cascade predictive model to use, by default
            'Global_EXC_30Hz_smoothing50ms_causalkernel'
        force_download : bool, optional
            Force the model to be downloaded to the `self.models.dir` path,
            even if it already exists there, by default False
        verbosity : int, optional
            Level of verbosity, by default 0. The higher the value, the more
            verbose are the output messages
        """
        self.model_name = model_name
        if force_download:
            # Force download the model to the models directory
            self.download(name=model_name, verbose=verbosity)
        # Get model sample rate from the model name. The model name is a
        # string of substrings separated by underscores. The sample rate is
        # the one substring with the 'Hz' suffix
        self.model_sample_rate = float(
            [
                substring
                for substring in model_name.split("_")
                if substring.endswith("Hz")
            ][0][:-2]
        )

    def predict(self, dFF, sample_rate, verbosity=0):
        """Predict spike probabilities from an array of baseline-corrected
        calcium fluorescence traces

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are #ROIs x #Time-steps
        model_name : str, optional
            Name of the Cascade predictive model to use, by default
            'Global_EXC_30Hz_smoothing50ms_causalkernel'
        force_download : bool, optional
            Force the model to be downloaded to the `self.models.dir` path,
            even if it already exists there, by default False
        verbosity : int, optional
            Level of verbosity, by default 0. The higher the value, the more
            verbose are the output messages

        Returns
        -------
        array-like
            Array of spike probability signals. It has the same dimensions as
            `dFF`

        Notes
        -----
        For guidelines on how to select an appropriate predictive model for
        your data, see the FAQ here:
        https://github.com/HelmchenLabSoftware/Cascade#frequently-asked-questions
        """
        # Make sure traces have dimensions #ROIs x #Time-steps, even if the
        # user inputs the flattened trace of a single ROI
        if len(dFF.shape) == 1:
            dFF = dFF.reshape(1, -1)

        # Downsample traces to approximately the model's sample rate (HZ)
        dFF = dFF[:, :: int(sample_rate / self.model_sample_rate)]

        try:
            spike_probs = cascade.predict(
                self.model_name, dFF, model_folder=self.models_dir, verbosity=verbosity
            )
        except Exception:
            # If the model is not found, download it and try again
            self.set_model(
                model_name=self.model_name, force_download=True, verbosity=verbosity
            )
            spike_probs = cascade.predict(
                self.model_name, dFF, model_folder=self.models_dir, verbosity=verbosity
            )

        # The beginning and end of the spike_probs arrays are filled with NaNs.
        # Replace them with zeros
        spike_probs[np.isnan(spike_probs)] = 0.0

        # Upsample spike probabilities to the original sample rate
        spike_probs = np.repeat(
            spike_probs, int(sample_rate / self.model_sample_rate), axis=1
        )

        return spike_probs

    def find_spikes(self, dFF, sample_rate, verbosity=0):
        """Find spike times from an array of baseline-corrected calcium
        fluorescence traces

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are
            #ROIs x #Time-steps
        verbosity : int, optional
            Level of verbosity, by default 0. The higher the value, the more
            verbose are the output messages

        Returns
        -------
        list
            List of spike times for each ROI

        Notes
        -----
        Binary spike prediction is recommended only for very high quality
        calcium fluorescence measurements. For more on this, check the FAQ in
        https://github.com/HelmchenLabSoftware/Cascade#frequently-asked-questions
        and the accompanying paper.
        """
        # Get spike probabilities first
        spike_probs = self.predict(dFF, sample_rate, verbosity=verbosity)

        # Predict spikes from probability kernels
        _, spikes = infer_discrete_spikes(
            spike_probs,
            model_name=self.model_name,
            model_folder=self.models_dir,
            verbosity=verbosity,
        )
        return spikes


class OASISDetector(SpikeDetector):
    """Spike detector based on OASIS deconvolution

    Parameters
    ----------
    model_dir : str, optional
        Path to the directory where to load the OASIS parameters from, by
        default None. If None, default parameters will be used

    Attributes
    ----------
    optimize_g : int
        How many of the large isolated calcium events to use for figuring out
        the decay time constant
    penalty : float
        Penalty norm to use (0- or 1-norm) in the OASIS deconvolution algorithm
    threshold : float
        Threshold for binarizing the spike trains returned by the OASIS
        deconvolution function. Numbers in the spike trains above this
        threshold will be set to 1, and numbers below it will be set to 0
    max_iter : int
        Maximum number of iterations to run the OASIS deconvolution algorithm
    sn_factor : float
        Factor by which to exponentiate the noise level estimation to better
        distinguish low- and high-SNR traces

    Notes
    -----
    The OASIS deconvolution algorithm is described in :cite:`friedrich2017`
    """

    def __init__(self, model_dir=None) -> None:
        super().__init__()
        # Load internal parameters from a model directory and set them as
        # attributes
        self._load_params(model_dir)

    def _load_params(self, model_dir):
        """Load parameters from a model directory

        Parameters
        ----------
        model_dir : str
            Path to the model directory
        """
        if model_dir is not None:
            # Load `best_params.json` file from model_dir
            with open(os.path.join(model_dir, "best_params.json")) as file:
                best_params = json.load(file)
        else:
            best_params = {}

        # Set OASIS parameters from loaded dictionary
        utils.set_param_from_dict(self, best_params, "optimize_g")
        utils.set_param_from_dict(self, best_params, "penalty")
        utils.set_param_from_dict(self, best_params, "threshold")
        utils.set_param_from_dict(self, best_params, "max_iter")
        utils.set_param_from_dict(self, best_params, "sn_factor")

        # Set default attribute values if they are not set
        self.optimize_g = 0 if self.optimize_g is None else self.optimize_g
        self.penalty = 0 if self.penalty is None else self.penalty
        self.max_iter = 5 if self.max_iter is None else self.max_iter
        self.threshold = (
            np.finfo(float).eps if self.threshold is None else self.threshold
        )
        self.sn_factor = 1.0 if self.sn_factor is None else self.sn_factor

    def _deconvolve_roi_trace(
        self,
        row,
        **kwargs,
    ):
        # Set OASIS parameters
        optimize_g, penalty, sn_factor, max_iter, kwargs = self._set_oasis_params(
            **kwargs
        )

        # Make ROI trace non-negative
        row -= np.min(row)

        # Get noise level estimation and correct it via exponentiation to
        # better distinguish low- and high-SNR traces
        sn = oasis.functions.GetSn(row)
        sn = sn**sn_factor

        # Try to deconvolve the trace
        try:
            results = oasis.functions.deconvolve(
                row,
                sn=sn,
                penalty=penalty,
                optimize_g=optimize_g,
                max_iter=max_iter,
                b=0,  # We have already corrected the traces for baseline
                **kwargs,
            )
            denoised = results[0]
            spike_train = results[1]
            baseline = results[2]
            exp_decay_param = results[3]
            penalty_lam = results[4]

        except IndexError:
            # This error is likely to happen if `sn_factor` is too small.
            # Return dummy values
            denoised = np.zeros_like(row)
            spike_train = np.zeros_like(row)
            baseline = None
            exp_decay_param = None
            penalty_lam = None

        return denoised, spike_train, baseline, exp_decay_param, penalty_lam

    def _set_oasis_params(self, **kwargs):
        optimize_g = kwargs.pop("optimize_g", None)
        penalty = kwargs.pop("penalty", None)
        sn_factor = kwargs.pop("sn_factor", None)
        max_iter = kwargs.pop("max_iter", None)

        optimize_g = self.optimize_g if optimize_g is None else optimize_g
        penalty = self.penalty if penalty is None else penalty
        sn_factor = self.sn_factor if sn_factor is None else sn_factor
        max_iter = self.max_iter if max_iter is None else max_iter

        return optimize_g, penalty, sn_factor, max_iter, kwargs

    def deconvolve(
        self,
        dFF,
        **kwargs,
    ):
        """Deconvolve calcium fluorescence traces

        Solves a noise constrained sparse non-negative deconvolution problem.
        Sparsity is promoted via an l0- or an l1-penalty term.

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are #ROIs x #Time-steps
        kwargs : dict, optional
            Extra keyword arguments to be passed to
            `oasis.functions.deconvolve`. Here you can optionally bypass the
            OASIS parameters from the model by providing them as keyword
            arguments.

        Returns
        -------
        tuple
            Tuple of five elements:
                - `denoised_traces`: array of denoised traces
                - `spike_trains`: array of spike trains
                - `baselines`: array of baselines
                - `exp_decay_params`: array of exponential decay parameters
                - `penalty_lams`: optimal Lagrange multipliers for the penalty term

        Notes
        -----
        For more information on the parameters, see the documentation of
        `oasis.functions.deconvolve`

        See Also
        --------
        oasis.functions.deconvolve
        """
        # Make sure traces have dimensions #ROIs x #Time-steps, even if the
        # user inputs the flattened trace of a single ROI
        if len(dFF.shape) == 1:
            dFF = dFF.reshape(1, -1)

        # Deconvolve traces
        denoised_traces = []
        spike_trains = []
        baselines = []
        exp_decay_params = []
        penalty_lams = []
        for row in dFF:
            denoised, spike_train, baseline, exp_decay_param, penalty_lam = (
                self._deconvolve_roi_trace(
                    row,
                    **kwargs,
                )
            )

            denoised_traces.append(denoised)
            spike_trains.append(spike_train)
            baselines.append(baseline)
            exp_decay_params.append(exp_decay_param)
            penalty_lams.append(penalty_lam)

        # Turn lists of traces into numpy arrays for compatibility with the
        # parent class' methods
        denoised_traces = np.vstack(denoised_traces)
        spike_trains = np.vstack(spike_trains)

        # Assemble output tuple
        output = (
            denoised_traces,
            spike_trains,
            baselines,
            exp_decay_params,
            penalty_lams,
        )

        return output

    def predict(self, dFF, **kwargs):
        """Wrapper to return denoised traces from OASIS deconvolution

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are #ROIs x #Time-steps
        kwargs : dict, optional
            Keyword arguments to be passed to `deconvolve`

        Returns
        -------
        array-like
        Array of denoised traces. It has the same dimensions as `dFF`

        See Also
        --------
        deconvolve
        """
        denoised_traces, _, _, _, _ = self.deconvolve(dFF, **kwargs)
        return denoised_traces

    def predict_discrete(self, dFF, threshold=None, **kwargs):
        """Predict binary spikes from an array of baseline-corrected
        calcium fluorescence traces

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are
            #ROIs x #Time-steps
        threshold : float, optional
            Threshold for binarizing the spike trains returned by the OASIS
            deconvolution function, by default `self.threshold`. Numbers in the
            spike trains above this threshold will be set to 1, and numbers
            below it will be set to 0
        kwargs : dict, optional
            Keyword arguments to be passed to `deconvolve`

        Returns
        -------
        array-like
            Array of binary spikes. It has the same dimensions as `dFF`, with
            1s indicating the presence of a spike and 0s indicating the
            absence of a spike.

        Notes
        -----
        For more information on the parameters, see the documentation of
        `oasis.functions.deconvolve`

        See Also
        --------
        deconvolve
        """
        # Set threshold parameter
        threshold = self.threshold if threshold is None else threshold

        # Get spikes from OASIS deconvolution function
        _, spike_trains, _, _, _ = self.deconvolve(dFF, **kwargs)

        # Spikes from OASIS are not binary. We need to threshold them for
        # compatibility with our pipeline
        discrete_spikes = np.zeros_like(spike_trains)
        discrete_spikes[spike_trains > threshold] = 1.0

        return discrete_spikes

    def find_spikes(self, dFF, threshold=None, **kwargs):
        """Find spike times from an array of baseline-corrected calcium
        fluorescence traces

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are
            #ROIs x #Time-steps
        threshold : float, optional
            Threshold for binarizing the spike trains returned by the OASIS
            deconvolution function, by default `self.threshold`. Numbers in the
            spike trains above this threshold will be set to 1, and numbers
            below it will be set to 0
        kwargs : dict, optional
            Keyword arguments to be passed to `deconvolve`

        Returns
        -------
        list
            List of spike times for each ROI

        Notes
        -----
        For more information on the parameters, see the documentation of
        `oasis.functions.deconvolve`

        See Also
        --------
        deconvolve
        """
        # Get discrete spikes from thresholded OASIS predictions
        discrete_spikes = self.predict_discrete(dFF, threshold=threshold, **kwargs)

        # Convert discrete spikes to spike time-steps
        spikes = []
        for row in discrete_spikes:
            spikes.append(np.where(row == 1)[0])

        return spikes


class LookupDetector(SpikeDetector):
    """Spike detector based on a lookup table"""

    def __init__(self, model_dir, subset="84C10_23E10_GCaMP7f") -> None:
        super().__init__()
        self.subset = subset
        self.model_dir = model_dir
        self.lookup_table = self._load_lookup_table()
        self._session_grouping = self.lookup_table.groupby("session_id")

    def _load_lookup_table(self):
        df = pd.read_csv(os.path.join(self.model_dir, f"{self.subset}.csv"))
        return df

    def predict(self, dFF, *args, **kwargs):
        """Dummy method to comply with the parent class

        This function does the same as `predict_discrete`

        See Also
        --------
        predict_discrete
        """
        # Dummy method to comply with the parent class
        return self.predict_discrete(dFF, *args, **kwargs)

    def find_spikes(self, dFF, session_id, rtol=1e-3) -> list:
        """Find spike times from an array of baseline-corrected calcium
        fluorescence traces

        Parameters
        ----------
        dFF : array-like
            Array of baseline-corrected calcium fluorescence traces. The
            implied dimensions are #ROIs x #Time-steps
        session_id : str
            Session ID of the calcium fluorescence traces
        rtol : float, optional
            Relative tolerance parameter for comparing dFF amplitudes at spike
            times in the lookup table with the input dFF array, by default
            1e-3


        Returns
        -------
        list
            List of spike times for each ROI

        Notes
        -----
        See the notes of the `predict_discrete` method for more information

        See Also
        --------
        predict_discrete
        """
        spikes = []

        # Get sub-dataframe for the session
        session_df = self._session_grouping.get_group(session_id)

        # Group sub-dataframe by ROI ID
        roi_grouping = session_df.groupby("roi")

        # Get total number of ROIs from the input dFF array
        n_rois = dFF.shape[0]

        # Loop over ROIs and find spikes
        for roi_id in range(n_rois):
            try:
                roi_df = roi_grouping.get_group(roi_id)
            except KeyError:
                # If the ROI is not in the lookup table, append an empty list
                spikes.append([])
                continue
            spikes.append(roi_df["time_step"].values.flatten())

        return spikes
