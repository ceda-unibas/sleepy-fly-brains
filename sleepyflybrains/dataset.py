"""Dataset processing module"""

import copy
import os
import warnings
from datetime import datetime
from glob import glob

import numpy as np
import pandas as pd
import yaml

from sleepyflybrains import detectors, timeseries


def get_session_paths(data_dir, subsets=["84C10_23E10_GCaMP7f"]):
    """List the paths of all subdirectories containing animal data

    Parameters
    ----------
    data_dir : str
        Path to the directory where to root the search
    subsets : list, optional
        List of subsets of data to include, by default
        `['84C10_23E10_GCaMP7f']`

    Returns
    -------
    list
        List of paths of animal data
    """
    # Get all session paths for the desired subsets
    session_paths = []
    for subset in subsets:
        session_paths += glob(os.path.join(data_dir, subset, "*"))
        # Retain only directories
        session_paths = [path for path in session_paths if os.path.isdir(path)]

    # Sort paths by ID
    session_paths = sorted(session_paths, key=lambda x: os.path.basename(x).split()[0])

    return session_paths


def get_traces(
    sessions, trace_type="dFF", sd: detectors.SpikeDetector = None, **kwargs
):
    """Get activity traces from list of sessions

    Parameters
    ----------
    sessions : list of `sleepyflybrains.dataset.Session`
        List of session objects
    trace_type : str, optional
        Type of activity traces to gather, by default 'dFF'.
        Options are `'dFF'`, `'predict'`, `'predict_discrete`, and
        `'find_spikes'`
    sd : `sleepyflybrains.detectors.SpikeDetector`, optional
        Detector used to infer spikes from baseline-corrected fluorescence
        traces, by default None. It is mandatory to provide this argument if
        `trace_type` is not `'dFF'`
    **kwargs : dict
        Additional keyword arguments to pass to the `get_activity_traces`
        method of each session

    Returns
    -------
    dict
        Dictionary where the keys are session IDs and the values are the
        corresponding arrays of activity traces.
    dict
        Dictionary where the keys are session IDs and the values are the
        corresponding sampling rates.

    Notes
    -----
    If `trace_type` is `'dFF'`, then the traces are the baseline-corrected,
    calcium fluorescence traces. If `trace_type` is `'predict'`,
    `'predict_discrete'`, `'find_spikes'`, then the traces are the respective
    outputs of the methods `SpikeDetector.predict`, `SpikeDetector.
    predict_discrete`, and `SpikeDetector.find_spikes`.


    Raises
    ------
    ValueError
        If `sd` is not provided and `trace_type` is not `'dFF'`
    ValueError
        If `trace_type` is not one of `'dFF'`, `'predict'`,
        `'predict_discrete'` or `'find_spikes'`
    """
    if sd is None and trace_type != "dFF":
        raise ValueError(
            "A SpikeDetector must be provided if `trace_type` is not `'dFF'`"
        )

    traces = {}
    sample_rates = {}

    for session in sessions:
        # Record sampling rate
        sample_rates[session.session_id] = session.sample_rate

        # Get dF/F activity traces
        dFF = session.get_activity_traces(**kwargs)

        # Get the method to use for the trace type
        trace_method = _get_trace_method(sd, trace_type)

        # Define kwargs based on detector type
        detector_kwargs = _define_detector_kwargs(sd, session)

        # Assign traces based on trace type
        traces[session.session_id] = trace_method(dFF, **detector_kwargs)

    return traces, sample_rates


def _define_detector_kwargs(sd, session):
    # Define kwargs based on detector type
    if isinstance(sd, detectors.LookupDetector):
        kwargs = {"session_id": session.session_id}
    else:
        kwargs = {"sample_rate": session.sample_rate}
    return kwargs


def _get_trace_method(sd, trace_type):
    if trace_type == "predict":
        return sd.predict
    elif trace_type == "predict_discrete":
        return sd.predict_discrete
    elif trace_type == "find_spikes":
        return sd.find_spikes
    elif trace_type == "dFF":
        return lambda x, **kwargs: x
    else:
        raise ValueError(
            "Options for `trace_type` are `'predict'`, "
            + ", `'predict_discrete'` or `'find_spikes'`"
        )


class SessionDataset:
    """Session dataset class

    Parameters
    ----------
    dataset_path : str
        Path to the dataset
    subsets : list, optional
        List of subsets of data to include, by default
        `['84C10_23E10_GCaMP7f']`

    Attributes
    ----------
    dataset_path : str
        Path to the dataset
    subsets : list
        List of subsets of data to include
    session_paths : list
        List of paths of animal data
    session_list : list of `sleepyflybrains.dataset.Session`
        List of session objects
    """

    def __init__(self, dataset_path, subsets=["84C10_23E10_GCaMP7f"]) -> None:
        self.dataset_path = dataset_path
        self.subsets = subsets
        self.session_paths = get_session_paths(self.dataset_path, self.subsets)
        self.session_list = self._load_session_list()

    def _load_session_list(self):
        session_list = []
        for path in self.session_paths:
            session_list.append(Session(path))
        return session_list

    def get_session_from_id(self, session_id):
        """Get Session object corresponding to the provided ID

        Parameters
        ----------
        session_id : string
            Unique identifier of an experiment session

        Returns
        -------
        `sleepyflybrains.dataset.Session`
            The Session object
        """
        selected_session = None
        for session in self.session_list:
            if session.session_id == session_id:
                selected_session = session
                break
        return selected_session

    def get_group(self, name, values):
        """Get list of Session objects grouped by an attribute taking the
        values provided

        Parameters
        ----------
        name : string
            Name of the attribute from which to define the group
        values : singleton or list
            List of values for the attribute that will define the group

        Returns
        -------
        list of `dataset.Session`
            List of experiment sessions grouped by the provided attribute and
            values
        """
        # Make sure values is an iterable
        try:
            _ = values[0]
        except TypeError:
            values = [values]

        selected_sessions = []
        for session in self.session_list:
            try:
                attribute = session.__getattribute__(name)
                if attribute in values:
                    selected_sessions.append(session)
            except AttributeError:
                pass

        return selected_sessions

    def _get_rested_names(self):
        # TODO: it would be more future proof it there was a metadata file with all possible names
        names = ["rested", "Recovered", "Rested"]
        return names

    def _get_sleep_deprived_names(self):
        # TODO: it would be more future proof it there was a metadata file with all possible names
        names = ["sleep_deprived", "Sleep-deprived"]
        return names

    def get_rested(self):
        """Shortcut to get a list of all the sessions in which the flies were rested or
        recovered
        """
        list_of_sessions = []
        for name in self._get_rested_names():
            list_of_sessions += self.get_group("state", name)
        return list_of_sessions

    def get_sleep_deprived(self):
        """Shortcut to get a list of all the sessions in which the flies were
        sleep-deprived
        """
        list_of_sessions = []
        for name in self._get_sleep_deprived_names():
            list_of_sessions += self.get_group("state", name)
        return list_of_sessions

    def tally_attribute_values(self):
        """Compile a table with attributes and their values for each experiment
        session in the dataset

        Returns
        -------
        `pandas.DataFrame`
            The tally table
        """
        rows = []
        for session in self.session_list:
            attributes = copy.deepcopy(session.__dict__)
            try:
                attributes.pop("path")  # Attribute `path` is non-informative
            except AttributeError:
                pass
            rows.append(attributes)

        return pd.DataFrame(rows)

    def fluorescence_trace_statistics(self, raw=True, **kwargs):
        """Compile a table with statistics of the fluorescence traces in each
        experiment session in the dataset

        Parameters
        ----------
        raw : bool, optional
            Use raw fluorescence traces when computing statistics, by default
            True. If False, then a cleaned-up version of the activity traces
            will be used. See `sleepyflybrains.dataset.Session.get_activity_traces` for
            more information.

        Returns
        -------
        `pandas.DataFrame`
            The statistics table
        """
        results = pd.DataFrame(
            columns=[
                "session_id",
                "# ROIs",
                "# time steps",
                "duration [min:sec]",
                "min_val",
                "max_val",
                "median_val",
                "avg_val",
                "std",
            ]
        )
        for session in self.session_list:
            if raw:
                F = session.load_functional_fluorescence()
            else:
                F = session.get_activity_traces(**kwargs)
            n_rois, n_time_steps = F.shape
            duration = datetime.strftime(
                datetime.utcfromtimestamp(n_time_steps / session.sample_rate),
                "%M:%S",
            )
            new_row = [
                session.session_id,
                n_rois,
                n_time_steps,
                duration,
                np.min(F),
                np.max(F),
                np.median(F),
                np.mean(F),
                np.std(F),
            ]
            results.loc[len(results)] = new_row

        return results


class Session:
    """Experiment session class

    Parameters
    ----------
    path : str
        Path to the directory `'<session_id>/'` containing
        the session data

    Attributes
    ----------
    path : str
        Path to the directory `'<session_id>/'` containing
        the session data
    session_id : str
        Unique identifier of the experiment session
    subset : str
        Subset to which the session belongs
    date : str
        Date of the experiment session
    state : str
        State of the fly in this session.
    sample_rate : float
        Sampling rate of the fluorescence traces
    pixel_size : float
        Area of a single pixel in the image. Only available for the
        R23E10_GCaMP7f subset
    scanning_area : float
        Area of the image. Only available for the R23E10_GCaMP7f subset
    genotype : str
        Genotype of the fly. Only available for the R23E10_GCaMP7f subset
    sex : str
        Sex of the fly. Only available for the R23E10_GCaMP7f subset
    start_frame : int
        Index of the first frame of valid data. Only available for the
        84C10_23E10_GCaMP7f subset
    stop_frame : int
        Index of the last frame of valid data. Only available for the
        84C10_23E10_GCaMP7f subset
    """

    def __init__(self, path) -> None:
        # Base session path
        self.path = path

        # Get animal ID and session date from path
        self.session_id = self._get_id()

        # Get sub-dataset to which the session belongs
        self.subset = self._get_subset()

        # Get properties from metadata file
        if self.subset == "R23E10_GCaMP7f":
            properties = self._read_metadata()
            self.date = self._set_property_from_metadata(properties, "date")
            self.state = self._set_property_from_metadata(properties, "state")
            self.age = self._set_property_from_metadata(properties, "age")

            # Fixed properties from the General_metadata.txt file
            self.sample_rate = 34.4614  # Hz
            self.pixel_size = 0.122367e-6 * 0.122367e-6  # m^2
            self.scanning_area = 67.3029e-6 * 67.3029e-6  # m^2
            self.genotype = "UAS-GCaMP7f; R23E10-GAL4, UAS-CD4-tdTomato"
            self.sex = "female"

        else:
            properties = self._read_metadata()
            self.date = self._set_property_from_metadata(properties, "date")
            self.state = self._set_property_from_metadata(properties, "state")
            self.sample_rate = self._set_property_from_metadata(
                properties, "sample_rate"
            )
            self.start_frame = self._set_property_from_metadata(
                properties, "start_frame"
            )
            self.stop_frame = self._set_property_from_metadata(properties, "stop_frame")

    def _set_property_from_metadata(self, properties_dict, name):
        try:
            properties = self._read_metadata()
            return properties[name]
        except KeyError:
            return None

    def _get_id(self):
        return os.path.basename(self.path).split()[0]

    def _get_subset(self):
        return os.path.basename(os.path.dirname(self.path))

    def _translate_property_name(self, name_in_metadata):
        if name_in_metadata == "group":
            return "state"
        if name_in_metadata == "framerate":
            return "sample_rate"
        else:
            return name_in_metadata

    def _read_metadata(self):
        properties = {}
        try:
            with open(os.path.join(self.path, "metadata.yaml"), "r") as file:
                metadata = yaml.safe_load(file)

            for key in metadata.keys():
                try:
                    properties[self._translate_property_name(key)] = metadata[key]
                except KeyError:
                    pass

            return properties

        except FileNotFoundError:
            raise FileNotFoundError(
                "File metadata.yaml not found in {}".format(self.path)
            )

    def load_array(self, name):
        """Load npy array from file

        Parameters
        ----------
        name : str
            Name of the array to load within the folder specified in
            `self.path`

        Returns
        -------
        array_like
            The loaded array, if the corresponding file exists. Otherwise, a
            warning is issued and None is returned
        """
        try:
            array = np.load(os.path.join(self.path, name), allow_pickle=True)
        except FileNotFoundError:
            warnings.warn("The desired array does not exist for this session.")
            array = None
        return array

    def load_functional_fluorescence(self):
        """Wrapper to load the raw fluorescence traces in the functional green
        channel.

        Returns
        -------
        array_like
            Calcium fluorescence traces in the form of one time-series
            (columns) per neuron (rows)

        Notes
        -----
        The green and red fluorescence channels represent the same regions of
        interest (ROIs), so they can be used to evaluate motion and baseline
        correction, as the non-functional channel should not have any activity
        transients. Any transients there should be measurement artifacts.
        """
        # Load calcium fluorescence data
        F_functional = self.load_array("F.npy")
        return F_functional

    def load_nonfunctional_fluorescence(self):
        """Wrapper to load the raw fluorescence traces in the non-functional
        red channel.

        Returns
        -------
        array_like
            Calcium fluorescence traces in the form of one time-series
            (columns) per neuron (rows)

        Notes
        -----
        The green and red fluorescence channels represent the same regions of
        interest (ROIs), so they can be used to evaluate motion and baseline
        correction, as the non-functional channel should not have any activity
        transients. Any transients there should be measurement artifacts.
        """
        # Load calcium fluorescence data
        F_nonfunctional = self.load_array("F_chan2.npy")
        return F_nonfunctional

    def clean_up_traces(self, F_functional, F_nonfunctional=None, cell_type=1):
        """Process the raw fluorescence signal and return dF/F activity traces
        corrected for baseline and measurement artifacts,

        Parameters
        ----------
        F_functional : array-like
            Raw functional fluorescence traces
        F_nonfunctional : array-like, optional
            Raw non-functional fluorescence traces, by default None
        cell_type : int, optional
            Type of cell to consider, by default 1

        Returns
        -------
        array-like
            Corrected activity traces array. If the cell type is not found, a
            warning is issued and None is returned.


        Notes
        -----
        In the R23E10_GCaMP7f sub-dataset, this function should be passed *after* baseline correction of the functional and non-functional channels. It will then remove measurement artifacts using the non-functional channel.

        In the other sub-datasets, this function should be passed *before* baseline correction. It will select valid ROIs using the `iscell.npy` array, trim the traces to a pre-determined 2 minute-window of valid data.
        """
        if self.subset == "R23E10_GCaMP7f":
            # Remove measurement artifacts using the nonfunctional channel
            F_cleaned = timeseries.remove_measurement_artifacts(
                F_functional, F_nonfunctional
            )

        else:
            # Load iscell array
            iscell = self.load_array("iscell.npy")

            # Select valid ROIs
            valid_rois = np.where(iscell[:, 0] == cell_type)[0]
            if len(valid_rois) == 0:
                warnings.warn(
                    "No valid ROIs of cell type {} found for session {}".format(
                        cell_type, self.session_id
                    )
                )
                return None
            F_cleaned = F_functional[valid_rois, :]

            # Trim to 2 minutes
            F_cleaned = F_cleaned[:, self.start_frame : self.stop_frame]

        return F_cleaned

    def get_activity_traces(self, baseline_correction_window=None, cell_type=1):
        """Process the raw fluorescence signal and return dF/F activity traces
        corrected for baseline and/or measurement artifacts

        Parameters
        ----------
        baseline_correction_window : float, optional
            Window size in seconds for baseline correction using the AMorMol algorithm, by default None. If None, a default value is used for each sub-dataset: 0.5 seconds for the R23E10_GCaMP7f subset and 1.0 second for the other
            sub-datasets.
        cell_type : int, optional
            Type of cell to consider, by default 1. See the dataset documentation for more information.

        Returns
        -------
        array-like
            Corrected activity traces array

        Notes
        -----
        In the R23E10_GCaMP7f sub-dataset, the non-functional
        channel is used to correct measurement artifacts, then baseline
        correction is done to get dF/F traces.

        In the other sub-datasets, the `iscell.npy` array is used
        to select valid ROIs, then the traces are trimmed to 2 minutes using
        the start and stop frames, and then baseline correction is done to get
        dF/F traces.
        """
        # Load raw functional fluorescence traces
        F_functional = self.load_functional_fluorescence()

        # For the R23E10 sub-dataset, we do baseline
        # correction to get dF/F traces. Then, we need to correct measurement
        # artifacts using the non-functional channel
        if self.subset == "R23E10_GCaMP7f":
            # Load raw nonfunctional fluorescence traces
            F_nonfunctional = self.load_nonfunctional_fluorescence()

            # Set default window for baseline correction for this dataset
            # TODO: ideally, this should be set in the metadata file
            window_in_sec = baseline_correction_window or 0.5

            # Correct baselines
            n_rois, _ = F_functional.shape
            baselines = timeseries.compute_baseline(
                np.concatenate([F_functional, F_nonfunctional], axis=0),
                method="amormol",
                half_window=int(window_in_sec * self.sample_rate),
            )
            F_functional = timeseries.correct_baseline(
                F_functional, baselines[:n_rois, :]
            )
            F_nonfunctional = timeseries.correct_baseline(
                F_nonfunctional, baselines[n_rois:, :]
            )

            # Remove measurement artifacts using the nonfunctional channel
            F_cleaned = self.clean_up_traces(
                F_functional, F_nonfunctional, cell_type=cell_type
            )

        # For the other sub-datasets, we need to select valid ROIs using
        # the `iscell.npy` array, trim the traces to 2 minutes using the start
        # and stop frames, and then do baseline correction to get dF/F traces.
        else:
            # Clean up traces
            F_cleaned = self.clean_up_traces(F_functional, cell_type=cell_type)

            if F_cleaned is not None:
                # Set default window for baseline correction for this dataset
                # TODO: ideally, this should be set in the metadata file
                window_in_sec = baseline_correction_window or 1.0

                # Correct baselines
                baselines = timeseries.compute_baseline(
                    F_cleaned,
                    method="amormol",
                    half_window=int(window_in_sec * self.sample_rate),
                )
                F_cleaned = timeseries.correct_baseline(F_cleaned, baselines)

        return F_cleaned
