.. toctree::
    :hidden:

    Home <self>
    Contributing <CONTRIBUTING.md>
    bibliography

.. include:: ../README.md
    :parser: myst_parser.sphinx_
