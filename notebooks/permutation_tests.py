"""Permutation tests"""

import matplotlib.pyplot as plt
import numpy as np

# Test statistics #


def difference_of_means(sample_1, sample_2, axis=0):
    """Difference of means test statistic

    Parameters
    ----------
    sample_1 : array_like
        Numbers in the first sample
    sample_2 : array_like
        Numbers in the second sample
    axis : int, optional
        Direction of the arrays onto which to gather the means, by default 0

    Returns
    -------
    float
        Difference of means in the two samples
    """
    return np.mean(sample_1, axis=axis) - np.mean(sample_2, axis=axis)


# Plotting functionality #


def plot_permutation_distribution(res, ax=None, bins=51, fontsize=12):
    """Plot permutation distribution

    Parameters
    ----------
    res : `PermutationTestResult`
        Permutation test result, as returned by `scipy.stats.permutation_test`
    ax : `matplotlib.Axis`, optional
        Axis onto which to make the plot. If None, a new figure/axis will be
        created, by default None
    bins : int, optional
        Number of bins in the permutation distribution histogram, by default 51
    fontsize : int, optional
        Font size, by default 12

    Returns
    -------
    `matplotlib.Axis`
        Axis onto which the plot was made
    """
    # Parse axis
    if ax is None:
        _, ax = plt.subplots(1, 1, figsize=(8, 4))

    # Plot histogram os test statistics across different permutations
    ax.hist(res.null_distribution, bins=bins, color="#46505a")

    # Overlay test statistic of the original permutation as a vertical line
    ax.axvline(x=res.statistic, color="#d20537", label="Original statistic")

    # Set titles and labels
    ax.set_title("Permutation distribution of test statistic", fontsize=fontsize)
    ax.set_xlabel("Value of Statistic", fontsize=fontsize)
    ax.set_ylabel("Frequency", fontsize=fontsize)
    ax.legend(fontsize=fontsize)

    return ax
