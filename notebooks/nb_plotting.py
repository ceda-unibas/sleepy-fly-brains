"""Plotting utilities for the notebooks"""

import plotly.express as px
import plotly.graph_objects as go


def _default_labels(x):
    return {
        x: "value",
        "group": "Group",
        "window_size_in_sec": "Window size (s)",
        "session_id": "Session ID",
        "roi": "ROI",
        "freq": "Frequency (Hz)",
        "psd": "Power (mV²/Hz)",
    }


def plot_value_distribution(df, x="value", color="group", log_y=False, slider=None):
    """Plot distribution of values from a data frame

    Histogram of values from a data frame, grouped by a column in the data,
    with box marginals plotted on top.

    Parameters
    ----------
    df : `pandas.DataFrame`
        Data frame containing the data to plot
    color : str, optional
        Column name to use for slicing the data frame by color, by default 'group'
    x : str, optional
        Column name to use for the x-axis, by default 'value'
    log_y : bool, optional
        Whether to use a log-scale for the y-axis, by default False
    slider : str, optional
        Column name to use for the slider, by default None

    Returns
    -------
    `plotly.graph_objs.Figure`
        Plotly figure where the plot was drawn
    """
    fig = px.histogram(
        df,
        x=x,
        color=color,
        animation_frame=slider,
        log_y=log_y,
        labels=_default_labels(x),
        opacity=0.5,
        histnorm="percent",
        barmode="overlay",
        marginal="box",
        template="plotly_white",
    )
    _set_yaxis_range(fig, log_y)
    fig["layout"].pop("updatemenus")
    return fig


def _set_yaxis_range(fig, log_y):
    yaxis_range = [-1, 2] if log_y else [0, 100]
    fig.update_layout(yaxis_range=yaxis_range)


def line_with_interval(*args, error_y_mode=None, alpha=0.3, **kwargs):
    """Extension of `plotly.express.line` to use interval (error) bands

    Parameters
    ----------
    *args : list
        Positional arguments to pass to `plotly.express.line`
    error_y_mode : str, optional
        One of {'bar','band','bars','bands',None}, by default None. If 'bar' or
        'bars', the error bars are plotted as vertical lines. If 'band' or
        'bands', the error bars are plotted as filled areas.
    **kwargs : dict
        Keyword arguments to pass to `plotly.express.line`

    Returns
    -------
    `plotly.graph_objects.Figure`
        The figure containing the line plot with error bars

    Raises
    ------
    ValueError
    If `interval_y_mode` is not one of {'bar','band','bars','bands',None}

    See Also
    --------
    plotly.express.line

    Notes
    -----
    This function is a modified version of the code found in this
    StackOverflow answer:
    https://stackoverflow.com/a/69594497/9474857
    """
    ERROR_MODES = {"bar", "band", "bars", "bands", None}

    if error_y_mode not in ERROR_MODES:
        raise ValueError(
            "'error_y_mode' must be one of {}".format(ERROR_MODES)
            + ", received {}.".format(error_y_mode)
        )

    if error_y_mode in {"bar", "bars", None}:
        # Use the default plotly.express.line function
        fig = px.line(*args, **kwargs)

    elif error_y_mode in {"band", "bands"}:
        if "error_y" not in kwargs:
            raise ValueError("You must also provide an 'error_y' argument.")

        # Initialize a temporary figure as a regular line plot with error bars
        figure_with_error_bars = _create_figure_with_error_bars(*args, **kwargs)

        # Initialize the final figure as a copy of the temporary figure without
        # the 'error_y' argument
        fig = _create_figure_without_error_bars(*args, **kwargs)

        # Add the error bands as filled areas
        _add_error_bands(fig, figure_with_error_bars, alpha)

        # Reorder data
        fig = _reorder_data(fig)

    return fig


def _create_figure_with_error_bars(*args, **kwargs):
    """Create a figure with error bars"""
    return px.line(*args, **kwargs)


def _create_figure_without_error_bars(*args, **kwargs):
    """Create a figure without error bars"""
    return px.line(
        *args, **{arg: val for arg, val in kwargs.items() if arg != "error_y"}
    )


def _add_error_bands(fig, figure_with_error_bars, alpha):
    """Add error bands as filled areas to the figure"""
    for data in figure_with_error_bars.data:
        # Get x-values
        x = list(data["x"])

        # Set upper and lower y-values
        y_upper = list(data["y"] + data["error_y"]["array"])
        if data["error_y"]["arrayminus"] is None:
            y_lower = list(data["y"] - data["error_y"]["array"])
        else:
            y_lower = list(data["y"] - data["error_y"]["arrayminus"])

        # Set color of the error band as a transparent version of the line
        # color
        color = tuple(
            int(data["line"]["color"].lstrip("#")[i : i + 2], 16) for i in (0, 2, 4)
        )
        color = f"rgba({color},{alpha})"
        color = color.replace("((", "(")
        color = color.replace("),", ",")
        color = color.replace(" ", "")

        # Add the error band traces to the figure
        # We need to include the traces in reverse order as well so that
        # they are plotted correctly, as explained here:
        # https://plotly.com/python/continuous-error-bars/
        fig.add_trace(
            go.Scatter(
                x=x + x[::-1],  # x, then x reversed
                y=y_upper + y_lower[::-1],  # upper, then lower reversed
                fill="toself",
                fillcolor=color,
                line=dict(color="rgba(255,255,255,0)"),
                hoverinfo="skip",
                showlegend=False,
                legendgroup=data["legendgroup"],
                xaxis=data["xaxis"],
                yaxis=data["yaxis"],
            )
        )


def _reorder_data(fig):
    # Reorder data as explained in this StackOverflow answer:
    # https://stackoverflow.com/a/66854398/8849755
    reordered_data = []
    for i in range(int(len(fig.data) / 2)):
        reordered_data.append(fig.data[i + int(len(fig.data) / 2)])
        reordered_data.append(fig.data[i])
    fig.data = tuple(reordered_data)

    return fig
