{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Correlation analysis of rolling mean firing rates\n",
    "\n",
    "Correlations between rolling mean firing rate of ROIs within each of the well-rested and sleep-deprived groups.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import config\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "\n",
    "# Ignore warnings from tensorflow\n",
    "os.environ[\"TF_CPP_MIN_LOG_LEVEL\"] = \"2\"\n",
    "\n",
    "from sleepyflybrains import dataset, detectors, plotting, timeseries  # noqa: E402\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "DATA_DIR = config.DATA_DIR\n",
    "\n",
    "# Models path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "MODELS_DIR = config.MODELS_DIR"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load sessions\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the whole dataset\n",
    "session_dataset = dataset.SessionDataset(DATA_DIR)\n",
    "\n",
    "# Get the 'rested' group and a sample session from this group\n",
    "rested = session_dataset.get_rested()\n",
    "sample_rested = rested[0]\n",
    "\n",
    "# Get the 'sleep-deprived' group and a sample session from this group\n",
    "sleep_deprived = session_dataset.get_sleep_deprived()\n",
    "sample_sleep_deprived = sleep_deprived[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Available session IDs (rested flies):\")\n",
    "for session in rested:\n",
    "    print(f\"- {session.session_id}\")\n",
    "\n",
    "print()\n",
    "\n",
    "print(\"Available session IDs (sleep-deprived flies):\")\n",
    "for session in sleep_deprived:\n",
    "    print(f\"- {session.session_id}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rolling mean firing rates\n",
    "\n",
    "1. Define a (Gaussian) window (e.g. 20 seconds)\n",
    "2. Roll it over the spike detection series of each ROI, computing a mean firing rate over the window\n",
    "3. This defines a smooth signal indicating how the 20-second-firing rate varies over time\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dFF = sample_sleep_deprived.get_activity_traces()\n",
    "session_id = sample_sleep_deprived.session_id\n",
    "sample_rate = sample_sleep_deprived.sample_rate\n",
    "\n",
    "detector = detectors.LookupDetector(model_dir=os.path.join(MODELS_DIR, \"lookup\"))\n",
    "\n",
    "discrete_traces = detector.predict_discrete(dFF, session_id=session_id)\n",
    "n_times = discrete_traces.shape[1]\n",
    "\n",
    "window_size_in_seconds = 10\n",
    "rolling_mean_firing_rates = (\n",
    "    timeseries.rolling_mean(\n",
    "        discrete_traces, window_size=int(window_size_in_seconds * sample_rate)\n",
    "    )\n",
    "    * sample_rate\n",
    ")  # Convert to Hz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "times = np.arange(n_times) / sample_rate\n",
    "\n",
    "plotting.plot_sequences(\n",
    "    discrete_traces, index=times, index_label=\"Time [s]\", seqlabel=\"ROI\", parallel=True\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plotting.plot_sequences(\n",
    "    rolling_mean_firing_rates,\n",
    "    index=times,\n",
    "    index_label=\"Time [s]\",\n",
    "    seqlabel=\"ROI\",\n",
    "    parallel=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plotting.plot_sequences(\n",
    "    rolling_mean_firing_rates,\n",
    "    index=times,\n",
    "    index_label=\"Time [s]\",\n",
    "    seqlabel=\"ROI\",\n",
    "    parallel=False,\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Rolling mean firing rates (sample 'sleep-deprived' session)\",\n",
    "    yaxis_title=\"Firing rate [Hz]\",\n",
    "    legend_title=\"ROI\",\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. Compute correlations between rolling mean firing rates for each pair of ROIs within a session.\n",
    "5. See how these correlation values are distributed within each group (Recovered\n",
    "   vs. Sleep-deprived). Are rolling mean firing rates more or less correlated in\n",
    "   the Recovered group?\n",
    "\n",
    "Let's define all window sizes to be tested\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "window_sizes_in_seconds = [2, 5, 10, 15, 20, 30, 60]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Remarks:_\n",
    "\n",
    "- We will study the correlations between rolling mean firing rate curves. However, the correlation coefficient of two \"silent\" neurons is undefined, because the signals are constant. Therefore, correlation analysis is blind to co-occurrence of silent neurons. We thus leave the analysis of the latter for another notebook.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `LookupDetector`-estimated spikes\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lookup_detector = detectors.LookupDetector(model_dir=os.path.join(MODELS_DIR, \"lookup\"))\n",
    "\n",
    "rested_traces_lookup_detector, rested_traces_sr = dataset.get_traces(\n",
    "    rested, trace_type=\"predict_discrete\", sd=lookup_detector\n",
    ")\n",
    "sleep_deprived_traces_lookup_detector, sleep_deprived_traces_sr = dataset.get_traces(\n",
    "    sleep_deprived, trace_type=\"predict_discrete\", sd=lookup_detector\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corr_df_lookup_detector = nb_utils.compile_rmfr_correlations(\n",
    "    {\n",
    "        \"Rested\": rested_traces_lookup_detector,\n",
    "        \"Sleep-deprived\": sleep_deprived_traces_lookup_detector,\n",
    "    },\n",
    "    {\"Rested\": rested_traces_sr, \"Sleep-deprived\": sleep_deprived_traces_sr},\n",
    "    window_sizes_in_seconds,\n",
    ")\n",
    "corr_df_lookup_detector.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = nb_plotting.plot_value_distribution(\n",
    "    corr_df_lookup_detector, x=\"corr_val\", slider=\"window_size_in_sec\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Correlations between rolling mean firing rates \" + \"(from `LookupDetector`)\",\n",
    "    yaxis_range=[0, 25],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouping = corr_df_lookup_detector.groupby(\"group\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group),\n",
    "    color=\"session_id\",\n",
    "    x=\"corr_val\",\n",
    "    slider=\"window_size_in_sec\",\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Correlations between r.m.f.r.s \"\n",
    "    + \"(from `LookupDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group),\n",
    "    color=\"session_id\",\n",
    "    x=\"corr_val\",\n",
    "    slider=\"window_size_in_sec\",\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Correlations between r.m.f.r.s \"\n",
    "    + \"(from `LookupDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `PeakDetector`-estimated spikes\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_detector = detectors.PeakDetector(\n",
    "    model_dir=os.path.join(MODELS_DIR, \"peakdetector\")\n",
    ")\n",
    "\n",
    "rested_traces_peak_detector, rested_traces_sr = dataset.get_traces(\n",
    "    rested, trace_type=\"predict_discrete\", sd=peak_detector\n",
    ")\n",
    "sleep_deprived_traces_peak_detector, sleep_deprived_traces_sr = dataset.get_traces(\n",
    "    sleep_deprived, trace_type=\"predict_discrete\", sd=peak_detector\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corr_df_peak_detector = nb_utils.compile_rmfr_correlations(\n",
    "    {\n",
    "        \"Rested\": rested_traces_peak_detector,\n",
    "        \"Sleep-deprived\": sleep_deprived_traces_peak_detector,\n",
    "    },\n",
    "    {\"Rested\": rested_traces_sr, \"Sleep-deprived\": sleep_deprived_traces_sr},\n",
    "    window_sizes_in_seconds,\n",
    ")\n",
    "corr_df_peak_detector.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = nb_plotting.plot_value_distribution(\n",
    "    corr_df_peak_detector, x=\"corr_val\", slider=\"window_size_in_sec\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Correlations between rolling mean firing rates \" + \"(from `PeakDetector`)\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouping = corr_df_peak_detector.groupby(\"group\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group),\n",
    "    color=\"session_id\",\n",
    "    x=\"corr_val\",\n",
    "    slider=\"window_size_in_sec\",\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Correlations between r.m.f.r.s \"\n",
    "    + \"(from `PeakDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group),\n",
    "    color=\"session_id\",\n",
    "    x=\"corr_val\",\n",
    "    slider=\"window_size_in_sec\",\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Correlations between r.m.f.r.s \"\n",
    "    + \"(from `PeakDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "vscode": {
   "interpreter": {
    "hash": "2da6f128e681b9be3b593636fae9c0e18e82a5a89fce918a5624c5a741aa670c"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
