{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tracking amplitude at detection and silent neurons\n",
    "\n",
    "1. For each ROI, record the amplitudes at the times when a spike was detected.\n",
    "\n",
    "2. Actually, take the maximum amplitude within a small window (200 ms) around the detected spike\n",
    "\n",
    "3. At the same time, measure each ROI firing rate throughout the recording period, as a proxy for how active that neuron was.\n",
    "\n",
    "4. Then see how these values are distributed in the two groups\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import config\n",
    "import nb_plotting\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import permutation_tests\n",
    "from matplotlib import pyplot as plt\n",
    "from scipy.stats import permutation_test\n",
    "\n",
    "# Ignore warnings from tensorflow\n",
    "os.environ[\"TF_CPP_MIN_LOG_LEVEL\"] = \"2\"\n",
    "\n",
    "from sleepyflybrains import dataset, detectors  # noqa: E402\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "DATA_DIR = config.DATA_DIR\n",
    "\n",
    "# Models path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "MODELS_DIR = config.MODELS_DIR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load sessions\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the whole dataset\n",
    "session_dataset = dataset.SessionDataset(DATA_DIR)\n",
    "\n",
    "# Get the 'rested' group and a sample session from this group\n",
    "rested = session_dataset.get_rested()\n",
    "sample_rested = rested[0]\n",
    "\n",
    "# Get the 'sleep-deprived' group and a sample session from this group\n",
    "sleep_deprived = session_dataset.get_sleep_deprived()\n",
    "sample_sleep_deprived = sleep_deprived[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Available session IDs (rested flies):\")\n",
    "for session in rested:\n",
    "    print(f\"- {session.session_id}\")\n",
    "\n",
    "print()\n",
    "\n",
    "print(\"Available session IDs (sleep-deprived flies):\")\n",
    "for session in sleep_deprived:\n",
    "    print(f\"- {session.session_id}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Baseline-corrected fluorescence amplitudes at event detection times\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below takes a session's fluorescence traces and event detection times and returns the maximum fluorescence amplitudes in a short search window around event detection times.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def amplitudes_at_detection(dFF, spikes, sample_rate, search_window=0.4):\n",
    "    \"\"\"Compute the amplitude of the calcium transient at the detection time.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    dFF : list\n",
    "        A session's calcium fluorescence traces. Should be an array of shape\n",
    "        (#ROIs, #Time-steps)\n",
    "    spikes_dict : list\n",
    "        Spike detection time steps. Should be a list with #ROIs elements, each\n",
    "        element being a list of time steps at which a spike was detected.\n",
    "    sample_rate : float\n",
    "        Sampling rate of the traces in the session\n",
    "    search_window : float, optional\n",
    "        Time window (in seconds) around the detection time to search for the\n",
    "        peak amplitude of the calcium transient. By default, 0.4 seconds.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    list\n",
    "        A list of lists with the amplitude of the calcium transient at the\n",
    "        detection time for each ROI.\n",
    "\n",
    "    Notes\n",
    "    -----\n",
    "    One needs to define a search window around the detection time to account for delays of other artifacts introduced by the peak detection models. For instance, `sleepyflybrains.detectors.PeakDetector` does by default a low-pass filtering of the signal before trying to detect peaks. Another example is `sleepyflybrains.detectors.CascadeDetector`, which will tend to detect bursts of peaks in particularly tall and wide calcium fluorescence pulses.\n",
    "    \"\"\"\n",
    "    amplitudes = []\n",
    "    search_window_in_samples = int(search_window * sample_rate)\n",
    "    half_search_window = int(search_window_in_samples / 2)\n",
    "    for roi, row in enumerate(dFF):\n",
    "        roi_amplitudes = []\n",
    "        for spike in spikes[roi]:\n",
    "            # Get the amplitude of the calcium transient at the detection time\n",
    "            # by searching for the peak in a small time window around the\n",
    "            # detection time\n",
    "            if half_search_window > 0:\n",
    "                if half_search_window > spike:\n",
    "                    amplitude = np.max(row[: spike + half_search_window])\n",
    "                elif half_search_window > len(row) - spike:\n",
    "                    amplitude = np.max(row[spike - half_search_window :])\n",
    "                else:\n",
    "                    amplitude = np.max(\n",
    "                        row[spike - half_search_window : spike + half_search_window]\n",
    "                    )\n",
    "            else:\n",
    "                amplitude = row[spike]\n",
    "            roi_amplitudes.append(amplitude)\n",
    "        amplitudes.append(roi_amplitudes)\n",
    "\n",
    "    return amplitudes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us set a global search window for this notebook:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SEARCH_WINDOW = 0  # Get the amplitude at the exact detection time\n",
    "SEARCH_WINDOW = 0.2  # Get the max. amplitude within a window"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below implements the following steps:\n",
    "\n",
    "- Compile dFF amplitude at event detection times for each ROI\n",
    "- Calculate mean firing rates over the recording period for each ROI\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compile_amplitudes_and_firing_rates(traces_dict, sample_rates, search_window=0.4):\n",
    "    \"\"\"Compile amplitudes and firing rates from a dictionary of traces.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    traces_dict : dict\n",
    "        Dictionary with traces and event detection times. Each key should\n",
    "        correspond to a group (e.g. 'rested', 'sleep-deprived'). Each of these\n",
    "        keys should have two dictionaries as values, accessible via keys 'dFF'\n",
    "        and 'find_spikes'. The latter should be in the same format as returned\n",
    "        by `dataset.get_traces(trace_type='find_spikes')`.\n",
    "    sample_rates : dict\n",
    "        Dictionary with sample rates for each session. Each key should\n",
    "        correspond to a group (e.g. 'rested', 'sleep-deprived'). Each of these\n",
    "        keys should have a dictionary as value, accessible via session IDs as\n",
    "        keys.\n",
    "    search_window : float, optional\n",
    "        Time window (in seconds) around the detection time to search for the\n",
    "        peak amplitude of the calcium transient. By default, 0.4 seconds.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    amplitudes : pd.DataFrame\n",
    "        DataFrame with amplitudes at event detection times.\n",
    "    firing_rates : pd.DataFrame\n",
    "        DataFrame with mean firing rates over the recording period.\n",
    "    \"\"\"\n",
    "    amplitudes_dict = {\n",
    "        \"group\": [],\n",
    "        \"session_id\": [],\n",
    "        \"roi\": [],\n",
    "        \"amplitude_at_detection\": [],\n",
    "    }\n",
    "    firing_rates_dict = {\n",
    "        \"group\": [],\n",
    "        \"session_id\": [],\n",
    "        \"roi\": [],\n",
    "        \"firing_rate\": [],\n",
    "    }\n",
    "    for group, group_dict in traces_dict.items():\n",
    "        dFF_dict = group_dict[\"dFF\"]\n",
    "        for session_id, dFF in dFF_dict.items():\n",
    "            # Get sample rate, and recording period duration in samples\n",
    "            sample_rate = sample_rates[group][session_id]\n",
    "            recording_duration_in_samples = dFF.shape[1]\n",
    "\n",
    "            # Get amplitudes at detection time\n",
    "            amplitudes = amplitudes_at_detection(\n",
    "                dFF,\n",
    "                group_dict[\"find_spikes\"][session_id],\n",
    "                sample_rate,\n",
    "                search_window=search_window,\n",
    "            )\n",
    "            for roi, roi_amplitudes in enumerate(amplitudes):\n",
    "                # Get number of non-zero amplitudes\n",
    "                num_non_zero_amplitudes = len(roi_amplitudes)\n",
    "\n",
    "                # Record amplitudes at detection in dictionary\n",
    "                amplitudes_dict[\"amplitude_at_detection\"] += roi_amplitudes\n",
    "                amplitudes_dict[\"roi\"] += [roi] * num_non_zero_amplitudes\n",
    "                amplitudes_dict[\"session_id\"] += [session_id] * num_non_zero_amplitudes\n",
    "                amplitudes_dict[\"group\"] += [group] * num_non_zero_amplitudes\n",
    "\n",
    "                # Get mean firing rate [Hz] as the number of non-zero amplitudes\n",
    "                # divided by the recording duration [s]\n",
    "                recording_duration = recording_duration_in_samples / sample_rate\n",
    "                firing_rate = num_non_zero_amplitudes / recording_duration\n",
    "\n",
    "                # Record results in dictionary\n",
    "                firing_rates_dict[\"firing_rate\"].append(firing_rate)\n",
    "                firing_rates_dict[\"roi\"].append(roi)\n",
    "                firing_rates_dict[\"session_id\"].append(session_id)\n",
    "                firing_rates_dict[\"group\"].append(group)\n",
    "\n",
    "    return pd.DataFrame(amplitudes_dict), pd.DataFrame(firing_rates_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get baseline-corrected fluorescence traces for all sessions in the dataset\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rested_dFF, rested_sr = dataset.get_traces(\n",
    "    rested,\n",
    "    trace_type=\"dFF\",\n",
    ")\n",
    "sleep_deprived_dFF, sleep_deprived_sr = dataset.get_traces(\n",
    "    sleep_deprived,\n",
    "    trace_type=\"dFF\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `LookupDetector`-estimated spike trains\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lookup_detector = detectors.LookupDetector(model_dir=os.path.join(MODELS_DIR, \"lookup\"))\n",
    "\n",
    "rested_spikes_lookup_detector, _ = dataset.get_traces(\n",
    "    rested, trace_type=\"find_spikes\", sd=lookup_detector\n",
    ")\n",
    "sleep_deprived_spikes_lookup_detector, _ = dataset.get_traces(\n",
    "    sleep_deprived, trace_type=\"find_spikes\", sd=lookup_detector\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_lookup_detector, firing_rates_lookup_detector = (\n",
    "    compile_amplitudes_and_firing_rates(\n",
    "        {\n",
    "            \"rested\": {\n",
    "                \"dFF\": rested_dFF,\n",
    "                \"find_spikes\": rested_spikes_lookup_detector,\n",
    "            },\n",
    "            \"sleep-deprived\": {\n",
    "                \"dFF\": sleep_deprived_dFF,\n",
    "                \"find_spikes\": sleep_deprived_spikes_lookup_detector,\n",
    "            },\n",
    "        },\n",
    "        {\n",
    "            \"rested\": rested_sr,\n",
    "            \"sleep-deprived\": sleep_deprived_sr,\n",
    "        },\n",
    "        search_window=SEARCH_WINDOW,\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_lookup_detector.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firing_rates_lookup_detector.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are amplitudes distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_range = [\n",
    "    amplitudes_lookup_detector[\"amplitude_at_detection\"].min(),\n",
    "    amplitudes_lookup_detector[\"amplitude_at_detection\"].max(),\n",
    "]\n",
    "amplitude_resolution = 0.01\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    amplitudes_lookup_detector, x=\"amplitude_at_detection\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"dF/F amplitudes at detection times (`LookupDetector`)\",\n",
    "    xaxis_range=[0, 0.4],\n",
    "    yaxis_range=[0, 20],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are amplitudes distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_grouping = amplitudes_lookup_detector.groupby(\"group\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    amplitudes_grouping.get_group(group), color=\"session_id\", x=\"amplitude_at_detection\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"dF/F amplitudes at detection times (`LookupDetector`),\"\n",
    "    + ' \"{}\" group'.format(group),\n",
    "    xaxis_range=[0, 0.4],\n",
    "    yaxis_range=[0, 30],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    amplitudes_grouping.get_group(group), color=\"session_id\", x=\"amplitude_at_detection\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"dF/F amplitudes at detection times (`LookupDetector`),\"\n",
    "    + ' \"{}\" group'.format(group),\n",
    "    xaxis_range=[0, 0.4],\n",
    "    yaxis_range=[0, 30],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are firing rates distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exclude_silent = True\n",
    "# exclude_silent = False\n",
    "\n",
    "if exclude_silent:\n",
    "    # Get only rows where the firing_rate is non-zero\n",
    "    df = firing_rates_lookup_detector[firing_rates_lookup_detector[\"firing_rate\"] > 0]\n",
    "    yaxis_range = [0, 25]\n",
    "else:\n",
    "    df = firing_rates_lookup_detector\n",
    "    yaxis_range = [0, 50]\n",
    "\n",
    "firing_rates_range = [\n",
    "    df[\"firing_rate\"].min(),\n",
    "    df[\"firing_rate\"].max(),\n",
    "]\n",
    "firing_rate_resolution = 0.01  # Hz\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(df, x=\"firing_rate\")\n",
    "\n",
    "if exclude_silent:\n",
    "    title = \"Firing rate per ROI (`LookupDetector`), excluding silent ROIs\"\n",
    "else:\n",
    "    title = \"Firing rate per ROI (`LookupDetector`)\"\n",
    "\n",
    "fig.update_layout(\n",
    "    title=title,\n",
    "    yaxis_range=yaxis_range,\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firing_rates_grouping = firing_rates_lookup_detector.groupby(\"group\")\n",
    "\n",
    "print(\"Number of actually silent ROIs in the 'rested' group:\")\n",
    "n_silent = firing_rates_grouping.get_group(\"rested\")[\"firing_rate\"].eq(0).sum()\n",
    "n_total = firing_rates_grouping.get_group(\"rested\").shape[0]\n",
    "print(\"\\t{} out of {} ({:.2f}%)\".format(n_silent, n_total, n_silent / n_total * 100))\n",
    "print(\"Number of actually silent ROIs in the 'sleep-deprived' group:\")\n",
    "n_silent = firing_rates_grouping.get_group(\"sleep-deprived\")[\"firing_rate\"].eq(0).sum()\n",
    "n_total = firing_rates_grouping.get_group(\"sleep-deprived\").shape[0]\n",
    "print(\"\\t{} out of {} ({:.2f}%)\".format(n_silent, n_total, n_silent / n_total * 100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Is this difference significant? A permutation test\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Gather a boolean signals telling us, for each ROI, whether it is silent or not\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "silent_rois = {\n",
    "    \"rested\": list(firing_rates_grouping.get_group(\"rested\")[\"firing_rate\"].eq(0)),\n",
    "    \"sleep-deprived\": list(\n",
    "        firing_rates_grouping.get_group(\"sleep-deprived\")[\"firing_rate\"].eq(0)\n",
    "    ),\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run a permutation test using difference in means as the test statistic. Since we have boolean samples, the difference of means just gives us the difference in the proportion of silent neurons between groups.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = permutation_test(\n",
    "    (silent_rois[\"rested\"], silent_rois[\"sleep-deprived\"]),\n",
    "    permutation_tests.difference_of_means,\n",
    "    vectorized=True,\n",
    "    n_resamples=20000,\n",
    "    alternative=\"two-sided\",\n",
    ")\n",
    "results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the permutation distribution of the test statistic and overlay the original difference in proportions of silent neurons:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = permutation_tests.plot_permutation_distribution(results, bins=21)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are firing rates distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    firing_rates_grouping.get_group(group), color=\"session_id\", x=\"firing_rate\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Mean firing rate per ROI (`LookupDetector`),\" + ' \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    firing_rates_grouping.get_group(group), color=\"session_id\", x=\"firing_rate\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Mean firing rate per ROI (`LookupDetector`),\" + ' \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `PeakDetector`-estimated spike trains\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_detector = detectors.PeakDetector(\n",
    "    model_dir=os.path.join(MODELS_DIR, \"peakdetector\")\n",
    ")\n",
    "\n",
    "rested_spikes_peak_detector, _ = dataset.get_traces(\n",
    "    rested, trace_type=\"find_spikes\", sd=peak_detector\n",
    ")\n",
    "sleep_deprived_spikes_peak_detector, _ = dataset.get_traces(\n",
    "    sleep_deprived, trace_type=\"find_spikes\", sd=peak_detector\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_peak_detector, firing_rates_peak_detector = (\n",
    "    compile_amplitudes_and_firing_rates(\n",
    "        {\n",
    "            \"rested\": {\n",
    "                \"dFF\": rested_dFF,\n",
    "                \"find_spikes\": rested_spikes_peak_detector,\n",
    "            },\n",
    "            \"sleep-deprived\": {\n",
    "                \"dFF\": sleep_deprived_dFF,\n",
    "                \"find_spikes\": sleep_deprived_spikes_peak_detector,\n",
    "            },\n",
    "        },\n",
    "        {\n",
    "            \"rested\": rested_sr,\n",
    "            \"sleep-deprived\": sleep_deprived_sr,\n",
    "        },\n",
    "        search_window=SEARCH_WINDOW,\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_peak_detector.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firing_rates_peak_detector.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are amplitudes distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_range = [\n",
    "    amplitudes_peak_detector[\"amplitude_at_detection\"].min(),\n",
    "    amplitudes_peak_detector[\"amplitude_at_detection\"].max(),\n",
    "]\n",
    "amplitude_resolution = 0.01\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    amplitudes_peak_detector, x=\"amplitude_at_detection\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"dF/F amplitudes at detection times (`PeakDetector`)\",\n",
    "    xaxis_range=[0, 0.4],\n",
    "    yaxis_range=[0, 20],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are amplitudes distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitudes_grouping = amplitudes_peak_detector.groupby(\"group\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    amplitudes_grouping.get_group(group), color=\"session_id\", x=\"amplitude_at_detection\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"dF/F amplitudes at detection times (`PeakDetector`),\"\n",
    "    + ' \"{}\" group'.format(group),\n",
    "    xaxis_range=[0, 0.4],\n",
    "    yaxis_range=[0, 20],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    amplitudes_grouping.get_group(group), color=\"session_id\", x=\"amplitude_at_detection\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"dF/F amplitudes at detection times (`PeakDetector`),\"\n",
    "    + ' \"{}\" group'.format(group),\n",
    "    xaxis_range=[0, 0.4],\n",
    "    yaxis_range=[0, 20],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are firing rates distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exclude_silent = True\n",
    "# exclude_silent = False\n",
    "\n",
    "if exclude_silent:\n",
    "    # Get only rows where the firing_rate is non-zero\n",
    "    df = firing_rates_peak_detector[firing_rates_peak_detector[\"firing_rate\"] > 0]\n",
    "    yaxis_range = [0, 25]\n",
    "else:\n",
    "    df = firing_rates_peak_detector\n",
    "    yaxis_range = [0, 50]\n",
    "\n",
    "firing_rates_range = [\n",
    "    df[\"firing_rate\"].min(),\n",
    "    df[\"firing_rate\"].max(),\n",
    "]\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(df, x=\"firing_rate\")\n",
    "if exclude_silent:\n",
    "    title = \"Firing rate per ROI (`PeakDetector`), non-silent ROIs\"\n",
    "else:\n",
    "    title = \"Firing rate per ROI (`PeakDetector`)\"\n",
    "\n",
    "fig.update_layout(\n",
    "    title=title,\n",
    "    yaxis_range=[0, 25],\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firing_rates_grouping = firing_rates_peak_detector.groupby(\"group\")\n",
    "\n",
    "print(\"Number of actually silent ROIs in the 'rested' group:\")\n",
    "n_silent = firing_rates_grouping.get_group(\"rested\")[\"firing_rate\"].eq(0).sum()\n",
    "n_total = firing_rates_grouping.get_group(\"rested\").shape[0]\n",
    "print(\"\\t{} out of {} ({:.2f}%)\".format(n_silent, n_total, n_silent / n_total * 100))\n",
    "print(\"Number of actually silent ROIs in the 'sleep-deprived' group:\")\n",
    "n_silent = firing_rates_grouping.get_group(\"sleep-deprived\")[\"firing_rate\"].eq(0).sum()\n",
    "n_total = firing_rates_grouping.get_group(\"sleep-deprived\").shape[0]\n",
    "print(\"\\t{} out of {} ({:.2f}%)\".format(n_silent, n_total, n_silent / n_total * 100))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Is this difference significant? A permutation test\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Gather a boolean signals telling us, for each ROI, whether it is silent or not\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "silent_rois = {\n",
    "    \"rested\": list(firing_rates_grouping.get_group(\"rested\")[\"firing_rate\"].eq(0)),\n",
    "    \"sleep-deprived\": list(\n",
    "        firing_rates_grouping.get_group(\"sleep-deprived\")[\"firing_rate\"].eq(0)\n",
    "    ),\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run a permutation test using difference in means as the test statistic. Since we have boolean samples, the difference of means just gives us the difference in the proportion of silent neurons between groups.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = permutation_test(\n",
    "    (silent_rois[\"rested\"], silent_rois[\"sleep-deprived\"]),\n",
    "    permutation_tests.difference_of_means,\n",
    "    vectorized=True,\n",
    "    n_resamples=20000,\n",
    "    alternative=\"two-sided\",\n",
    ")\n",
    "results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the permutation distribution of the test statistic and overlay the original difference in proportions of silent neurons:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = permutation_tests.plot_permutation_distribution(results, bins=11)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are firing rates distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    firing_rates_grouping.get_group(group), color=\"session_id\", x=\"firing_rate\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Mean firing rate per ROI (`PeakDetector`),\" + ' \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    firing_rates_grouping.get_group(group), color=\"session_id\", x=\"firing_rate\"\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Mean firing rate per ROI (`PeakDetector`),\" + ' \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "vscode": {
   "interpreter": {
    "hash": "2da6f128e681b9be3b593636fae9c0e18e82a5a89fce918a5624c5a741aa670c"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
