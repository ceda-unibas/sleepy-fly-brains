{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How are the intervals between events distributed?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import config\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "\n",
    "# Ignore warnings from tensorflow\n",
    "os.environ[\"TF_CPP_MIN_LOG_LEVEL\"] = \"2\"\n",
    "\n",
    "from sleepyflybrains import dataset, detectors  # noqa: E402\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "DATA_DIR = config.DATA_DIR\n",
    "\n",
    "# Models path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "MODELS_DIR = config.MODELS_DIR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load sessions\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the whole dataset\n",
    "session_dataset = dataset.SessionDataset(DATA_DIR)\n",
    "\n",
    "# Get the 'rested' group and a sample session from this group\n",
    "rested = session_dataset.get_rested()\n",
    "sample_rested = rested[0]\n",
    "\n",
    "# Get the 'sleep-deprived' group and a sample session from this group\n",
    "sleep_deprived = session_dataset.get_sleep_deprived()\n",
    "sample_sleep_deprived = sleep_deprived[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Available session IDs (rested flies):\")\n",
    "for session in rested:\n",
    "    print(f\"- {session.session_id}\")\n",
    "\n",
    "print()\n",
    "\n",
    "print(\"Available session IDs (sleep-deprived flies):\")\n",
    "for session in sleep_deprived:\n",
    "    print(f\"- {session.session_id}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computing event intervals\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Compute time between spikes (events) for each ROI in each session\n",
    "2. See how these values compare across groups (Recovered vs. Sleep-deprived)\n",
    "\n",
    "We define two scalar values to help with interpretation:\n",
    "\n",
    "- Mean event rate (MER) = $\\frac{1}{\\text{mean event interval}}$\n",
    "\n",
    "- Coefficient of variation (CV) = $\\frac{\\text{std(event\n",
    "  interval)}}{\\text{mean(event interval)}}$\n",
    "\n",
    "Let us set some global parameters for the plots:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interval_range = [0, 120]  # in seconds\n",
    "interval_resolution = 1  # in seconds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Remarks:_\n",
    "\n",
    "- We plot the y-axis on a log-scale by default to better see the differences in the distribution of intervals. Keep this in mind when interpreting the plots.\n",
    "- By default, the interval value range is set to 600 seconds because all the flies in the dataset were recorded for 10 minutes.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `LookupDetector`-estimated spikes\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lookup_detector = detectors.LookupDetector(model_dir=os.path.join(MODELS_DIR, \"lookup\"))\n",
    "\n",
    "rested_spikes_lookup_detector, rested_sr = dataset.get_traces(\n",
    "    rested, trace_type=\"find_spikes\", sd=lookup_detector\n",
    ")\n",
    "\n",
    "sleep_deprived_spikes_lookup_detector, sleep_deprived_sr = dataset.get_traces(\n",
    "    sleep_deprived, trace_type=\"find_spikes\", sd=lookup_detector\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "event_intervals_df_lookup_detector = nb_utils.compile_event_intervals(\n",
    "    {\n",
    "        \"Rested\": rested_spikes_lookup_detector,\n",
    "        \"Sleep-deprived\": sleep_deprived_spikes_lookup_detector,\n",
    "    },\n",
    "    {\"Rested\": rested_sr, \"Sleep-deprived\": sleep_deprived_sr},\n",
    ")\n",
    "event_intervals_df_lookup_detector.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = nb_plotting.plot_value_distribution(\n",
    "    event_intervals_df_lookup_detector, x=\"event_interval\", log_y=True\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Distribution of event intervals (from `LookupDetector`)\",\n",
    "    xaxis_title=\"Event interval (s)\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouping = event_intervals_df_lookup_detector.groupby(\"group\")\n",
    "\n",
    "for name, group in grouping:\n",
    "    print(\"Group: {}\".format(name))\n",
    "    print(\"Mean event rate = {:.3f} Hz\".format(nb_utils.mean_event_rate(group)))\n",
    "    print(\n",
    "        \"Coefficient of variation = {:.3f}\".format(\n",
    "            nb_utils.coefficient_of_variation(group)\n",
    "        )\n",
    "    )\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group), color=\"session_id\", x=\"event_interval\", log_y=True\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Distribution of event intervals (from `LookupDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_grouping = grouping.get_group(group).groupby(\"session_id\")\n",
    "\n",
    "for name, group in session_grouping:\n",
    "    print(\"Session ID: {}\".format(name))\n",
    "    print(\"Mean event rate = {:.2f} Hz\".format(nb_utils.mean_event_rate(group)))\n",
    "    print(\n",
    "        \"Coefficient of variation = {:.2f}\".format(\n",
    "            nb_utils.coefficient_of_variation(group)\n",
    "        )\n",
    "    )\n",
    "    print()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group), color=\"session_id\", x=\"event_interval\", log_y=True\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Distribution of event intervals (from `LookupDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_grouping = grouping.get_group(group).groupby(\"session_id\")\n",
    "\n",
    "for name, group in session_grouping:\n",
    "    print(\"Session ID: {}\".format(name))\n",
    "    print(\"Mean event rate = {:.2f} Hz\".format(nb_utils.mean_event_rate(group)))\n",
    "    print(\n",
    "        \"Coefficient of variation = {:.2f}\".format(\n",
    "            nb_utils.coefficient_of_variation(group)\n",
    "        )\n",
    "    )\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `PeakDetector`-estimated spikes\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_detector = detectors.PeakDetector(\n",
    "    model_dir=os.path.join(MODELS_DIR, \"peakdetector\")\n",
    ")\n",
    "\n",
    "rested_spikes_peak_detector, rested_sr = dataset.get_traces(\n",
    "    rested, trace_type=\"find_spikes\", sd=peak_detector\n",
    ")\n",
    "\n",
    "sleep_deprived_spikes_peak_detector, sleep_deprived_sr = dataset.get_traces(\n",
    "    sleep_deprived, trace_type=\"find_spikes\", sd=peak_detector\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "event_intervals_df_peak_detector = nb_utils.compile_event_intervals(\n",
    "    {\n",
    "        \"Rested\": rested_spikes_peak_detector,\n",
    "        \"Sleep-deprived\": sleep_deprived_spikes_peak_detector,\n",
    "    },\n",
    "    {\"Rested\": rested_sr, \"Sleep-deprived\": sleep_deprived_sr},\n",
    ")\n",
    "event_intervals_df_peak_detector.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed across groups?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = nb_plotting.plot_value_distribution(\n",
    "    event_intervals_df_peak_detector, x=\"event_interval\", log_y=True\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Distribution of event intervals (from `PeakDetector`)\",\n",
    "    xaxis_title=\"Event interval (s)\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grouping = event_intervals_df_peak_detector.groupby(\"group\")\n",
    "\n",
    "for name, group in grouping:\n",
    "    print(\"Group: {}\".format(name))\n",
    "    print(\"Mean event rate = {:.2f} Hz\".format(nb_utils.mean_event_rate(group)))\n",
    "    print(\n",
    "        \"Coefficient of variation = {:.2f}\".format(\n",
    "            nb_utils.coefficient_of_variation(group)\n",
    "        )\n",
    "    )\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How are the values distributed within groups, across sessions?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Rested\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group), color=\"session_id\", x=\"event_interval\", log_y=True\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Distribution of event intervals (from `PeakDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_grouping = grouping.get_group(group).groupby(\"session_id\")\n",
    "\n",
    "for name, group in session_grouping:\n",
    "    print(\"Session ID: {}\".format(name))\n",
    "    print(\"Mean event rate = {:.2f} Hz\".format(nb_utils.mean_event_rate(group)))\n",
    "    print(\n",
    "        \"Coefficient of variation = {:.2f}\".format(\n",
    "            nb_utils.coefficient_of_variation(group)\n",
    "        )\n",
    "    )\n",
    "    print()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "group = \"Sleep-deprived\"\n",
    "\n",
    "fig = nb_plotting.plot_value_distribution(\n",
    "    grouping.get_group(group), color=\"session_id\", x=\"event_interval\", log_y=True\n",
    ")\n",
    "fig.update_layout(\n",
    "    title=\"Distribution of event intervals (from `PeakDetector`) \"\n",
    "    + 'within the \"{}\" group'.format(group),\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_grouping = grouping.get_group(group).groupby(\"session_id\")\n",
    "\n",
    "for name, group in session_grouping:\n",
    "    print(\"Session ID: {}\".format(name))\n",
    "    print(\"Mean event rate = {:.2f} Hz\".format(nb_utils.mean_event_rate(group)))\n",
    "    print(\n",
    "        \"Coefficient of variation = {:.2f}\".format(\n",
    "            nb_utils.coefficient_of_variation(group)\n",
    "        )\n",
    "    )\n",
    "    print()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "vscode": {
   "interpreter": {
    "hash": "2da6f128e681b9be3b593636fae9c0e18e82a5a89fce918a5624c5a741aa670c"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
