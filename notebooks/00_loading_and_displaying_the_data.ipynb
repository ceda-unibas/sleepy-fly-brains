{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading and displaying data\n",
    "\n",
    "This notebook discusses how to load and display the project's data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import config\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "# Ignore warnings from tensorflow\n",
    "os.environ[\"TF_CPP_MIN_LOG_LEVEL\"] = \"2\"\n",
    "\n",
    "from sleepyflybrains import dataset, detectors, plotting  # noqa: E402\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "DATA_DIR = config.DATA_DIR\n",
    "\n",
    "# Models path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "MODELS_DIR = config.MODELS_DIR"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading sessions\n",
    "\n",
    "The `dataset.SessionDataset` object looks into the dataset path an automatically loads all the measurement sessions contained in the desired subsets. By default, it will load only the sessions in the `84C10_23E10_GCaMP7f` sub-dataset. We have made this explicit in the cell below. Its output shows the full path to all the sessions found in this manner. If you'd like to load sessions from the `R23E10_GCaMP7f` sub-dataset, simply provide `subsets=['R23E10_GCaMP7f']` variable below. It's also possible to load sessions from both sub-datasets by providing `subsets=['84C10_23E10_GCaMP7f', 'R23E10_GCaMP7f']`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_dataset = dataset.SessionDataset(DATA_DIR, subsets=[\"84C10_23E10_GCaMP7f\"])\n",
    "session_dataset.session_paths"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The loaded sessions themselves are structured as `dataset.Session` objects.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_dataset.session_list"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The moment they are created, these objects record general information about the measurement session by reading the associated metadata in the dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pick a sample session from the session list,\n",
    "# sample_session = session_dataset.session_list[0]\n",
    "\n",
    "# or, you may also pick a sample session directly via its unique\n",
    "# identifier. Using 84C10_23E10_20231007_19 as an example, the syntax for\n",
    "# doing so is\n",
    "sample_session = session_dataset.get_session_from_id(\"84C10_23E10_20231007_19\")\n",
    "\n",
    "print(\"Session ID: {}\".format(sample_session.session_id))\n",
    "print(\"Date: {}\".format(sample_session.date))\n",
    "try:\n",
    "    print(\"Sex of the fly: {}\".format(sample_session.sex))\n",
    "except AttributeError:\n",
    "    pass\n",
    "try:\n",
    "    print(\"Age of the fly: {} days old\".format(sample_session.age))\n",
    "except AttributeError:\n",
    "    pass\n",
    "print(\"The fly's state of restfulness: {}\".format(sample_session.state))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Grouping by attribute\n",
    "\n",
    "You can group the experiment sessions by attributes of either the fly being measured or the experiment itself. To do this, we use the method `get_group` from `sleepyflybrains.dataset.SessionDataset`. For example, to get all the sessions where the imaging sample rate were 73.197 Hz, we run:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_dataset.get_group(name=\"sample_rate\", values=[73.197])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You could operate similarly with the attribute `Session.state` to get the groups of flies that were either rested or sleep-deprived. But since this grouping is quite essential for this project, we've implemented a shortcut for them. Here's how to get all sessions with rested/recovered flies:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_dataset.get_rested()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's how to get all sessions with sleep-deprived flies:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_dataset.get_sleep_deprived()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading fluorescence traces\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "No actual data (other than the metadata) is loaded when the `Session` objects are created to avoid unnecessary memory use. Any `.npy` array stored in an experiment session can be loaded with the method `dataset.Session.load_array()`. For the fluorescence traces, there are handy wrappers of the latter that avoid having you type in the name of the array you want to load as it was saved to disk. The functional fluorescence traces (green channel) are available via the following command.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "F_functional = sample_session.load_functional_fluorescence()\n",
    "n_rois, n_times = F_functional.shape\n",
    "print(\n",
    "    \"Shape of fluorescence data (# ROIs, # time-steps) = {}\".format((n_rois, n_times))\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this array has a number of rows equal to the number of neurons (ROIs) identified during 2p calcium imaging. The number of columns is the same as the number of frames of 2p calcium images that were recorded in the measurement session. Here is a plot of the fluorescence traces in this array as parallel time-series.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plotting.plot_sequences(\n",
    "    F_functional,\n",
    "    index=np.arange(n_times).astype(float) / sample_session.sample_rate,\n",
    "    index_label=\"Time [s]\",\n",
    "    seqlabel=\"ROI\",\n",
    "    parallel=True,\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `R23E10_GCaMP7f` sub-dataset offers also non-functional (red channel) fluorescence data to help correct for measurement artifacts. It also has a handy loading wrapper. If you chose to load this sub-dataset you will see that this array has the same shape as the one with functional data. In fact, they measure the same ROIs but in two different channels.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "F_nonfunctional = sample_session.load_nonfunctional_fluorescence()\n",
    "if F_nonfunctional is not None:\n",
    "    print(\n",
    "        \"Shape of (non-functional) fluorescence data = {}\".format(F_nonfunctional.shape)\n",
    "    )"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We shouldn't expect neuronal activity to be reflected in the non-functional data, so any transients we see here should be interpreted as measurement artifacts. The non-functional data can thus be useful towards correction motion or baseline levels on the functional data.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if F_nonfunctional is not None:\n",
    "    plotting.plot_sequences(\n",
    "        F_nonfunctional,\n",
    "        index=np.arange(n_times).astype(float) / sample_session.sample_rate,\n",
    "        index_label=\"Time [s]\",\n",
    "        seqlabel=\"ROI\",\n",
    "        parallel=True,\n",
    "    )"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can get a \"cleaned up\" version of the fluorescence time-series via the `session.get_activity_traces` method. \"Cleaning up\" varies according to the sub-dataset from where the session comes from.\n",
    "\n",
    "Traces from `R23E10_GCaMP7f` need to be corrected for measurement artifacts using the non-functional channel, then be baseline corrected.\n",
    "\n",
    "Traces from `84C10_23E10_GCaMP7f` need to have ROIs filtered for validity, be trimmed to a pre-established 2 minutes chunk, and then be baseline-corrected. The `get_activity_traces` method does all that automatically.\n",
    "\n",
    "Note that you can choose to load the traces from any of the valid cell types by providing the `cell_type` argument. The default is `cell_type=1`. See the dataset documentation for more information on the cell types.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dFF = sample_session.get_activity_traces(cell_type=1)\n",
    "if dFF is not None:\n",
    "    n_rois, n_times = dFF.shape\n",
    "    print(\"Shape of dFF data (# ROIs, # time-steps) = {}\".format((n_rois, n_times)))\n",
    "else:\n",
    "    n_rois, n_times = None, None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plotting.plot_sequences(\n",
    "    dFF,\n",
    "    index=np.arange(n_times).astype(float) / sample_session.sample_rate,\n",
    "    index_label=\"Time [s]\",\n",
    "    seqlabel=\"ROI\",\n",
    "    parallel=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Estimating calcium events (spikes) with a simple peak finding algorithm\n",
    "\n",
    "The corrected $\\Delta F / F$ activity traces could still be quite noisy, making it difficult to directly distinguish calcium spiking events from background noise. The children of the class `sleepyflybrains.detectors.SpikeDetector` can help us filter out these spike events.\n",
    "\n",
    "Let us start with `sleepyflybrains.detectors.PeakDetector`, which implements as\n",
    "simple peak finding algorithm using the\n",
    "[`scipy.signal.find_peaks`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html)\n",
    "function. The underlying idea is that peaks are characterized by a local maximum\n",
    "in the signal that is higher than its neighbors. The algorithm can be tuned by\n",
    "setting, for instance, the minimum distance allowed between peaks, or the\n",
    "minimum width of a peak, or the minimum prominence of a candidate peak to be\n",
    "considered an actual peak.\n",
    "\n",
    "The default parameters of the predictive functions in\n",
    "`sleepyflybrains.detectors.PeakDetector` are set to values that work well for\n",
    "the data in this project. The default parameters are loaded when the\n",
    "`PeakDetector` object is created, via specification of the `model_dir` variable.\n",
    "This should be a path to a directory containing a `best_params.json` file with\n",
    "the relevant parameters. The `best_params.json` file was created by running a\n",
    "grid search over the parameters of the peak finding algorithm, using a\n",
    "human-annotated validation set. For details, see:\n",
    "\n",
    "- `scripts/hyperparam_search_peakdetector.py`\n",
    "\n",
    "If you recall from the sample session's dFF plot in the cell above, ROI 4 has many pulses that look like a characteristic calcium spike. Our `PeakDetector` correctly identifies most of them, although, if you zoom in, you will see that it seems to miss some smaller pulses (false negatives).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peak_detector = detectors.PeakDetector(\n",
    "    model_dir=os.path.join(MODELS_DIR, \"peakdetector\")\n",
    ")\n",
    "peaks = peak_detector.find_spikes(\n",
    "    dFF,\n",
    "    sample_rate=sample_session.sample_rate,\n",
    ")\n",
    "\n",
    "roi = 4\n",
    "fig = plotting.plot_detections(\n",
    "    dFF[roi, :],\n",
    "    peaks[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_ROI_{}\".format(roi),\n",
    "    detection_label=\"Detected Peaks\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To help identify the smaller peaks, `PeakDetector` performs a denoising operation (Gaussian filter) prior to the actual peak detection procedure. If the user provides a sample rate number when predicting spikes, the default denoising filter (1D Gaussian kernel) is used. Otherwise, no denoising is performed. The user can also provide a custom denoising filter when calling `find_spikes`, for example, via the argument `denoising_filter`.\n",
    "\n",
    "Let us see how the default (Gaussian) denoising filter affects the way the fluorescence traces look to the peak detector in the cell below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "denoising_filter = peak_detector._default_denoising_filter(sample_session.sample_rate)\n",
    "dFF_filtered = denoising_filter(dFF)\n",
    "\n",
    "roi = 4\n",
    "fig = plotting.plot_detections(\n",
    "    signal=dFF_filtered[roi, :],\n",
    "    detection_time_steps=peaks[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_filtered_ROI_{}\".format(roi),\n",
    "    detection_label=\"Detected Peaks\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For better context, one can actually view both the baseline-corrected and denoised fluorescence traces in the same plot, followed by the identified calcium events, as shown below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spikes = np.zeros_like(dFF[roi, :])\n",
    "spikes[peaks[roi]] = 1\n",
    "\n",
    "fig, ax = plotting.plot_spike_deconvolution(\n",
    "    dFF[roi, :],\n",
    "    dFF_filtered[roi, :],\n",
    "    spikes,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrast to ROI 4, ROI 2 is most likely just noise and measurement artifacts.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi = 2\n",
    "fig = plotting.plot_detections(\n",
    "    dFF[roi, :],\n",
    "    peaks[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_ROI_{}\".format(roi),\n",
    "    detection_label=\"Detected Peaks\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can play with the parameters of the predictive functions in `PeakDetector` to see how they affect the results, although the default values should work well the project's data. For example, if you do not set the sample-rate parameter in `find_spikes`, `PeakDetector` will use the default parameters of `scipy.signal.find_peaks`. The result is that too many peaks (false positives) are detected in ROI 4 of the sample session.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peaks = peak_detector.find_spikes(dFF)\n",
    "\n",
    "roi = 4\n",
    "fig = plotting.plot_detections(\n",
    "    dFF[roi, :],\n",
    "    peaks[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_ROI_{}\".format(roi),\n",
    "    detection_label=\"Detected Peaks\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the other hand, if you wish to use most of the default parameters and teak one of them, you can supply the sampling rate along with the parameter you wish to change. For example, if you wish to force the detected peaks to be at least 4 seconds wide, do the following. Note that in doing so, many actual spikes in the ROI are missed (false negatives).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "peaks = peak_detector.find_spikes(dFF, sample_rate=sample_session.sample_rate, width=4)\n",
    "\n",
    "roi = 4\n",
    "fig = plotting.plot_detections(\n",
    "    dFF[roi, :],\n",
    "    peaks[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_ROI_{}\".format(roi),\n",
    "    detection_label=\"Detected Peaks\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see the default parameters used by the peak detector by running the cell below. The parameters that have units are in seconds. For example, the default minimum distance between peaks is about 0.6 seconds.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(peak_detector.__dict__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looking up calcium events (spikes) with `LookupDetector`\n",
    "\n",
    "To guarantee that all calcium events are being captured, some datasets (e.g.\n",
    "`84C10_23E10_GCaMP7f`) where hand-annotated by an expert. For each calcium trace\n",
    "in the dataset, a peak was identified and its position recorded in a\n",
    "standardized table. For details, see:\n",
    "\n",
    "- `../scripts/build_event_detection_validation_set.py` and\n",
    "- `../slurm/build_event_detection_validation_set.sh`).\n",
    "\n",
    "After annotation, a lookup table with smaller footprint was saved to disk as a\n",
    "`.csv` file. For details, see:\n",
    "\n",
    "- `../scripts/build_lookup_table.py` and\n",
    "- `../slurm/build_lookup_table.sh`\n",
    "\n",
    "This lookup table serves as a \"model\" for the `LookupDetector` class, which can be used to look up the annotated peaks in the calcium traces. The `LookupDetector` class is a child of the `SpikeDetector` class, and it has the same methods and attributes. When the `LookupDetector` object is created, one should specify the location of the \"model directory\" (`model_dir`) containing the look-up tables created previously and (optionally) a `subset` variable, which by default is `84C10_23E10_GCaMP7f`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lookup_detector = detectors.LookupDetector(\n",
    "    model_dir=os.path.join(MODELS_DIR, \"lookup\"),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once instanciated, the `LookupDetector` object can be used just as any other `SpikeDetector` object. For example, to check the annotated peaks in ROI 4 of the sample session, we can run:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spikes = lookup_detector.find_spikes(\n",
    "    dFF,\n",
    "    session_id=sample_session.session_id,\n",
    ")\n",
    "\n",
    "roi = 4\n",
    "fig = plotting.plot_detections(\n",
    "    dFF[roi, :],\n",
    "    spikes[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_ROI_{}\".format(roi),\n",
    "    detection_label=\"Detected Peaks\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Estimating calcium events (spikes) with other algorithms\n",
    "\n",
    "We have implemented other spike detection algorithms in this repository. Their usage is all similar to that of `PeakDetector`. However, they either need to have hyperparameters tailored to each individual ROI trace or they employ models that are not entirely adapted to our dataset. Still, we showcase them here for the benefit of the reader.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Estimating spikes with `OASISDetector`\n",
    "\n",
    "Online Active Set method to Infer Spikes (OASIS) is the algorithm described in [Friedrich et al. 2017](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5370160/). It is a sparse deconvolution problem that simultaneously estimates the spike train and a denoised version of the calcium fluorescence trace.\n",
    "\n",
    "We implemented OASIS in the class `OASISDetector`. Here's a usage example with parameters that seem to work well for the highlighted ROI trace. Check the class docstring for more details on the parameters.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi = 4\n",
    "\n",
    "oasis_detector = detectors.OASISDetector()\n",
    "\n",
    "kwargs = {\n",
    "    \"penalty\": 0,\n",
    "    \"optimize_g\": 1,\n",
    "    \"sn_factor\": 0.875,\n",
    "}\n",
    "\n",
    "denoised = oasis_detector.predict(dFF[roi, :], **kwargs)\n",
    "spike_train = oasis_detector.predict_discrete(dFF[roi, :], threshold=0.05, **kwargs)\n",
    "\n",
    "fig, ax = plotting.plot_spike_deconvolution(\n",
    "    dFF[roi, :].flatten(),\n",
    "    denoised.flatten(),\n",
    "    spike_train.flatten(),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Estimating spikes with [Cascade](https://github.com/HelmchenLabSoftware/Cascade)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The class `CascadeDetector` is based on pre-trained [Cascade](https://github.com/HelmchenLabSoftware/Cascade) deep neural networks. Cascade models were trained to reconstruct electrophysiology based on calcium fluorescence imaging across a variety of animal brains (e.g. Zebrafish, mouse). Thus, Cascade spike predictions are empirically-backed, even though our specific imaging technology or animal brains (flies) may not be in Cascade's training datasets.\n",
    "\n",
    "To instantiate a `CascadeDetector` object, it suffices to point to a directory where the Cascade models will be loaded/saved.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd = detectors.CascadeDetector(models_dir=os.path.join(MODELS_DIR, \"cascade\"))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a list of available models to use, run the class method `get_available_models_list`. By default, the verbosity level is set to zero, but we've set it to `1` here to see where the available models file was downloaded. Note that there is now a file `available_models.yaml` under the directory pointed at by the path `os.path.join(MODELS_DIR, 'cascade').\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "available_models = cd.get_available_models_list(verbose=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Cascade models have informative names, indicating what are the tasks that each of these deep neural networks were trained to do. For example, `Global_EXC` indicates models that have been trained on a diverse set of ground truth animal datasets from excitatory neurons. The string `30Hz` indicates that the calcium fluorescence traces in the corresponding training dataset had a frame rate of `30Hz`. And the string `causalkernel` indicates that the spike probabilities in the model are fitted with an asymmetric causal kernel, rather than a symmetric Gaussian one. For guidelines on selecting a particular model for a particular dataset, see the [Cascade FAQ](https://github.com/HelmchenLabSoftware/Cascade#frequently-asked-questions).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"List of available models:\")\n",
    "print(\"\\n\".join([\"\\t\" + model for model in available_models]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The default predictive model that `CascadeDetector` uses is `Global_EXC_30Hz_smoothing50ms_causalkernel`. The causal kernel avoids assigning non-zero probability to times before the underlying spike takes place, but it's not clear if this choice makes much of a difference in the analysis that we will do. The [Cascade authors suggest](https://github.com/HelmchenLabSoftware/Cascade#what-does-the-causal-mean-for-some-of-the-models) that causal kernels might be important if one wants to analyze stimulus-triggered activity patterns.\n",
    "\n",
    "To predict spike probabilities from the baseline-corrected fluorescence traces, simply run the class method `predict`.\n",
    "\n",
    "**_Special notes regarding sub-datasets and sample rates:_**\n",
    "\n",
    "- To better match the sampling rate at which the chosen Cascade model was pre-trained, the `predict` method will automatically try to downsample the fluorescence traces to the model's frame rate. For that, it needs the `sample_rate` of the provided fluorescence traces.\n",
    "- The `R23E10_GCaMP7f` sub-dataset has a consistent frame rate of 34.4614 Hz, which is not a standard frame rate in the Cascade models, but is close enough to that of the default predictive model that `CascadeDetector` uses, so no samples will be discarded.\n",
    "- The `84C10_23E10_GCaMP7f` sub-dataset has varying frame rates, all of which above 67 Hz. With the default 30 Hz Cascade model, you should expect to lose some samples in the fluorescence traces. Despite this adjustment, you can check empirically in the cells below that Cascade ignores many real-looking spikes in the `84C10_23E10_GCaMP7f` sub-dataset. `PeakDetector` should be preferred for the latter.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spike_probs = cd.predict(dFF, sample_rate=sample_session.sample_rate, verbosity=1)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see in the plot below, most of the noise has been attenuated and the most likely locations of the neuronal spikes have been narrowed down as result of the spike prediction model.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plotting.plot_sequences(\n",
    "    spike_probs,\n",
    "    index=np.arange(spike_probs.shape[1]).astype(float) / sample_session.sample_rate,\n",
    "    index_label=\"Time [s]\",\n",
    "    seqlabel=\"ROI\",\n",
    "    parallel=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can attempt to see the difference in choosing another predictive model on the quality of the spikes that it produces. We do this via the `set_model` method. The model `Global_EXC_30Hz_smoothing25ms` uses a symmetric kernel with a smaller spread than the default. [The Cascade authors suggest](https://github.com/HelmchenLabSoftware/Cascade#what-does-the-smoothing-parameter-for-the-models-mean) smaller kernels if if one wants to avoid temporally smooth predictions in good quality data, so it could be an option here.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd.set_model(\"Global_EXC_30Hz_smoothing25ms\")\n",
    "spike_probs_alternative = cd.predict(dFF, sample_rate=sample_session.sample_rate)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can try to zoom in to the next and the previous plot to see if you spot differences in the shape and quantity of detected spikes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plotting.plot_sequences(\n",
    "    spike_probs_alternative,\n",
    "    index=np.arange(spike_probs_alternative.shape[1]).astype(float)\n",
    "    / sample_session.sample_rate,\n",
    "    index_label=\"Time [s]\",\n",
    "    seqlabel=\"ROI\",\n",
    "    parallel=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's also possible to get purely binary spike trains from any `SpikeDetector` object. However, if you are looking for accurate electrophysiology reconstructions, the Cascade authors only recommend this step for very high quality fluorescence measurements (see the [FAQ](https://github.com/HelmchenLabSoftware/Cascade#frequently-asked-questions)). It is not our case here, as we only wish to zone in on the most likely locations of calcium spikes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "discrete_spikes = cd.predict_discrete(\n",
    "    dFF, sample_rate=sample_session.sample_rate, verbosity=1\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can glimpse at issues with binarization in the plot below. Lower likelihood spikes are now indistinguishable from higher likelihood ones, so keep this in mind when using discrete spike trains in further analyses.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plotting.plot_sequences(\n",
    "    discrete_spikes,\n",
    "    index=np.arange(discrete_spikes.shape[1]).astype(float)\n",
    "    / sample_session.sample_rate,\n",
    "    index_label=\"Time [s]\",\n",
    "    seqlabel=\"ROI\",\n",
    "    parallel=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For convenience, you can retrieve the spike times for each ROI directly via the `find_spikes` method. The result is a list of lists, where each sublist contains the spike times for a particular ROI.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spike_time_estimates = cd.find_spikes(dFF, sample_rate=sample_session.sample_rate)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The list of spike time estimates can be useful as Cascade provides a function for plotting side-by-side the baseline-corrected fluorescence (blue), the predicted spike probabilities (orange, and the predicted discrete spikes (black). The plot below shows the first 50 seconds of the sample signals we are been working with so far (each of the 8 cells corresponds to the rows of the plots from before, with cells ordered from top to down and left to right).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sleepyflybrains.third_party.cascade2p.utils import plot_dFF_traces\n",
    "\n",
    "_ = plot_dFF_traces(\n",
    "    dFF,\n",
    "    np.arange(8),\n",
    "    sample_session.sample_rate,\n",
    "    spiking=spike_probs,\n",
    "    discrete_spikes=spike_time_estimates,\n",
    "    y_range=(-2, 1.5),\n",
    ")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a sense of how `CascadeDetector` fares against `PeakDetector`, we can plot again the $\\Delta F / F$ traces of ROIs 4 and 2 of the sample session, but this time overlaid with the discrete spikes estimated by Cascade.\n",
    "\n",
    "Note that in detected calcium events `CascadeDetector` will predict a burst of spikes, while `PeakDetector` will only find one. This is because Cascade models were trained to reconstruct electrophysiology from calcium fluorescence traces, and in simultaneous experiments we see that short bursts of neuronal electrical spikes will trigger tall and wide calcium fluorescence pulses. `PeakDetector` is agnostic to this biological fact, and will, generally, only find one peak per fluorescence pulse.\n",
    "\n",
    "We should remark that the currently used Cascade model has flaws. Looking into the plot below will reveal that real-looking calcium spikes are missed (false negatives).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi = 4\n",
    "fig = plotting.plot_detections(\n",
    "    dFF[roi, :],\n",
    "    spike_time_estimates[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_ROI_{}\".format(roi),\n",
    "    detection_label=\"Cascade-predicted spikes\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Still, Cascade deals well with false positives. Try the cell below with any of the ROIs in the sample session that are essentially noise and you will see little to none detected spikes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi = 2\n",
    "fig = plotting.plot_detections(\n",
    "    dFF[roi, :],\n",
    "    spike_time_estimates[roi],\n",
    "    sample_rate=sample_session.sample_rate,\n",
    "    signal_label=\"dFF_ROI_{}\".format(roi),\n",
    "    detection_label=\"Cascade-predicted spikes\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "sfb",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
