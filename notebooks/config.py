"""Configuration module for the notebooks

Works mainly to setup data file paths
"""

import os

# Path to the dataset directory. By default, it points to a variable defined in
# the user's bash_profile (or zshrc) file.
try:
    DATA_DIR = os.environ["SFB_DATA"]
except KeyError:
    print(
        "There is no environment variable named SFB_DATA. "
        + "Try setting DATA_DIR directly or assign first a "
        + "value to variable SFB_DATA in your "
        + "bash_profile (or zshrc) file."
    )
    DATA_DIR = ""

# Path to the pre-trained models directory. By default, it points to a
# variable defined in the user's bash_profile (or zshrc) file.
try:
    MODELS_DIR = os.environ["SFB_MODELS"]
except KeyError:
    print(
        "There is no environment variable named "
        + "SFB_MODELS. "
        + "Try setting MODELS_DIR directly or assign first a "
        + "value to variable "
        + "SFB_MODELS "
        + "in your bash_profile (or zshrc) file."
    )
    MODELS_DIR = ""
