"""Utilities for the notebooks"""

import json
import os
import warnings
from glob import glob

import numpy as np
import pandas as pd
import periodicity_detection as pyd
from scipy.signal import find_peaks
from tqdm import tqdm

from sleepyflybrains import timeseries


def load_eval_metrics(dir_path, tolerances):
    """Load evaluation metrics from JSON files.

    Parameters
    ----------
    dir_path : str
        Path to the directory containing the evaluation metrics JSON files.
    tolerances : list
        List of tolerances for which to load the evaluation metrics.

    Returns
    -------
    eval_df : pd.DataFrame
        Dataframe containing the evaluation metrics for the specified
        tolerances.
    """
    eval_df = pd.DataFrame(
        columns=["tolerance", "TP", "FP", "FN", "precision", "recall", "f1"]
    )

    eval_files = glob(os.path.join(dir_path, "eval_metrics_*ms_tolerance.json"))

    for tolerance in tolerances:
        eval_file = _find_eval_file(eval_files, tolerance)
        if eval_file is None:
            warnings.warn(f"File with tolerance {tolerance} not found. Skipping...")
            continue

        eval_dict = _load_eval_dict(eval_file)
        if eval_dict is None:
            warnings.warn(f"File {eval_file} not found. Skipping...")
            continue

        eval_dict["tolerance"] = tolerance
        eval_df = _add_eval_dict_to_df(eval_df, eval_dict)

    return eval_df


def _find_eval_file(eval_files, tolerance):
    for file in eval_files:
        if _get_tolerance_from_path(file) == tolerance:
            return file
    return None


def _get_tolerance_from_path(path):
    return int(path.split("_")[-2][:-2])


def _load_eval_dict(eval_file):
    try:
        with open(eval_file, "r") as f:
            eval_dict = json.load(f)
        return eval_dict
    except FileNotFoundError:
        return None


def _add_eval_dict_to_df(eval_df, eval_dict):
    if eval_df.empty:
        eval_df = pd.DataFrame(eval_dict, index=[0])
    else:
        eval_df = pd.concat(
            [eval_df, pd.DataFrame(eval_dict, index=[0])], ignore_index=True
        )
    return eval_df


def compute_corr_vals(trace_array):
    """Compute correlation values from a trace array.

    Parameters
    ----------
    trace_array : np.ndarray
        Array of shape (n_rois, n_times) containing the traces of the ROIs.

    Returns
    -------
    corr_vals : list
        List of correlation values. The list is pruned of NaNs and only
        contains the upper triangular part of the full correlation matrix to
        avoid duplicate values.
    """
    # Turn the array into a pandas dataframe so that correlation computation is
    # NaN-friendly
    trace_df = pd.DataFrame(trace_array.T)

    corr_vals = trace_df.corr().values

    # Keep only a flattened version of the upper triangular part of the
    # correlation matrix
    corr_vals = corr_vals[np.triu_indices(corr_vals.shape[0], k=1)]

    # # Remove NaNs
    corr_vals = [x for x in corr_vals if not np.isnan(x)]

    return corr_vals


def compile_rmfr_correlations(traces_dict, sr_dict, window_sizes_in_seconds):
    """Compute rolling mean firing rate correlations for a set of sessions.

    Parameters
    ----------
    traces_dict : dict
        Dictionary containing the traces of the sessions to be analyzed.
    sr_dict : dict
        Dictionary containing the sample rates of the sessions to be analyzed.
    Returns
    -------
    results_df : pd.DataFrame
        Pandas dataframe containing the results of the analysis. The dataframe
        has the following columns:
        - corr_val: correlation value
        - window_size_in_sec: window size in seconds
        - group: group to which the session belongs
        - session_id: session ID
    """
    results_dict = {
        "corr_val": [],
        "window_size_in_sec": [],
        "group": [],
        "session_id": [],
    }

    # Compute correlation values for all sessions, keeping track of the window
    # size, and whether the fly was rested or sleep-deprived
    for i, time_window in enumerate(window_sizes_in_seconds):
        print("Computing correlations for window size " + f"{time_window} seconds")

        for group, group_traces in traces_dict.items():
            group_sample_rates = sr_dict[group]
            for session_id, session_traces in group_traces.items():
                session_sample_rate = group_sample_rates[session_id]
                window_size = int(time_window * session_sample_rate)
                rolling_mean_firing_rates = (
                    timeseries.rolling_mean(
                        session_traces,
                        window_size=window_size,
                    )
                    * session_sample_rate
                )

                session_vals = compute_corr_vals(rolling_mean_firing_rates)

                results_dict["corr_val"] += session_vals
                results_dict["window_size_in_sec"] += [
                    window_sizes_in_seconds[i]
                ] * len(session_vals)
                results_dict["group"] += [group] * len(session_vals)
                results_dict["session_id"] += [session_id] * len(session_vals)

    # Return a pandas dataframe to make things easier to plot
    return pd.DataFrame(results_dict)


def compile_event_intervals(spikes_dict, sr_dict):
    """Compile event intervals from a dictionary of spikes and sample rates.

    Parameters
    ----------
    spikes_dict : dict
        Dictionary of spikes, organized by group and session ID.
    sr_dict : dict
        Dictionary of sample rates, organized by group and session ID.

    Returns
    -------
    pandas.DataFrame
        Dataframe containing the event intervals for each ROI in each session
        in each group.
    """
    # Compute event intervals for each session in each group
    results_dict = {"session_id": [], "group": [], "event_interval": []}

    for group, group_spikes in spikes_dict.items():
        for session_id, session_spikes in group_spikes.items():
            sample_rate = sr_dict[group][session_id]
            for roi_spikes in session_spikes:
                event_intervals = (np.diff(roi_spikes) / sample_rate).tolist()
                results_dict["event_interval"] += event_intervals
                results_dict["group"] += [group] * len(event_intervals)
                results_dict["session_id"] += [session_id] * len(event_intervals)

    # Return pandas dataframe from results dict
    return pd.DataFrame(results_dict)


def mean_event_rate(event_intervals_df):
    return 1 / np.mean(event_intervals_df["event_interval"])


def coefficient_of_variation(event_intervals_df):
    return np.std(event_intervals_df["event_interval"]) / np.mean(
        event_intervals_df["event_interval"]
    )


def compile_periods(trace_dict, sr_dict):
    """Compile main periods from a dictionary of traces and sample rates.

    Parameters
    ----------
    trace_dict : dict
        Dictionary of traces, organized by group and session ID.
    sr_dict : dict
        Dictionary of sample rates, organized by group and session ID.

    Returns
    -------
    pd.DataFrame
        Dataframe containing the main periods for each ROI in each session in
        each group.
    """
    periods = {
        "session_id": [],
        "roi": [],
        "group": [],
        "period_size": [],
    }

    for group, traces in trace_dict.items():
        for session_id, array in traces.items():
            # Get sample rate for this session
            sample_rate = sr_dict[group][session_id]

            for roi in range(array.shape[0]):
                periods["session_id"].append(session_id)
                periods["roi"].append(roi)
                periods["group"].append(group)
                periods["period_size"].append(
                    _calculate_period_size(array[roi, :], sample_rate)
                )

    return pd.DataFrame(periods)


def _calculate_period_size(trace, sample_rate):
    # If trace is all zeros, return NaN
    if np.all(trace == 0):
        return np.nan
    else:
        try:
            period_size = pyd.estimate_periodicity(
                trace,
                method="autoperiod",
                detrend=False,
            )
            return period_size / sample_rate
        except OverflowError:
            return np.nan


def gather_bkps_intervals(bkps_dict, sample_rates):
    """Gather the breakpoint intervals from a dictionary of breakpoints.

    Parameters
    ----------
    bkps_dict : dict
        Dictionary of breakpoints, organized by group and session ID.
    sample_rates : dict
        Dictionary of sample rates, organized by group and session ID.

    Returns
    -------
    pd.DataFrame
        Dataframe containing the breakpoint intervals for each ROI in each
        session in each group.
    """
    breakpoint_intervals = {
        "group": [],
        "breakpoint_interval": [],
        "session_id": [],
        "roi": [],
    }
    for group, session_dict in bkps_dict.items():
        for session_id, bkps in session_dict.items():
            sample_rate = sample_rates[group][session_id]

            for roi, roi_bkps in enumerate(bkps):
                intervals = _calculate_intervals(roi_bkps, sample_rate)
                breakpoint_intervals["breakpoint_interval"] += intervals
                breakpoint_intervals["group"] += [group] * len(intervals)
                breakpoint_intervals["session_id"] += [session_id] * len(intervals)
                breakpoint_intervals["roi"] += [roi] * len(intervals)

    # Turn dictionary into DataFrame
    breakpoint_intervals = pd.DataFrame(breakpoint_intervals)

    return breakpoint_intervals


def _calculate_intervals(roi_bkps, sample_rate):
    n_breakpoints = len(roi_bkps)
    if n_breakpoints > 1:
        intervals = np.diff(roi_bkps).tolist()
    else:
        intervals = []

    # Convert intervals to seconds
    intervals = [interval / sample_rate for interval in intervals]

    return intervals


def compute_intermittency_signals(
    traces_dict, sample_rates, window_sizes_in_seconds, strides_in_seconds
):
    """Compute the intermitency signals for a set of traces.

    Intermittency is computed by comparing the distribution of the signal in
    the left and right halves of a sliding window.

    Parameters
    ----------
    traces_dict : dict
        Dictionary of traces, where the keys are the group names and the values
        are dictionaries of traces, where the keys are the session IDs and the
        values are the traces.
    sample_rates : dict
        Dictionary of sample rates, where the keys are the group names and the
        values are dictionaries of sample rates, where the keys are the session
        IDs and the values are the sample rates.
    window_sizes_in_seconds : list
        List of window sizes in seconds to use for computing the
        intermittency signals.
    strides_in_seconds : list
        List of strides in seconds to use for computing the intermittency
        signals.

    Returns
    -------
    intermitency_dict : dict
        Nested dictionary of intermitency signals, where the first level keys
        are the window sizes, the second level keys are the group names, and
        the third level keys are the session IDs. The values are the
        intermitency signals.
    """
    intermitency_dict = {}

    for i, time_window in tqdm(enumerate(window_sizes_in_seconds)):
        print(
            "Computing intermittency signals for window size "
            + f"{time_window} seconds"
        )

        intermitency_dict[time_window] = _compute_group_intermittency_signals(
            traces_dict, sample_rates, time_window, strides_in_seconds[i]
        )

    return intermitency_dict


def _compute_group_intermittency_signals(
    traces_dict, sample_rates, time_window, stride_in_seconds
):
    group_intermittency_dict = {}

    for group, traces in traces_dict.items():
        group_intermittency_dict[group] = _compute_session_intermittency_signals(
            traces, sample_rates[group], time_window, stride_in_seconds
        )

    return group_intermittency_dict


def _compute_session_intermittency_signals(
    traces, session_sample_rates, time_window, stride_in_seconds
):
    session_intermittency_dict = {}

    for session_id, array in traces.items():
        sample_rate = session_sample_rates[session_id]
        window_size_in_samples = int(time_window * sample_rate)
        stride_in_samples = int(stride_in_seconds * sample_rate)

        intermittency_signal = _compute_intermittency_signal(
            array, window_size_in_samples, stride_in_samples
        )

        session_intermittency_dict[session_id] = intermittency_signal

    return session_intermittency_dict


def _compute_intermittency_signal(array, window_size_in_samples, stride_in_samples):
    intermittency_signal = []
    for j in range(0, array.shape[-1] - window_size_in_samples, stride_in_samples):
        intermittency_signal.append(
            timeseries.intermittency_measure(array[:, j : j + window_size_in_samples])
        )
    intermittency_signal = np.array(intermittency_signal).T

    # Replace NaNs with zeros
    intermittency_signal[np.isnan(intermittency_signal)] = 0

    return intermittency_signal


def compile_change_intervals(
    intermittency_dict, sample_rates, window_sizes_in_seconds, strides_in_seconds
):
    """Compile the change intervals from the intermitency signals.

    Parameters
    ----------
    intermittency_dict : dict
        Dictionary of intermittency signals. See output of
        `compute_intermittency_signals`.
    sample_rates : dict
        Dictionary of sample rates, where the keys are the group names and the
        values are dictionaries of sample rates, where the keys are the session
        IDs and the values are the sample rates.

    Returns
    -------
    change_intervals : pd.DataFrame
        Dataframe of change intervals, where each row is a change interval and
        the columns are: group, session_id, change_interval, roi.
    """
    change_intervals = {
        "group": [],
        "session_id": [],
        "change_interval": [],
        "roi": [],
        "window_size_in_sec": [],
    }

    for i, time_window in enumerate(window_sizes_in_seconds):
        window_dict = intermittency_dict[time_window]

        for group in window_dict.keys():
            group_dict = window_dict[group]

            for session_id in group_dict.keys():
                session_intermittency = group_dict[session_id]

                roi_changes = _find_roi_changes(session_intermittency)

                # Find the intervals between the peaks
                results = _compute_intervals(
                    {
                        "group": group,
                        "session_id": session_id,
                        "changes": roi_changes,
                        "time_window": time_window,
                    },
                    sample_rates[group][session_id],
                    strides_in_seconds[i],
                )
                change_intervals["group"] += results["group"]
                change_intervals["session_id"] += results["session_id"]
                change_intervals["change_interval"] += results["change_interval"]
                change_intervals["roi"] += results["roi"]
                change_intervals["window_size_in_sec"] += results["window_size_in_sec"]

    return pd.DataFrame(change_intervals)


def _find_roi_changes(session_intermittency):
    roi_changes = {}
    for j, row in enumerate(session_intermittency):
        peaks, _ = find_peaks(row)
        roi_changes[j] = peaks
    return roi_changes


def _compute_intervals(roi_data, sample_rate, stride_in_seconds):
    group_list = []
    session_list = []
    roi_list = []
    change_intervals_list = []
    window_size_list = []
    for roi, changes in roi_data["changes"].items():
        # Convert stride to samples
        stride_in_samples = int(stride_in_seconds * sample_rate)

        # Compute the intervals between the peaks and convert
        # them to seconds
        intervals = (np.diff(changes) * stride_in_samples) / sample_rate
        intervals = intervals.tolist()

        group_list += [roi_data["group"]] * len(intervals)
        session_list += [roi_data["session_id"]] * len(intervals)
        change_intervals_list += intervals
        roi_list += [roi] * len(intervals)
        window_size_list += [roi_data["time_window"]] * len(intervals)

    return {
        "group": group_list,
        "session_id": session_list,
        "change_interval": change_intervals_list,
        "roi": roi_list,
        "window_size_in_sec": window_size_list,
    }
