{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Evaluating the quality of spike detectors\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import config\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import plotly.express as px\n",
    "\n",
    "# Ignore warnings from tensorflow\n",
    "os.environ[\"TF_CPP_MIN_LOG_LEVEL\"] = \"2\"\n",
    "\n",
    "from sleepyflybrains import dataset, detectors, plotting  # noqa: E402\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "DATA_DIR = config.DATA_DIR\n",
    "\n",
    "# Models path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "MODELS_DIR = config.MODELS_DIR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load dataset information\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "session_dataset = dataset.SessionDataset(DATA_DIR, subsets=[\"84C10_23E10_GCaMP7f\"])\n",
    "\n",
    "# Get sample rate of each session in the dataset\n",
    "sample_rates = {}\n",
    "for session in session_dataset.session_list:\n",
    "    sample_rates[session.session_id] = session.sample_rate\n",
    "\n",
    "\n",
    "def sample_rate_from_roi_id(roi_id):\n",
    "    \"\"\"Get sample rate from roi_id\"\"\"\n",
    "    session_id, _ = roi_id.split(\"@\")\n",
    "    return sample_rates[session_id]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## On the evaluations\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the sections below we will evaluate the performance of the spike detectors implemented in this repository. The narrative structure is the same, so we will only spell out the details for the `PeakDetector` section, which is the first one.\n",
    "\n",
    "Each of the models first had their hyperparameters selected using a hold-out validation set with human-annotated peaks. The hyperparameters were selected so that the model's spike predictions were close to the human-annotated in the [1-Wasserstein sense](https://en.wikipedia.org/wiki/Wasserstein_metric). See `scripts/hyperparam_search_*` and `slurm/hyperparam_search_*` for details.\n",
    "\n",
    "The models were evaluated on the same validation set by computing actual peak matches within a tolerance window $\\tau$. That is, if a model predicted a peak at time $t$, and there was a human-annotated peak within $\\tau$ of $t$, then we counted that as a match. We then computed the precision, recall, and F1 score of the models's predictions. See `scripts/evaluate_spike_detections.py` and `slurm/evaluate_*` for details.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reference: consistency in past and current human-annotated peaks\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tolerances = [50, 100, 150, 200, 250, 300]  # in ms\n",
    "\n",
    "eval_df_human = nb_utils.load_eval_metrics(\n",
    "    os.path.join(DATA_DIR, \"analysis_files\"), tolerances\n",
    ")\n",
    "\n",
    "eval_df_human"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the F1 score as a function of the tolerance window, we see that this\n",
    "metric grows as we increase the tolerance window, but then it saturates. This is\n",
    "expected, the exact location of a peak, especially in noisy data, is\n",
    "ill-defined. On the other hand, if we allow a sufficiently large tolerance\n",
    "window, then it should be easy to agree on whether there's a peak in that window\n",
    "or not.\n",
    "\n",
    "It is interesting to see that even with large tolerance windows, the human\n",
    "annotator still disagrees with themselves from the past. The best F1 score\n",
    "achieved is only 0.946, and only given a tolerance larger than 200ms. In an\n",
    "ideal world, we would expect the score to be 1.0 and the tolerance window to the\n",
    "0ms. The actual observed behavior should serve to set expectations for the\n",
    "performance of automated spike detectors.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    eval_df_human,\n",
    "    x=\"tolerance\",\n",
    "    y=[\"f1\"],\n",
    "    title=\"Consistency of past and current human annotations\",\n",
    "    labels={\"value\": \"Score\", \"variable\": \"Metric\", \"tolerance\": \"Tolerance (ms)\"},\n",
    "    markers=True,\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    height=600,\n",
    "    width=600,\n",
    "    yaxis_range=[0, 1],\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Which places in the fluorescence trace cause disagreement? We can explore this\n",
    "question by loading the past and current annotations from the merged file\n",
    "generated when running the consistency evaluation.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "merged_annotations = pd.read_csv(\n",
    "    os.path.join(DATA_DIR, \"analysis_files\", \"past_current_annotations_merged.csv\")\n",
    ")\n",
    "merged_annotations.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ROI IDs that appear both in the past and current annotations are those:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_grouping = merged_annotations.groupby(\"series\")\n",
    "list(roi_grouping.groups.keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's pick one ROI and plot its trace, and the past and current annotations as\n",
    "spike trains. Current annotations show up as red spikes, and past annotations as green spikes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_id = \"84C10_23E10_20231006_31@4\"\n",
    "\n",
    "df = roi_grouping.get_group(roi_id)\n",
    "\n",
    "session_id = roi_id.split(\"@\")[0]\n",
    "sample_rate = sample_rates[session_id]\n",
    "\n",
    "y = df[\"value\"].values\n",
    "c = np.nan * np.ones_like(y)\n",
    "s = df[\"label_past\"].values\n",
    "\n",
    "fig, ax = plotting.plot_spike_deconvolution(\n",
    "    y.flatten(),\n",
    "    c.flatten(),\n",
    "    s.flatten(),\n",
    "    true_spikes=df[\"label_current\"].values.flatten(),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `PeakDetector`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "detector = \"peakdetector\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given that we have two sets of annotations (past and current), we could in\n",
    "principle fit a `PeakDetector` on one set of annotations and evaluate it on\n",
    "another. Let us first load the results of both fitting and evaluating the model on current\n",
    "annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tolerances = [50, 100, 150, 200, 250, 300]  # in ms\n",
    "\n",
    "fit_on_current_eval_on_current = nb_utils.load_eval_metrics(\n",
    "    os.path.join(MODELS_DIR, detector, \"eval_on_current\"), tolerances\n",
    ")\n",
    "\n",
    "fit_on_current_eval_on_current"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us load the results for other fit/evaluation pair for comparison.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_on_current_eval_on_past = nb_utils.load_eval_metrics(\n",
    "    os.path.join(MODELS_DIR, detector, \"eval_on_past\"), tolerances\n",
    ")\n",
    "fit_on_current_eval_on_past"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_on_past_eval_on_current = nb_utils.load_eval_metrics(\n",
    "    os.path.join(MODELS_DIR, detector, \"fit_with_past_annotations\", \"eval_on_current\"),\n",
    "    tolerances,\n",
    ")\n",
    "fit_on_past_eval_on_current"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fit_on_past_eval_on_past = nb_utils.load_eval_metrics(\n",
    "    os.path.join(MODELS_DIR, detector, \"fit_with_past_annotations\", \"eval_on_past\"),\n",
    "    tolerances,\n",
    ")\n",
    "fit_on_past_eval_on_past"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assess `PeakDetector` in the context of the consistency of human-annotated peaks\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot first human consistency curve\n",
    "fig = px.line(\n",
    "    eval_df_human,\n",
    "    x=\"tolerance\",\n",
    "    y=[\"f1\"],\n",
    "    title=\"Peak detection performance\",\n",
    "    markers=True,\n",
    ")\n",
    "\n",
    "# Change name of the trace to 'Human agreement'\n",
    "fig.data[0].name = \"Human agreement\"\n",
    "\n",
    "# Add traces for peak detection performance\n",
    "fig.add_scatter(\n",
    "    x=fit_on_past_eval_on_past[\"tolerance\"],\n",
    "    y=fit_on_past_eval_on_past[\"f1\"],\n",
    "    name=\"Fit on past, eval on past\",\n",
    ")\n",
    "fig.add_scatter(\n",
    "    x=fit_on_current_eval_on_past[\"tolerance\"],\n",
    "    y=fit_on_current_eval_on_past[\"f1\"],\n",
    "    name=\"Fit on current, eval on past\",\n",
    ")\n",
    "fig.add_scatter(\n",
    "    x=fit_on_current_eval_on_current[\"tolerance\"],\n",
    "    y=fit_on_current_eval_on_current[\"f1\"],\n",
    "    name=\"Fit on current, eval on current\",\n",
    ")\n",
    "fig.add_scatter(\n",
    "    x=fit_on_past_eval_on_current[\"tolerance\"],\n",
    "    y=fit_on_past_eval_on_current[\"f1\"],\n",
    "    name=\"Fit on past, eval on current\",\n",
    ")\n",
    "\n",
    "# Update layout\n",
    "fig.update_layout(\n",
    "    height=600,\n",
    "    width=800,\n",
    "    yaxis_range=[0, 1],\n",
    "    hovermode=\"x unified\",\n",
    "    legend_title_text=\"F1 score\",\n",
    ")\n",
    "\n",
    "# Change hover info to be only the y value\n",
    "fig.update_traces(\n",
    "    hoverinfo=\"y\",\n",
    "    hovertemplate=\"%{y:.3f}\",\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inspect 'true' and predicted peaks side-by-side\n",
    "\n",
    "Let's load the human annotated peaks and the predicted peaks for the validation dataset so we can inspect them side-by-side. In the validation table, human annotations are stored in the `label` columns, while predicted peaks are stored in the `predicted_label` column.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "validation_and_detected_peaks = pd.read_csv(\n",
    "    os.path.join(\n",
    "        MODELS_DIR, detector, \"validation_and_detected_peaks_\" + detector + \".csv\"\n",
    "    ),\n",
    ")\n",
    "validation_and_detected_peaks.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we can see the IDs of all regions-of-interest (ROIs) that have been annotated. Their naming convention is `session_id@roi_number`. For example, `84C10_23E10_20231005_02@4` corresponds to the 5th ROI (counting starts from zero) in the session `84C10_23E10_20231005_02`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_grouping = validation_and_detected_peaks.groupby(\"series\")\n",
    "list(roi_grouping.groups.keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To aid in inspection, here are some remarks about the traces in these ROIs:\n",
    "\n",
    "- ROIs `84C10_23E10_20231005_16@2`, `84C10_23E10_20231006_31@4`,\n",
    "  `84C10_23E10_20231006_49@0`, `84C10_23E10_20231006_49@4`,\n",
    "  `84C10_23E10_20231008_26@1` are full of peaks, the rest is mostly just noise\n",
    "\n",
    "- ROI `84C10_23E10_20231017_39@10` is weird, in that it has a single slow\n",
    "  peak-like event, but this event has not been annotated.\n",
    "\n",
    "Let's examine one of them in more detail. The top plot shows the (baseline-corrected) calcium fluorescence trace of the ROI, overlaid by the denoised version that the peak detector sees. The bottom plot shows the human-annotated peaks (red) and the predicted peaks (green). Both plots are aligned temporally, so one can match the spikes in the bottom to their location in the noisy data in the top plot.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_id = \"84C10_23E10_20231006_31@4\"\n",
    "\n",
    "df = roi_grouping.get_group(roi_id)\n",
    "\n",
    "session_id = roi_id.split(\"@\")[0]\n",
    "sample_rate = sample_rates[session_id]\n",
    "\n",
    "y = df[\"value\"].values\n",
    "detector = \"peakdetector\"\n",
    "peak_detector = detectors.PeakDetector(\n",
    "    model_dir=os.path.join(\n",
    "        MODELS_DIR,\n",
    "        detector,\n",
    "    )\n",
    ")\n",
    "\n",
    "denoising_filter = peak_detector._default_denoising_filter(sample_rate)\n",
    "c = denoising_filter(y)\n",
    "\n",
    "s = peak_detector.predict_discrete(\n",
    "    y,\n",
    "    sample_rate=sample_rate,\n",
    ")\n",
    "\n",
    "fig, ax = plotting.plot_spike_deconvolution(\n",
    "    y.flatten(), c.flatten(), s.flatten(), true_spikes=df[\"label\"].values.flatten()\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the F1-score plot indicated, `PeakDetector` with well-selected hyperparameters performs quite well overall. However, it does still miss some calcium events.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## OASISDetector\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "detector = \"oasis\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us load the evaluation metrics for a set of different tolerance windows. `OASISDetector` was only fit and evaluated on the past annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tolerances = [50, 100, 150, 200, 250, 300]  # in ms\n",
    "\n",
    "eval_df = nb_utils.load_eval_metrics(os.path.join(MODELS_DIR, detector), tolerances)\n",
    "\n",
    "eval_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the F1 score as a function of the tolerance window.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    eval_df,\n",
    "    x=\"tolerance\",\n",
    "    y=[\"f1\"],\n",
    "    title=\"Peak detection performance\",\n",
    "    labels={\"value\": \"Score\", \"variable\": \"Metric\", \"tolerance\": \"Tolerance (ms)\"},\n",
    "    markers=True,\n",
    ")\n",
    "\n",
    "# Add trace for human annotations in red\n",
    "fig.add_scatter(\n",
    "    x=eval_df_human[\"tolerance\"],\n",
    "    y=eval_df_human[\"f1\"],\n",
    "    mode=\"lines+markers\",\n",
    "    name=\"Human annotation agreement\",\n",
    "    line=dict(color=\"red\"),\n",
    "    hovertemplate=\"f1 = %{y:.3f}<extra></extra>\",\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    height=600,\n",
    "    width=800,\n",
    "    yaxis_range=[0, 1],\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inspect 'true' and predicted peaks side-by-side\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "validation_and_detected_peaks = pd.read_csv(\n",
    "    os.path.join(\n",
    "        MODELS_DIR,\n",
    "        detector,\n",
    "        \"validation_and_detected_peaks_\" + detector + \"detector\" + \".csv\",\n",
    "    ),\n",
    ")\n",
    "validation_and_detected_peaks.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_grouping = validation_and_detected_peaks.groupby(\"series\")\n",
    "list(roi_grouping.groups.keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remarks:\n",
    "\n",
    "- ROIs `84C10_23E10_20231005_16@2`, `84C10_23E10_20231006_31@4`,\n",
    "  `84C10_23E10_20231006_49@0`, `84C10_23E10_20231006_49@4`,\n",
    "  `84C10_23E10_20231008_26@1` are full of peaks, the rest is mostly just noise\n",
    "\n",
    "- ROI `84C10_23E10_20231017_39@10` is weird, in that it has a single slow\n",
    "  peak-like event, but this event has not been annotated.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_id = \"84C10_23E10_20231006_31@4\"  # Strong signal\n",
    "# roi_id = '84C10_23E10_20231005_02@4'  # Just noise\n",
    "\n",
    "df = roi_grouping.get_group(roi_id)\n",
    "\n",
    "y = df[\"value\"].values\n",
    "detector = \"oasis\"\n",
    "oasis_detector = detectors.OASISDetector(\n",
    "    model_dir=os.path.join(\n",
    "        MODELS_DIR,\n",
    "        detector,\n",
    "    )\n",
    ")\n",
    "c, s, b, g, lam = oasis_detector.deconvolve(\n",
    "    y,\n",
    ")\n",
    "\n",
    "fig, ax = plotting.plot_spike_deconvolution(\n",
    "    y.flatten(),\n",
    "    c.flatten(),\n",
    "    (s > oasis_detector.threshold).astype(int).flatten(),\n",
    "    true_spikes=df[\"label\"].values.flatten(),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected from the F1-score plot, `OASISDetector` performs worse than `PeakDetector` overall. This is because we tried setting a global noise level correction (via hyperparameter `sn_factor`) for the whole dataset. Indeed, by exploring `sn_factor` values a bit more for the highlighted ROI above, we can get much better performance from `OASISDetector`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Values fit during hyperparameter optimization:\")\n",
    "print(\"SN factor: {:.3f}\".format(oasis_detector.sn_factor))\n",
    "print(\"Threshold: {:.3f}\".format(oasis_detector.threshold))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = roi_grouping.get_group(roi_id)\n",
    "\n",
    "# Altered values:\n",
    "sn_factor = 0.97\n",
    "threshold = 0.04\n",
    "\n",
    "y = df[\"value\"].values\n",
    "detector = \"oasis\"\n",
    "oasis_detector = detectors.OASISDetector(\n",
    "    model_dir=os.path.join(\n",
    "        MODELS_DIR,\n",
    "        detector,\n",
    "    )\n",
    ")\n",
    "c, s, b, g, lam = oasis_detector.deconvolve(\n",
    "    y,\n",
    "    sn_factor=sn_factor,\n",
    ")\n",
    "\n",
    "fig, ax = plotting.plot_spike_deconvolution(\n",
    "    y.flatten(),\n",
    "    c.flatten(),\n",
    "    (s > threshold).astype(int).flatten(),\n",
    "    true_spikes=df[\"label\"].values.flatten(),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This indicates that the noise level should be better estimated individually for each ROI. It's a challenge to do this automatically in a reliable way, but one possibility is to do as in the cell below:\n",
    "\n",
    "1. Use `OASISDetector` to deconvolve the calcium fluorescence trace using a range of `sn_factor` values\n",
    "2. Compute the l1-norm of the output spike train for each `sn_factor` value\n",
    "3. Pick the `sn_factor` at the elbow of the l1-norm curve.\n",
    "\n",
    "The reasoning behind point 3 is that in sparse reconstruction problems in general there should be a phase transition between a regime where the l1-norm is large (and the output is mostly noise) and a regime where the l1-norm is small (and the output is mostly signal). The elbow of the l1-norm (transition region) would thus represent a good trade-off between those two regimes.\n",
    "\n",
    "Note that in the plot below the \"good\" `sn_factor` chosen in the previous cell is precisely in the elbow of the l1-norm curve.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "factors = np.linspace(0.9, 0.99, 100)\n",
    "l1_norms = []\n",
    "for factor in factors:\n",
    "    c, s, b, g, lam = oasis_detector.deconvolve(\n",
    "        y,\n",
    "        sn_factor=factor,\n",
    "    )\n",
    "    l1_norms.append(np.linalg.norm(s.flatten(), 1))\n",
    "\n",
    "fig = px.line(\n",
    "    pd.DataFrame(\n",
    "        {\n",
    "            \"sn_factor\": factors,\n",
    "            \"l1_norm\": l1_norms,\n",
    "        }\n",
    "    ),\n",
    "    x=\"sn_factor\",\n",
    "    y=[\"l1_norm\"],\n",
    "    title=\"L1 norm of the spike train\",\n",
    "    labels={\"value\": \"Score\", \"variable\": \"Metric\", \"sn_factor\": \"SN factor\"},\n",
    "    markers=True,\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    height=600,\n",
    "    width=600,\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because `OASISDetector` cannot be reliably used \"out-of-the-box\" and should be tuned for each ROI, we do not recommend it over `PeakDetector` for this dataset.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `CascadeDetector`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "detector = \"cascade\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us load the evaluation metrics for a set of different tolerance windows. `CascadeDetector` was only evaluated on the past annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tolerances = [50, 100, 150, 200, 250, 300]  # in ms\n",
    "\n",
    "eval_df = nb_utils.load_eval_metrics(os.path.join(MODELS_DIR, detector), tolerances)\n",
    "\n",
    "eval_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    eval_df,\n",
    "    x=\"tolerance\",\n",
    "    y=[\"f1\"],\n",
    "    title=\"Peak detection performance\",\n",
    "    labels={\"value\": \"Score\", \"variable\": \"Metric\", \"tolerance\": \"Tolerance (ms)\"},\n",
    "    markers=True,\n",
    ")\n",
    "\n",
    "# Add trace for human annotations in red\n",
    "fig.add_scatter(\n",
    "    x=eval_df_human[\"tolerance\"],\n",
    "    y=eval_df_human[\"f1\"],\n",
    "    mode=\"lines+markers\",\n",
    "    name=\"Human annotation agreement\",\n",
    "    line=dict(color=\"red\"),\n",
    "    hovertemplate=\"f1 = %{y:.3f}<extra></extra>\",\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    height=600,\n",
    "    width=600,\n",
    "    yaxis_range=[0, 1],\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inspect 'true' and predicted peaks side-by-side\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "validation_and_detected_peaks = pd.read_csv(\n",
    "    os.path.join(\n",
    "        MODELS_DIR,\n",
    "        detector,\n",
    "        \"validation_and_detected_peaks_\" + detector + \"detector.csv\",\n",
    "    ),\n",
    ")\n",
    "validation_and_detected_peaks.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_grouping = validation_and_detected_peaks.groupby(\"series\")\n",
    "list(roi_grouping.groups.keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remarks:\n",
    "\n",
    "- ROIs `84C10_23E10_20231005_16@2`, `84C10_23E10_20231006_31@4`,\n",
    "  `84C10_23E10_20231006_49@0`, `84C10_23E10_20231006_49@4`,\n",
    "  `84C10_23E10_20231008_26@1` are full of peaks, the rest is mostly just noise\n",
    "\n",
    "- ROI `84C10_23E10_20231017_39@10` is weird, in that it has a single slow\n",
    "  peak-like event, but this event has not been annotated.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "roi_id = \"84C10_23E10_20231006_31@4\"  # Strong signal\n",
    "# roi_id = '84C10_23E10_20231005_02@4'  # Just noise\n",
    "\n",
    "df = roi_grouping.get_group(roi_id)\n",
    "\n",
    "session_id = roi_id.split(\"@\")[0]\n",
    "sample_rate = sample_rates[session_id]\n",
    "\n",
    "y = df[\"value\"].values\n",
    "c = np.nan * np.ones_like(y)\n",
    "s = df[\"predicted_label\"].values\n",
    "\n",
    "fig, ax = plotting.plot_spike_deconvolution(\n",
    "    y.flatten(), c.flatten(), s.flatten(), true_spikes=df[\"label\"].values.flatten()\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is currently no Cascade model tailored to our dataset. From the ones available, even if we downsample the data to the model's native sampling rate, we get extremely conservative predictions. Lots of real calcium events are missed. One would have to either train a new model or fine-tune the existing ones to get good performance on our dataset. Therefore, we cannot recommend it over `PeakDetector`.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "sfb",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "2da6f128e681b9be3b593636fae9c0e18e82a5a89fce918a5624c5a741aa670c"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
